/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				init_hyphob.c

Purpose:
	Initialize hydrophobicity values associated with the general purpose
	sequence and with the reference sequence.

Input:
	(1) Pointer to RuntimeS structure,  with both input and output data.

Output:
	(1) Hydrophobicity information initialized.

Return value:
	No return value.

Notes:
	(1) Check init_runtime.c for default hydrophobicity scale!

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

double		Hydrophobicity_ (char *, int, int);

/*======initialize hydrophobicity values:====================================*/

void InitHyphob_ (RuntimeS *runtimeSP)
{
int		max_length;
size_t		residueI;
int		scaleI;
char		*residue_nameP;
char		residue_nameA[RESNAMESIZE];
double		hydrophobicity;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Hydrophobicity scale index: */
scaleI = runtimeSP->hydrophobicity_scaleI;

/* Scan the general purpose sequence: */
for (residueI = 0; residueI < runtimeSP->residuesN; residueI++)
	{
	/* Pointer to the current residue name: */
	residue_nameP = runtimeSP->sequenceP + residueI * max_length;

	/* Copy the residue name: */
	strncpy (residue_nameA, residue_nameP, max_length);
	residue_nameA[max_length] = '\0';

	/* The corresponding hydrophobicity: */
	hydrophobicity = Hydrophobicity_ (residue_nameA, scaleI, 0);

	/* Store the hydrophobicity value: */
	*(runtimeSP->hydrophobicityP + residueI) = hydrophobicity;
	}

/* Scan the reference sequence: */
for (residueI = 0; residueI < runtimeSP->reference_residuesN; residueI++)
	{
	/* Pointer to the current residue name: */
	residue_nameP = runtimeSP->reference_sequenceP + residueI * max_length;

	/* Copy the residue name: */
	strncpy (residue_nameA, residue_nameP, max_length);
	residue_nameA[max_length] = '\0';

	/* The corresponding hydrophobicity: */
	hydrophobicity = Hydrophobicity_ (residue_nameA, scaleI, 0);

	/* Store the hydrophobicity value: */
	*(runtimeSP->reference_hydrophobicityP + residueI) = hydrophobicity;
	}

}

/*===========================================================================*/


