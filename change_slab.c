/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				change_slab.c

Purpose:
	Change slab mode for each caught macromolecular complex.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Slab mode index.

Output:
	(1) Slab mode changed for each caught macromolecular complex.
	(2) Return value.

Return value:
	(1) The number of complexes for which the slab mode was changed.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		InitSlab_ (MolComplexS *);

/*======change slab mode:====================================================*/

int ChangeSlab_ (MolComplexS *mol_complexSP, int mol_complexesN,
		 int slab_modeI)
{
int		changesN = 0;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Do not apply slab if position has not changed: **/
	if (curr_mol_complexSP->catchF== 0) continue;

	/** Change the slab mode: **/
	curr_mol_complexSP->slab_modeI = slab_modeI;

	/** Initialize slab: **/
	InitSlab_ (curr_mol_complexSP);

	/** Change the position_changedF: **/
	curr_mol_complexSP->position_changedF = 1;

	/** Increase the count: **/
	changesN++;
	}

/* Return the number of m. complexes for */
/* for which  the slab mode was changed: */
return changesN;
}

/*===========================================================================*/

