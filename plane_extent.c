/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				plane_extent.c

Purpose:
	Find  plane extent for  the current image of  the current plane.  If
	drawing in stereo mode, there are two images of each plane. For mono
	mode, there is only one image.

Input:
	(1) Pointer to minimal value of x, in screen units.
	(2) Pointer to maximal value of x, in screen units.
	(3) Pointer to minimal value of y, in screen units.
	(4) Pointer to maximal value of y, in screen units.
	(5) Pointer to PlaneS structure,  with data about the current plane.
	(6) Pointer to ConfigS structure, with configuration data.
	(7) The current image index (0 or 1).

Output:
	(1) Plane extent prepared (four integers).

Return value:
	No return value.

=============================================================================*/

#include <stdio.h>

#include <limits.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======find plane extent:===================================================*/

void PlaneExtent_ (int *screen_x_minP, int *screen_x_maxP,
		   int *screen_y_minP, int *screen_y_maxP,
		   PlaneS *curr_planeSP, ConfigS *configSP, int curr_imageI)
{
int		left_edge, right_edge;
int		center_screen_x, center_screen_y;
double		angle, sin_angle, cos_angle;
double		x1, y1, x2, y2, x3, y3, x4, y4;
double		x1rot, y1rot, x2rot, y2rot, x3rot, y3rot, x4rot, y4rot;
double		x_min, x_max, y_min, y_max;

/* Left and right edge of the current image: */
left_edge  = configSP->image_screen_x0[curr_imageI];
right_edge = configSP->image_screen_x1[curr_imageI];

/* Position of the window free area center: */
center_screen_x = curr_planeSP->center_screen_x[curr_imageI];
center_screen_y = curr_planeSP->center_screen_y;

/* The shifted phi angle, used to rotate the "plane" bounding rectangle: */
angle = curr_planeSP->normal_phi[curr_imageI] - 4.712389;

/* Sine and cosine of this angle: */
sin_angle = sin (angle);
cos_angle = cos (angle);

/* Four rectangle corners before rotation and translation: */
x1 =  curr_planeSP->screen_a;
y1 =  curr_planeSP->screen_b[curr_imageI];
x2 = -curr_planeSP->screen_a;
y2 =  curr_planeSP->screen_b[curr_imageI];
x3 = -curr_planeSP->screen_a;
y3 = -curr_planeSP->screen_b[curr_imageI];
x4 =  curr_planeSP->screen_a;
y4 = -curr_planeSP->screen_b[curr_imageI];

/* Four rectangle corners after rotation and translation: */
x1rot = x1 * cos_angle - y1 * sin_angle + center_screen_x;
y1rot = x1 * sin_angle + y1 * cos_angle + center_screen_y;
x2rot = x2 * cos_angle - y2 * sin_angle + center_screen_x;
y2rot = x2 * sin_angle + y2 * cos_angle + center_screen_y;
x3rot = x3 * cos_angle - y3 * sin_angle + center_screen_x;
y3rot = x3 * sin_angle + y3 * cos_angle + center_screen_y;
x4rot = x4 * cos_angle - y4 * sin_angle + center_screen_x;
y4rot = x4 * sin_angle + y4 * cos_angle + center_screen_y;

/* Find extreme values: */
x_min = x1rot;
if (x2rot < x_min) x_min = x2rot;
if (x3rot < x_min) x_min = x3rot;
if (x4rot < x_min) x_min = x4rot;
x_max = x1rot;
if (x2rot > x_max) x_max = x2rot;
if (x3rot > x_max) x_max = x3rot;
if (x4rot > x_max) x_max = x4rot;
y_min = y1rot;
if (y2rot < y_min) y_min = y2rot;
if (y3rot < y_min) y_min = y3rot;
if (y4rot < y_min) y_min = y4rot;
y_max = y1rot;
if (y2rot > y_max) y_max = y2rot;
if (y3rot > y_max) y_max = y3rot;
if (y4rot > y_max) y_max = y4rot;

/* Check extreme values: */
if (x_min < (double) left_edge)		x_min = left_edge;
if (x_min > (double) right_edge)	x_min = right_edge;
if (x_max < (double) left_edge)		x_max = left_edge;
if (x_max > (double) right_edge)	x_max = right_edge;
if (y_min < (double) configSP->image_screen_y0)
					y_min = configSP->image_screen_y0;
if (y_min > (double) configSP->image_screen_y1)
					y_min = configSP->image_screen_y1;
if (y_max < (double) configSP->image_screen_y0)
					y_max = configSP->image_screen_y0;
if (y_max > (double) configSP->image_screen_y1)
					y_max = configSP->image_screen_y1;

/* Extra check, to avoid overflow: */
if (x_min > (double) INT_MAX / 4) x_min = (double) INT_MAX / 4;
if (x_min < (double) INT_MIN / 4) x_min = (double) INT_MIN / 4;
if (x_max > (double) INT_MAX / 4) x_max = (double) INT_MAX / 4;
if (x_max < (double) INT_MIN / 4) x_max = (double) INT_MIN / 4;
if (y_min > (double) INT_MAX / 4) y_min = (double) INT_MAX / 4;
if (y_min < (double) INT_MIN / 4) y_min = (double) INT_MIN / 4;
if (y_max > (double) INT_MAX / 4) y_max = (double) INT_MAX / 4;
if (y_max < (double) INT_MIN / 4) y_max = (double) INT_MIN / 4;

/* Copy extreme values: */
*screen_x_minP = (int) x_min;
*screen_x_maxP = (int) x_max;
*screen_y_minP = (int) y_min;
*screen_y_maxP = (int) y_max;
}

/*===========================================================================*/


