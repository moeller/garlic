/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				ex2doubles.c

Purpose:
	Extract two double values from a string.

Input:
	(1) Pointer to the first double value.
	(2) Pointer to the second double value.
	(3) Input string pointer.

Output:
	(1) The first double value.
	(2) The second double value.
	(3) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

/*======extract two double values from a string:=============================*/

int ExtractTwoDoubles_ (double *value1P, double *value2P, char *sP)
{
char		*P0, *P1;
int		n;
static double		d1, d2;

/* Colon should be separator: */
if ((P0 = strstr (sP, ":")) == NULL) P0 = sP;
else P0++;

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P1 = P0;
while ((n = *P1++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P1 - 1) = ' ';
	}

/* Try to read two double values: */
if (sscanf (P0, " %lf %lf", &d1, &d2) != 2) return -1;

/* On success, copy the extracted values: */
*value1P = d1;
*value2P = d2;

/* If everything worked fine, return positive integer: */
return 1;
}

/*===========================================================================*/


