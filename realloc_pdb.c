/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				realloc_pdb.c

Purpose:
	Check is there enough memory left for  PDB atomic data.  If not,
	reallocate  memory.  If reallocation fails,  print error message
	and return negative value.

Input:
	(1) Pointer to pointer to  AtomS structure.  Its value points to
	    the first element in an array of AtomS structures.
	(2) Pointer to pointer to  AtomS structure.  Its value points to
	    the next free (unused) element of the same array.
	(3) Pointer to maximal number of atoms.
	(4) Current number of atoms.

Output:
	(1) Memory may be reallocated,  pointer to the first element may
	    be changed.
	(2) Pointer to the next free (unused) element updated.
	(3) Maximal number of atoms updated.
	(4) Return value.

Return value:
	(1) Zero if there is enough memory (no reallocation).
	(2) Positive if memory successfully reallocated.
	(3) Negative if memory reallocation failed.

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======reallocate memory for PDB atomic data:===============================*/

int ReallocPDBMemory_ (AtomS **first_atomSPP, AtomS **next_atomSPP,
		       size_t *max_atomsNP, size_t curr_atomsN)
{
size_t		new_memory_size;
static AtomS	*new_first_atomSP;

/* If there is enough space for at least 100 atoms, do nothing: */
if (*max_atomsNP - curr_atomsN > 100) return 0; 

/* If this point is reached, reallocation is necessary: */
*max_atomsNP += ATOMS_IN_CHUNK;
new_memory_size = *max_atomsNP * sizeof (AtomS);
new_first_atomSP = realloc (*first_atomSPP, new_memory_size);

/* If reallocation fails: */
if (new_first_atomSP == NULL)
	{
	ErrorMessage_ ("garlic", "ReallocPDBMemory_", "",
		"Failed to reallocate memory for PDB atomic data!\n",
		"", "", "");

	/** Free  the original  memory  block and  reset pointer: **/
	/** (hope that realloc left the original block untouched) **/
	free (*first_atomSPP);
	*first_atomSPP = NULL;

	return -1;
	}

/* If reallocation was successful, update pointer values: */
*first_atomSPP = new_first_atomSP;
*next_atomSPP = new_first_atomSP + curr_atomsN;

/* Return positive value if reallocation successfully done: */
return 1;
}

/*===========================================================================*/


