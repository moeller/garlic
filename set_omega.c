/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

				set_omega.c

Purpose:
	Set the omega angle  for each selected residue  in the given complex.
	A residue is treated  as selected  if the first atom  of this residue
	is selected.  For proteins,  this is typically the N atom (nitrogen).

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to ConfigS structure.
	(3) The omega angle in degrees.

Output:
	(1) Omega angle set for each selected residue in each caught complex.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

Notes:
	(1) This function uses the atoms  N and CA of the current residue and
	    and the atoms CA and C of the previous residue.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractNCA_ (VectorS *, VectorS *, AtomS *, size_t, size_t);
int		ExtractCAC_ (VectorS *, VectorS *, AtomS *, size_t, size_t);
double		OmegaFromCACNCA_ (VectorS *, VectorS *, VectorS *, VectorS *,
				  ConfigS *);
int		RotateAtom_ (AtomS *, VectorS *, VectorS *, double);
int		DihedralAngles_ (MolComplexS *, ConfigS *);

/*======set omega angle(s):==================================================*/

int SetOmega_ (MolComplexS *mol_complexSP, ConfigS *configSP, double omega_new)
{
int			residuesN, residueI;
size_t			atomsN, atomI;
ResidueS		*current_residueSP;
AtomS			*first_atomSP;
size_t			current_startI, current_endI;
int			n;
static VectorS		N_vectorS, CA_vectorS;
ResidueS		*previous_residueSP;
size_t			previous_startI, previous_endI;
static VectorS		previousCA_vectorS, previousC_vectorS;
double			omega_old, delta_omega;
AtomS			*atomSP;

/* Copy the number of residues in the specified complex: */
residuesN = mol_complexSP->residuesN;

/* Copy the number of atoms in the specified complex: */
atomsN = mol_complexSP->atomsN;

/* Scan residues, but skip the first one: */
for (residueI = 1; residueI < residuesN; residueI++)
	{
	/* Pointer to the current residue: */
	current_residueSP = mol_complexSP->residueSP + residueI;

	/* Pointer to the first atom of this residue: */
	first_atomSP = mol_complexSP->atomSP +
		       current_residueSP->residue_startI;

	/* If the first atom of this residue is not */
	/* selected,  the residue  is not selected: */
	if (first_atomSP->selectedF == 0) continue;

	/* The range of atomic indices for the current residue: */
	current_startI = current_residueSP->residue_startI;
	current_endI   = current_residueSP->residue_endI;

	/* Try to extract N and CA coordinates for the current residue: */
	n = ExtractNCA_ (&N_vectorS, &CA_vectorS,
			 mol_complexSP->atomSP, current_startI, current_endI);
	if (n < 2) continue;

	/* The CA and C atoms of the previous residue are required. */

	/* Pointer to the previous residue: */
	previous_residueSP = mol_complexSP->residueSP + residueI - 1;

	/* The atomic index range for the previous residue: */
	previous_startI = previous_residueSP->residue_startI;
	previous_endI   = previous_residueSP->residue_endI;

	/* Extract CA and C coordinates from the previous residue: */
	n = ExtractCAC_ (&previousCA_vectorS, &previousC_vectorS,
			 mol_complexSP->atomSP,
			 previous_startI, previous_endI);
	if (n < 2) continue;

	/* Calculate and check the old omega: */
	omega_old = OmegaFromCACNCA_ (&previousCA_vectorS, &previousC_vectorS,
				      &N_vectorS, &CA_vectorS,
				      configSP);
	if (omega_old == BADDIHEDANGLE) continue;

	/* Calculate the difference: */
	delta_omega = omega_new - omega_old;

        /* Rotate all atoms of the current residue and all */
        /* atoms after the current residue about C-N bond: */
	for (atomI = current_startI; atomI < atomsN; atomI++)
		{
		/* Pointer to the current atom: */
		atomSP = mol_complexSP->atomSP + atomI;

		/* Rotate the current atom about N-CA bond: */
		RotateAtom_ (atomSP,
			     &previousC_vectorS, &N_vectorS, delta_omega);
                }
	}

/* Update dihedral angles and cis-trans flags: */
DihedralAngles_ (mol_complexSP, configSP);

/* Set the position_changedF: */
mol_complexSP->position_changedF = 1;

/* Return positive value (trivial): */
return 1;
}

/*===========================================================================*/


