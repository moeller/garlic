/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

			    residue_names.c

Purpose:
	Extract residue names from the list.

Input:
	(1) Pointer to SelectS structure, where data will be stored.
	(2) Pointer to the string with list of residue names.

Output:
	(1) Some data added to SelectS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract residue names:===============================================*/

int ExtractResidueNames_ (SelectS *selectSP, char *stringP)
{
char		*P, *tokenP;
int		nameI;

/* Check the string length: */
if (strlen (stringP) == 0) return -1;

/* Initialize all_residue_namesF (1 = select all residue names): */
selectSP->all_residue_namesF = 0;

/* Initialize the number of residue names: */
selectSP->residue_namesN = 0;

/* If wildcard (asterisk) is present, set */
/* all_residue_namesF  to one and return: */
if (strstr (stringP, "*"))
	{
	selectSP->all_residue_namesF = 1;
	return 1;
	}

/* Parse the list of residue names: */
P = stringP;
while ((tokenP = strtok (P, " \t,;")) != NULL)
	{
	/** Ensure the proper operation of strtok: **/
	P = NULL;

	/** Copy the token and update the count: **/
	nameI = selectSP->residue_namesN;
	strncpy (selectSP->residue_nameAA[nameI],
		 tokenP, RESNAMESIZE - 1);
	selectSP->residue_nameAA[nameI][RESNAMESIZE - 1] = '\0';

	/** Update and check the count: **/
	selectSP->residue_namesN++;
	if (selectSP->residue_namesN >= MAXFIELDS) break;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


