/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				chi2_from_cacbcg1cd.c

Purpose:
	Calculate dihedral angle chi2, using CA, CB, CG1 and CD coordinates.

Input:
	(1) Pointer to AtomS structure, pointing to the first atom of the
	    current macromolecular complex.
	(2) Index of the first atom of the current residue.
        (3) Index of the last atom of the currrent residue.

Output:
	Return value.

Return value:
	(1) Dihedral angle chi2, on success.
	(2) BADDIHEDANGLE on failure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractFourAtoms_ (VectorS *, VectorS *, VectorS *, VectorS *,
				   char *, char *, char *, char *,
				   AtomS *, size_t, size_t);
void		VectorProduct_ (VectorS *, VectorS *, VectorS *);
double		AbsoluteValue_ (VectorS *);
double		ScalarProduct_ (VectorS *, VectorS *);

/*======calculate chi2 from CA, CB, CG1 and CD:==============================*/

double Chi2FromCACBCG1CD_ (AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
static VectorS		CA_vectorS, CB_vectorS, CG1_vectorS, CD_vectorS;
int			n;
VectorS			CB_CA_vectorS, CB_CG1_vectorS;
VectorS			CG1_CB_vectorS, CG1_CD_vectorS;
VectorS			u1S, u2S;
VectorS			v1S, v2S;
double			denom, ratio, alpha;
double			chi2;

/* Extract CA, CB, CG1 and CD coordinates: */
n = ExtractFourAtoms_ (&CA_vectorS, &CB_vectorS, &CG1_vectorS, &CD_vectorS,
		       "CA", "CB", "CG1", "CD",
		       atomSP, atom_startI, atom_endI);

/* All four atoms are required to calculate the angle chi2: */
if (n < 4) return BADDIHEDANGLE;

/* The first pair of auxiliary vectors: */
CB_CA_vectorS.x  = CA_vectorS.x  - CB_vectorS.x;
CB_CA_vectorS.y  = CA_vectorS.y  - CB_vectorS.y;
CB_CA_vectorS.z  = CA_vectorS.z  - CB_vectorS.z;
CB_CG1_vectorS.x = CG1_vectorS.x - CB_vectorS.x;
CB_CG1_vectorS.y = CG1_vectorS.y - CB_vectorS.y;
CB_CG1_vectorS.z = CG1_vectorS.z - CB_vectorS.z;

/* The second pair of auxiliary vectors: */
CG1_CB_vectorS.x = CB_vectorS.x - CG1_vectorS.x;
CG1_CB_vectorS.y = CB_vectorS.y - CG1_vectorS.y;
CG1_CB_vectorS.z = CB_vectorS.z - CG1_vectorS.z;
CG1_CD_vectorS.x = CD_vectorS.x - CG1_vectorS.x;
CG1_CD_vectorS.y = CD_vectorS.y - CG1_vectorS.y;
CG1_CD_vectorS.z = CD_vectorS.z - CG1_vectorS.z;

/* Two vectors  perpendicular to  CB_CG1_vectorS,  mutually orthogonal, */
/* the second in the plane defined by CB_CA_vectorS and CB_CG1_vectorS: */
VectorProduct_ (&u1S, &CB_CA_vectorS, &CB_CG1_vectorS);
VectorProduct_ (&u2S, &u1S, &CB_CG1_vectorS);

/* Two vectors  perpendicular to  CG1_CB_vectorS,  mutually  orthogonal, */
/* the second in the plane defined by CG1_CB_vectorS and CG1_CD_vectorS: */
VectorProduct_ (&v1S, &CG1_CB_vectorS, &CG1_CD_vectorS);
VectorProduct_ (&v2S, &CG1_CB_vectorS, &v1S);

/* Calculate the angle alpha, which will be used to calculate chi2: */

/* Avoid division by zero: */
denom = AbsoluteValue_ (&u1S) * AbsoluteValue_ (&v1S);
if (denom == 0.0) return BADDIHEDANGLE;

/* Use the scalar product to calculate the cosine of the angle: */
ratio = ScalarProduct_ (&u1S, &v1S) / denom;

/* Arc cosine is very sensitive to floating point errors: */
if (ratio <= -1.0) alpha = 3.1415927;
else if (ratio >= 1.0) alpha = 0.0;
else alpha = acos (ratio);

/* There are two possible solutions; the right one is resolved here: */
if (ScalarProduct_ (&v2S, &u1S) >= 0) chi2 = alpha;
else chi2 = -alpha;

/* Return the angle (in radians): */
return chi2;
}

/*===========================================================================*/


