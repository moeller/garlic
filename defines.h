/* Copyright (C) 2001-2006 Damir Zucic */


/* Defines used in garlic program. */


/* Some general purpose symbols: */

#define STRINGSIZE         1024
#define SHORTSTRINGSIZE      80
#define MAXMESSAGES         100   /* Maximal number of error messages */
#define OPEN_FAILURE       -999


/* Conversion factors for angular units: */

#define DEG_TO_RAD        0.017453293
#define RAD_TO_DEG       57.29578


/* The maximal size of the main window: */
/* Notes: */
/* (1) Normally, the size of  NearestAtomS array is controled through */
/*     .garlicrc  file.  If  the corresponding  entries  are  missing */
/*     there, the values defined here are used. Check your .garlicrc! */
/* (2) Each pixel is  associated  with one  NearestAtomS  (defined in */
/*     typedefs.h). This may require too much memory on some systems. */
/* (3) The size of  NearestAtomS  array is  proportional  to  display */
/*     resolution. See sizehints.c for max. size of  the main window! */

#define MAXWINWIDTH        3000
#define MAXWINHEIGHT       3000


/* Array sizes used for protein data structures: */

#define PDBCODESIZE           5
#define TAGSIZE             128
#define ATOMNAMESIZE          5
#define SYMBOLSIZE            3
#define RESNAMESIZE           4
#define SEGNAMESIZE           5
#define ELEMNAMESIZE          3
#define CHARGESIZE            3
#define MAXCOMPLEXES       1000
#define ATOMS_IN_CHUNK     2500


/* Symbols related to PDB header: */

#define HEADERLINESIZE       81
#define MAXHEADERLINES        1
#define MAXTITLELINES         1
#define MAXCOMPNDLINES        5
#define MAXSOURCELINES        5
#define MAXEXPDTALINES        5
#define MAXAUTHORLINES        5


/* File format identifiers (see fileformat.c): */

#define PDB_FORMAT            1


/* Number of keywords in a standard format: */

#define PDB_KEYWORDS         52


/* The maximal value of refreshI: */

#define MAXREFRESHI       32000


/* The maximal number of color fading surfaces */
/* (do not confuse this with MAXMOLSURFACES!): */

#define MAXCOLORSURFACES      8


/* Default maximal number of lines: */

#define MAXLINES              7


/* The maximal bond drawing style index: */

#define MAXBONDSTYLEI         5


/* The identifier for nice bonds (one of bond drawing styles): */

#define NICE_BONDS          100


/* The identifier for sticks (one of bond drawing styles): */

#define STICK_BONDS    200


/* The identifier for the request to refresh bonds: */

#define REFRESH_BONDS	300


/* The maximal backbone drawing style index: */

#define MAXBONESTYLEI         5


/* The maximal atom drawing style index: */

#define MAXATOMSTYLEI         7


/* Spacefill style identifier: */

#define SPACEFILL           100


/* Spacefill (version 2) style identifier: */

#define SPACEFILL2          101


/* Ball style identifier: */

#define BALL                150


/* Ball (version 2) style identifier: */

#define BALL2               151


/* Big sphere style identifier: */

#define BIG_SPHERE          200


/* Big sphere (version 2) style identifier: */

#define BIG_SPHERE2         201


/* Covalent style identifier: */

#define COVALENT            250


/* Covalent style (version 2) identifier: */

#define COVALENT2           251


/* Small atom style identifier: */

#define SMALL_ATOM          300


/* Small atom (version 2) style identifier: */

#define SMALL_ATOM2         301


/* Plane style identifier: */

#define PLANE_STYLE          99


/* Default user and screen geometric parameters in real and atomic world: */

#define SCREEN_REAL_WX           270.0
#define SCREEN_REAL_WY           195.0
#define USER_SCREEN_REAL_DIST    700.0
#define SCREEN_ATOMIC_WX           0.5
#define USER_ATOMIC_Z           -250.0


/* The shift along z axis (used to tilt bonds): */

#define Z_SHIFT            0.1


/* Stereo image parameters: */

#define STEREO_MARGIN       10
#define STEREO_ANGLE       5.0                  /* Degrees */


/* Maximal number of bonds per atom (including hydrogen bonds): */

#define MAXBONDS            10


/* The number of known atomic pairs  (recognized by this program). */
/* Both " C N" and " N C" are included in the list of known pairs, */
/* though they are equivalent. This is done for practical reasons. */

#define KNOWNPAIRS          22 


/* The size of atomic pair array (string): */

#define PAIRSIZE             5


/* The number of hydrophobicity scales which are hard-coded in the program: */

#define SCALESN              6


/* The minimal spacing of slab surfaces: */

#define MINSLABSPACING     1.0


/* The minimal spacing of outer fading surfaces: */

#define MINFADINGSPACING   1.0


/* The maximal number of command strings: */

#define MAXCOMMSTRINGS     500


/* The maximal length of the command string: */

#define COMMSTRINGSIZE    1024


/* The text margin: */

#define TEXTMARGIN           1


/* The maximal number of chain  ID's,  residue ranges, */
/* residue names and atom names in selection cryteria: */

#define MAXFIELDS          100


/* Number of predefined (hard-coded) color schemes: */

#define COLORSCHEMES        26


/* The identifier used to identify H to H bond (must be negative!): */

#define H_TO_H_BOND       -100


/* The identifier used to identify unrecognized atomic pairs: */

#define GENERICID        16348


/* The margin of the circle used to represent the plane: */

#define CIRCLEMARGIN      0.25


/* Circle tilt angle (from negative y axis toward observer), in degrees: */

#define CIRCLETILT          20


/* Default circle transparency (should be between 0.0 and 1.0): */
#define DEFAULT_TRANSP    0.40


/* The maximal number of titles (strings added to the main window): */

#define MAXTITLES          100


/* The maximal length of the title string: */

#define TITLESTRINGSIZE   1024


/* The maximal number of residues in the sequence buffer: */

#define MAXRESIDUES      20000


/* The maximal pattern length (the maximal number of sets in pattern): */

#define MAX_PATT_LENGTH    100


/* For each position in the pattern, more than one residue may be specified. */
/* The following symbol defines the maximal number of residues per position: */

#define MAX_NAMES_IN_SET    30


/* The maximal number of residues in the pattern buffer: */
#define MAX_RES_IN_PATT   3000


/* The value which signals that dihedral angle is missing (not calculated): */

#define BADDIHEDANGLE    999.0


/* The screen width of a single residue, in pixels (used for plots): */

#define RESIDUE_WIDTH        5


/* Bit masks for structure, plane and envelope: */

#define STRUCTURE_MASK     0x1
#define PLANE_MASK         0x2
#define MEMBRANE_MASK      0x4
#define ENVELOPE_MASK      0x8


/* The gap between two objects prepared for docking (in angstroms): */

#define DOCKING_GAP          4


/* The parameter used  to define the width */
/* and height of docking area (angstroms): */

#define DOCKING_AREA_WIDTH     40.0


/* The projected surface of some structure is divided into small */
/* squares. This parameter defines the square size in angstroms: */

#define DOCKING_CELL_WIDTH     0.20


/* The atomic radius used for docking (in angstroms): */

#define DOCKING_ATOM_RADIUS     3.0


/* The maximal number of exposed residues: */

#define MAX_EXPOSED_RESIDUES   1000


/* The number  of neighbours  to be scanned while */
/* searching the representative atom for docking: */

#define DOCKING_SCAN_WIDTH       20


/* Sequence neighborhood half width: */

#define NEIGH_HALF_WIDTH          11


/* The omega angle tolerance (degrees). For a */
/* planar peptide, omega is 0 or 180 degrees. */

#define OMEGA_TOLERANCE          20


/* The segment width and the minimal score (used for sequence comparison): */

#define SEGMENT_WIDTH             5
#define MINIMAL_SCORE             5


/* The maximal number of template atoms: */

#define MAXTEMPLATEATOMS       1000


/* The angle between C-CA bond and C-N bond (degrees): */

#define CACN_ANGLE              116


/* The length of N-C bond: */

#define CN_BOND_LENGTH        1.325


/* The angle between N-C and N-CA bond (degrees): */

#define CNCA_ANGLE              122


/* The length of N-CA bond: */

#define NCA_BOND_LENGTH       1.455


/* The angle between CA-N and CA-C bond (degrees): */

#define NCAC_ANGLE              111


/* The length of CA-C bond: */

#define CAC_BOND_LENGTH       1.510


/* Default membrane thickness in angstroms: */

#define MEMBRANE_THICKNESS       30


/* The maximal number of  molecular surfaces per */
/* structure (do not confuse with MAXSURFACES!): */

#define MAXMOLSURFACES            5


/* The maximal number of macromolecular complexes in a group: */

#define MAX_GROUP_SIZE          100


/* An auxiliary cylinder is used to find the proper position and */
/* orientation of membrane with respect to  beta barrel protein. */
/* The following parameters define  the maximal number  of cells */
/* on the surface of this cylinder along  the axis and in a ring */
/* which runs  around the cylinder,  perpendicular to  the axis. */

#define BETA_CELLS_ALONG_AXIS         40
#define BETA_CELLS_IN_RING            20


/* The linear width of a single cell on the surface of auxiliary cylinder: */

#define BETA_CELL_LINEAR_WIDTH       3.0


/* The number of simple  (ordinary)  cells associated with the axis.  These */
/* cells are used to count PHE, TRP and TYR residues projected to the axis. */

#define SIMPLE_CELLS_ALONG_AXIS     4000


/* The linear width of a single simple cell: */

#define SIMPLE_CELL_WIDTH            0.2


