/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

				comparison_mouse.c

Purpose:
	Handle MotionNotify events if the main window contains the comparison
	of two sequences.

Input:
	(1) Pointer to RuntimeS structure, with runtime data.
	(2) Pointer to GUIS structure, with GUI data.
	(3) Pointer to NearestAtomS structure.
	(4) The number of pixels in the main window free area.
	(5) Pointer to XMotionEvent structure.

Output:
	(1) The information  about the sequence segment  covered by the mouse
	    pointer (and its neighborhood) written to the output window.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Zero if event is ignored.
	(3) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======handle MotionNotify events for sequence comparison:==================*/

int ComparisonMouse_ (RuntimeS *runtimeSP, GUIS *guiSP,
		      NearestAtomS *nearest_atomSP, size_t pixelsN,
		      XMotionEvent *motion_eventSP)
{
int		name_length;
int		square_width, square_height;
int		max_screen_x, max_screen_y;
int		ref_startI, startI;
int		screen_x, screen_y;
int		ref_residuesN, residuesN;
int		first_ref_residueI, first_residueI;
int		minimal_score;
int		line_height;
int		screen_x0, screen_y0;
int		ref_residueI, residueI;
char		stringA[SHORTSTRINGSIZE];
int		string_width;
int		localI;
size_t		pixelI;
NearestAtomS	*curr_pixelSP;
int		colorI;
int		serialI;
char		*nameP;
char		nameA[RESNAMESIZE];

/* Residue name length: */
name_length = RESNAMESIZE - 1;

/* Initialize square_width and square_height: */
square_width  = runtimeSP->zoom_factor;
if (square_width < 1) square_width = 1;
square_height = runtimeSP->zoom_factor;

/* Initialize the width and height of the usable area: */
max_screen_x = guiSP->main_win_free_area_width - 1;
max_screen_y = guiSP->main_win_free_area_height - 1;

/* Prepare start (offset) indices: */
ref_startI = runtimeSP->reference_offset;
startI = runtimeSP->sequence_offset;

/* Copy screen coordinates of the mouse pointer: */
screen_x = motion_eventSP->x;
screen_y = motion_eventSP->y;

/* Numbers of residues: */
ref_residuesN = runtimeSP->reference_residuesN;
residuesN = runtimeSP->residuesN;

/* Calculate and check the residue indices: */
first_ref_residueI = screen_x / square_width + ref_startI;
if (first_ref_residueI < 0) return -1;
if (first_ref_residueI >= ref_residuesN) return -2;
first_residueI = screen_y / square_height + startI;
if (first_residueI < 0) return -3;
if (first_residueI >= residuesN) return -4;

/* The score associated with the current pair should be found. */

/* The minimal score: */
minimal_score = runtimeSP->minimal_score;

/* Prepare and check the pixel index: */
screen_x = square_width * (first_ref_residueI - ref_startI);
if ((screen_x < 0) || (screen_x > max_screen_x)) return -5;
screen_y = square_height * (first_residueI - startI);
if ((screen_y < 0) || (screen_y > max_screen_y)) return -6;
pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;
if (pixelI >= pixelsN) return -7;

/* Pointer to NearestAtomS structure */
/* associated with the current pair: */
curr_pixelSP = nearest_atomSP + pixelI;

/* No drawing for pairs with low score: */
/* (score was stored as  mol_complexI!) */
if (curr_pixelSP->mol_complexI < minimal_score) return 0;

/* Refresh the output window: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->black_colorID);
XFillRectangle (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		0, 0, guiSP->output_winS.width, guiSP->output_winS.height);

/* Prepare  three colors for  the text: */
/* 0 = red     = mismatch               */
/* 1 = magenta = acceptable replacement */
/* 2 = yellow  = exact match            */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->red_colorID);
XSetForeground (guiSP->displaySP, guiSP->theGCA[1], guiSP->magenta_colorID);
XSetForeground (guiSP->displaySP, guiSP->theGCA[2], guiSP->yellow_colorID);

/* Prepare white color for header and lines: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[3], guiSP->white_colorID);

/* Text line height: */
line_height = guiSP->output_winS.text_line_height;

/* Initialize the string position: */
screen_x0 = guiSP->output_winS.width / 2;
screen_y0 = guiSP->output_winS.font_height + 5;

/* Draw title: */
strcpy (stringA, "HORIZ.");
string_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[3],
	     screen_x0 - string_width - 2, screen_y0,
	     stringA, strlen (stringA));
strcpy (stringA, "VERT.");
XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[3],
	     screen_x0 + 2, screen_y0,
	     stringA, strlen (stringA));

/* Draw vertical line which divides the output window in two parts: */
XDrawLine (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[3],
	   screen_x0, 0, screen_x0, guiSP->output_winS.height);

/* Draw horizontal line (under the title): */
screen_y0 += 2;
XDrawLine (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[3],
	   0, screen_y0, guiSP->output_winS.width, screen_y0);

/* Scan  twenty  residues  starting  from the */
/* first residue; do this for both sequences: */
for (localI = 0; localI < 20; localI++)
	{
	/* Update screen_y0: */
	screen_y0 += line_height;

	/* Do not draw text if it falls outside the output window: */
	if (screen_y0 > (int) guiSP->output_winS.height + line_height)
		{
		continue;
		}

	/* Prepare and check local indices for both sequences: */
	ref_residueI = first_ref_residueI + localI;
	if ((ref_residueI < 0) || (ref_residueI >= ref_residuesN)) continue;
	residueI = first_residueI + localI;
	if ((residueI < 0) || (residueI >= residuesN)) continue;

	/* If this point is reached, the current residue pair is available. */

	/* Prepare and check  the screen */
	/* position of the current pair: */
	screen_x = (int) square_width * (ref_residueI - ref_startI);
	if ((screen_x < 0) || (screen_x > max_screen_x)) continue;
	screen_y = (int) square_height * (residueI - startI);
	if ((screen_y < 0) || (screen_y > max_screen_y)) continue;

	/* Prepare and check the pixel index: */
	pixelI = guiSP->main_win_free_area_width * screen_y + screen_x;
	if (pixelI >= pixelsN) continue;

	/* Pointer to NearestAtomS structure */
	/* associated with the current pair: */
	curr_pixelSP = nearest_atomSP + pixelI;

	/* Prepare and check the color index: */
	colorI = curr_pixelSP->styleI;
	if ((colorI != 0) && (colorI != 1) && (colorI != 2)) continue;

	/* Prepare the reference residue serial number: */
	serialI = *(runtimeSP->reference_serialIP + ref_residueI);

	/* Prepare the reference residue name: */
	nameP = runtimeSP->reference_sequenceP + ref_residueI * name_length;
	strncpy (nameA, nameP, name_length);
	nameA[name_length] = '\0';

	/* The left string contains residue name */
	/* and serial number  for ref. sequence: */
	sprintf (stringA, "%d%s", serialI, nameA);

	/* Draw the left string: */
	string_width = XTextWidth (guiSP->main_winS.fontSP,
				   stringA, strlen (stringA));
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID,
		     guiSP->theGCA[colorI],
		     screen_x0 - string_width - 2, screen_y0,
		     stringA, strlen (stringA));

	/* Prepare the residue serial number (main sequence): */
	serialI = *(runtimeSP->serialIP + residueI);

	/* Prepare the residue name: */
	nameP = runtimeSP->sequenceP + residueI * name_length;
	strncpy (nameA, nameP, name_length);
	nameA[name_length] = '\0';

	/* The right string contains residue name */
	/* and serial number for refer. sequence: */
	sprintf (stringA, "%s%d", nameA, serialI);

	/* Draw the right string: */
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID,
		     guiSP->theGCA[colorI],
		     screen_x0 + 2, screen_y0,
		     stringA, strlen (stringA));
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


