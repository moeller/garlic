/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				scalar_product.c

Purpose:
	Calculate the scalar product of two vectors.

Input:
	(1) Pointer to VectorS structure, with the first vector.
	(2) Pointer to VectorS structure, with the second vector.

Output:
	Return value.

Return value:
	Scalar product of two vectors (double value).

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======calculate scalar product of two vectors:=============================*/

double ScalarProduct_ (VectorS *vector1SP, VectorS *vector2SP)
{

return (vector1SP->x * vector2SP->x +
	vector1SP->y * vector2SP->y +
	vector1SP->z * vector2SP->z);

}

/*===========================================================================*/


