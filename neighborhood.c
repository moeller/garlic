/* Copyright (C) 2000-2004 Damir Zucic */

/*=============================================================================

				neighborhood.c

Purpose:
	Execute neighborhood command: show or hide sequence neighborhood.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of the command string.  This command
	    may be given without keyword or with keyword OFF.

Output:
	(1) The show_sequence_neighborhoodF flag set to zero, one or two. 
	    Values:  0 = hide,  1 = show residue names,  2 = show residue
	    names, serial numbers and chain identifiers.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute neighborhood command:========================================*/

int Neighborhood_ (MolComplexS *mol_complexSP, int mol_complexesN,
		   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		   NearestAtomS *nearest_atomSP, size_t pixelsN,
		   unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain keyword OFF: */
if (remainderP)
	{
	/* If keyword OFF is present, set flag value to zero: */
	if (strstr (tokenA, "OFF") == tokenA)
		{
		/* Set flag: */
		configSP->show_sequence_neighborhoodF = 0;

		/* Refresh the main window: */
		(*refreshIP)++;
		MainRefresh_ (mol_complexSP, mol_complexesN,
			      runtimeSP, configSP, guiSP,
			      nearest_atomSP, pixelsN, *refreshIP);

		/* Refresh the control window: */
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);

		/* Return the command code: */
		return COMMAND_NEIGHBORHOOD;
		}

	/* If keyword VER (VERBOSE) is present, set flag to two: */
	else if (strstr (tokenA, "VER") == tokenA)
		{
		/* Set flag: */
		configSP->show_sequence_neighborhoodF = 2;

		/* Refresh the main window: */
		(*refreshIP)++;
		MainRefresh_ (mol_complexSP, mol_complexesN,
			      runtimeSP, configSP, guiSP,
			      nearest_atomSP, pixelsN, *refreshIP);

		/* Refresh the control window: */
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);

		/* Return the command code: */
		return COMMAND_NEIGHBORHOOD;
		}

	/* If keyword recognition fails: */
	else
		{
		sprintf (runtimeSP->messageA,
			 "Keyword %s not recognized!", tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_NEIGHBORHOOD;
		}
	}

/* If this point is reached, there are no keywords. Set flag value to one: */
configSP->show_sequence_neighborhoodF = 1;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_NEIGHBORHOOD;
}

/*===========================================================================*/


