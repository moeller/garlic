/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				load_complex.c

Purpose:
	Load the specified macromolecular complex. Read the specified file
	and do some additional operations.

Input:
	(1) Pointer to  the storage where  the group flag might be stored.
	    The group flag is a member of RuntimeS structure.  The pointer
	    used here  should point  either to  this flag or to some dummy
	    integer.
	(2) Pointer to MolComplexS structure (macromolecular complexes).
	(3) Pointer to the number of macromolecular complexes.
	(4) Pointer to the next macromolecular complex identifier.
	(5) Input file name (pointer).
	(6) Pointer to ConfigS structure, with configuration data.
	(7) Pointer to GUIS structure, with GUI data.

Output:
	(1) One macromolecular complex loaded.
	(2) Number of macromolecular complexes updated.
	(3) Return value.

Return value:
	(1) The updated number of macromolecular complexes, on success.
	(2) Negative on failure.

Notes:
	(1) Default hydrophobicity scale is the  Eisenberg scale published
	    in the article:

	    Eisenberg, D., Schwarz,  E., Komaromy, M. and Wall, R. (1984).
	    J. Mol. Biol. 179, p. 125.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ReadFile_ (MolComplexS *, char *, ConfigS *);
int		GeometricCenter_ (MolComplexS *);
void		FlipVector_ (VectorS *, VectorS *);
void		TranslateComplex_ (MolComplexS *, VectorS *, ConfigS *, int);
int		ComplexExtent_ (MolComplexS *);
void		InitSlab_ (MolComplexS *);
void		InitFading_ (MolComplexS *);
int		PrepareStereoData_ (MolComplexS *, ConfigS *);
size_t		StrongBonds_ (MolComplexS *, int, ConfigS *);
size_t		PseudoBonds_ (MolComplexS *, int, ConfigS *);
size_t		DisulfideBonds_ (MolComplexS *, int, ConfigS *);
int		PrepareBackbone_ (MolComplexS *, ConfigS *);
int		AssignRadii_ (MolComplexS *, ConfigS *);
int		AssignHydrophobicity_ (MolComplexS *, int);
int		ExtractSequence_ (MolComplexS *);
int		DihedralAngles_ (MolComplexS *, ConfigS *);
int		AllocSecondaryStructure_ (MolComplexS *);
/*@@*/ /* Generate the secondary structure */
int		GeneratePlane_ (MolComplexS *, ConfigS *, GUIS *);
int		InitializeMembrane_ (MolComplexS *, ConfigS *, GUIS *);

/*======read file, do some initializations:==================================*/

int LoadComplex_ (int *groupFP,
		  MolComplexS *mol_complexSP,
		  int *mol_complexesNP, int *next_mol_complexIDP,
		  char *mol_file_nameP, ConfigS *configSP, GUIS *guiSP)
{
int		new_mol_complexI;
MolComplexS	*new_mol_complexSP;
int		new_mol_complexID;
VectorS		shift_vectorS;			/* Shift to geometric center */
int		default_hyphob_scaleI = 2;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* Check is there any space left in the array of MolComplexS structures: */
if (*mol_complexesNP >= MAXCOMPLEXES) return -1;

/* Prepare the index of the new macromolecular complex: */
new_mol_complexI = *mol_complexesNP;

/* Prepare the pointer to macromolecular complex: */
new_mol_complexSP = mol_complexSP + new_mol_complexI;

/* Read file (if 0 returned, file is not specified): */
if (ReadFile_ (new_mol_complexSP, mol_file_nameP, configSP) <= 0) return -2;

/* Store the file name: */
strncpy (new_mol_complexSP->file_nameA, mol_file_nameP, STRINGSIZE - 1);
new_mol_complexSP->file_nameA[STRINGSIZE - 1] = '\0';

/* Set the unique  macromolecular complex  identifier. */
/* This ID wouldn't change even if some macromolecular */
/* complexes are loaded or discarded.  The array index */
/* of a given  complex  may change  after something is */
/* discarded so it should  not be used  as identifier. */
/* The first mol. complex ID is equal to 1 (garlic.c). */
new_mol_complexID = *next_mol_complexIDP;
new_mol_complexSP->mol_complexID = new_mol_complexID;

/* Update the next macromol. complex identifier: */
(*next_mol_complexIDP)++;

/* Update the number of macromolecular complexes: */
(*mol_complexesNP)++;

/* Initialize the move bits: */
new_mol_complexSP->move_bits =	STRUCTURE_MASK |
				PLANE_MASK |
				MEMBRANE_MASK |
				ENVELOPE_MASK;

/* Find geometric center: */
GeometricCenter_ (new_mol_complexSP);

/* Flip vector (multiply by -1): */
FlipVector_ (&shift_vectorS, &new_mol_complexSP->geometric_center_vectorS);

/* Translate geometric  center of the complex to */
/* the origin of the absolute coordinate system: */
TranslateComplex_ (new_mol_complexSP, &shift_vectorS, configSP, 0);

/* Initial rotation center position: */
new_mol_complexSP->rotation_center_vectorS.x = 0.0;
new_mol_complexSP->rotation_center_vectorS.y = 0.0;
new_mol_complexSP->rotation_center_vectorS.z = 0.0;

/* Spatial extend of the whole macromolecular complex: */
ComplexExtent_ (new_mol_complexSP);

/* Initialize the slab mode (use the current default slab mode): */
new_mol_complexSP->slab_modeI = configSP->default_slab_modeI;

/* Initialize slab: */
InitSlab_ (new_mol_complexSP);

/* Initialize the color fading mode (use the current default fading mode): */
new_mol_complexSP->fading_modeI = configSP->default_fading_modeI;

/* Initialize color fading parameters: */
InitFading_ (new_mol_complexSP);

/* Prepare the stereo data (coordinates for the right image): */
if (configSP->stereoF) PrepareStereoData_ (new_mol_complexSP, configSP);

/* Prepare bonds; use inter-atomic */
/* distances  for  identification: */
StrongBonds_ (new_mol_complexSP, new_mol_complexI, configSP);

/* Generate pseudo-bonds (bonds formed by imaginary Q atoms): */
PseudoBonds_ (new_mol_complexSP, new_mol_complexI, configSP);

/* Prepare disulfide bonds: */
DisulfideBonds_ (new_mol_complexSP, new_mol_complexI, configSP);

/* Prepare  backbone  data - allocate  and */
/* fill the array of BackboneS structures: */
PrepareBackbone_ (new_mol_complexSP, configSP);

/* Assign radius to each atom: */
AssignRadii_ (new_mol_complexSP, configSP);

/* Assign default hydrophobicity to each atom: */
AssignHydrophobicity_ (new_mol_complexSP, default_hyphob_scaleI);

/* Extract sequence information: */
ExtractSequence_ (new_mol_complexSP);

/* Calculate dihedral angles and cis_trans flags: */
DihedralAngles_ (new_mol_complexSP, configSP);

/* Allocate and initialize the storage for the secondary structure: */
if (AllocSecondaryStructure_ (new_mol_complexSP) < 0) return -3;

/*@@*/ /* Generate the secondary structure information: */

/* Generate the plane (represented by an ellipse): */
GeneratePlane_ (new_mol_complexSP, configSP, guiSP);

/* Initialize data required to draw the membrane: */
InitializeMembrane_ (new_mol_complexSP, configSP, guiSP);

/* Initialize the flag reserved for hydrogen bonds: if value */
/* is equal to zero, hydrogen bonds are missing or obsolete. */
new_mol_complexSP->hydrogen_bondsF = 0;

/* The hydrogen bonds are initially invisible: */
new_mol_complexSP->hydrogen_bonds_hiddenF = 1;

/* The membrane is initially undefined: */
new_mol_complexSP->membraneS.definedF = 0;

/* Set the probe radius (copy default value): */
new_mol_complexSP->bond_probe_radius = configSP->default_bond_probe_radius;

/* Set the stick radius (used to draw bonds as sticks; copy default value): */
new_mol_complexSP->stick_radius = configSP->default_stick_radius;

/* Set the group_memberF to zero: */
new_mol_complexSP->group_memberF = 0;

/* If a group of structures was formed before, break it: */
*groupFP = 0;

/* If some structures were involved in a formation of a */
/* group, restore the original rotation center vectors: */
for (mol_complexI = 0; mol_complexI < *mol_complexesNP; mol_complexI++)
	{
	/* Prepare the pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check was this complex a group member: */
	if (curr_mol_complexSP->group_memberF == 0) continue;

	/* If this point is reached, the current complex was a group member. */

	/* Restore the original rotation center vector: */
	curr_mol_complexSP->rotation_center_vectorS.x =
				curr_mol_complexSP->backup_vectorS.x;
	curr_mol_complexSP->rotation_center_vectorS.y =
				curr_mol_complexSP->backup_vectorS.y;
	curr_mol_complexSP->rotation_center_vectorS.z =
				curr_mol_complexSP->backup_vectorS.z;

	/* Reset the group_memberF: */
	curr_mol_complexSP->group_memberF = 0;
	}

/* Set  the catch flag  to indicate  that  the new */
/* complex is the only one  which is caught,  i.e. */
/* affected by movement, slab and fading controls: */
for (mol_complexI = 0; mol_complexI < *mol_complexesNP; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* The catch flag for the new complex should be set to one: */
	if (curr_mol_complexSP->mol_complexID == new_mol_complexID)
		{
		curr_mol_complexSP->catchF = 1;
		}

	/* All other catch flags should be set to zero: */
	else curr_mol_complexSP->catchF = 0;
	}

/* Return the updated number of macromolecular complexes on success: */
return *mol_complexesNP;
}

/*===========================================================================*/


