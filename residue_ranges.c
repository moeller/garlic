/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

			    residue_ranges.c

Purpose:
	Extract residue ranges (serial numbers) from the list.

Input:
	(1) Pointer to SelectS structure, where data will be stored.
	(2) Pointer to the string with list of ranges.

Output:
	(1) Some data added to SelectS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractSerials_ (int *, int *, char *);

/*======extract residue ranges:==============================================*/

int ExtractResidueRanges_ (SelectS *selectSP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
int		max;
int		n1, n2;
int		rangeI;

/* Check the string length: */
if (strlen (stringP) == 0) return -1;

/* Initialize all_residue_serialF (1 = select all serial numbers): */
selectSP->all_residue_serialF = 0;

/* Initialize the number of ranges: */
selectSP->residue_serial_rangesN = 0;

/* If wildcard (asterisk) is present, set */
/* all_residue_serialF to one and return: */
if (strstr (stringP, "*"))
	{
	selectSP->all_residue_serialF = 1;
	return 1;
	}

/* Parse the list of ranges: */
remainderP = stringP;
max = SHORTSTRINGSIZE;
while ((remainderP = ExtractToken_ (tokenA, max, remainderP, " \t,;")) != NULL)
	{
	/** Extract serial numbers: **/
	if (ExtractSerials_ (&n1, &n2, tokenA) < 0) return -2;

	/** Copy serial numbers: **/
	rangeI = selectSP->residue_serial_rangesN;
	selectSP->residue_serial_start[rangeI] = n1;
	selectSP->residue_serial_end[rangeI]   = n2;

	/** Increase and check the number of ranges: **/
	selectSP->residue_serial_rangesN++;
	if (selectSP->residue_serial_rangesN >= MAXFIELDS) break;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


