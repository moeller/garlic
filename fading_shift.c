/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				fading_shift.c

Purpose:
	Prepare the fading shift.  By default, the normal fading shift is
	used  (fading_shiftA[2] from  ConfigS structure).  See note 1 for
	all combinations of modifier keys.

Input:
	(1) Pointer to ConfigS structure, with configuration data.
	(2) Pointer to GUIS structure, with GUI data.
	(3) The sign used to distinguish positive and negative shift.

Output:
	(1) Return value.

Return value:
	(1) Fading shift.

Notes:
	(1) The following  combinations  of modifier keys  may be used to
	    select the fading shift:
	    -------------------------------------------------------------
	    | modifiers           fading shift        ConfigS member    |
	    |-----------------------------------------------------------|
	    | none                normal (default)    fading_stepA[2]   |
	    | shift               large               fading_stepA[3]   |
	    | alt+shift           very large          fading_stepA[4]   |
	    | control             small               fading_stepA[1]   |
	    | alt+control         very small          fading_stepA[0]   |
	    | control+shift       normal              fading_stepA[2]   |
	    | alt                 normal              fading_stepA[2]   |
	    | alt+shift+control   normal              fading_stepA[2]   |
	    -------------------------------------------------------------
	    Note that some modifier combinations are ignored  (alt alone,
	    control+shift and alt+control+shift).

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======prepare fading shift:================================================*/

double FadingShift_ (ConfigS *configSP, GUIS *guiSP, double sign)
{
double		fading_shift;
int		largeF, smallF, altF;

/* Copy the flags: */
largeF = guiSP->shift_pressedF;
smallF = guiSP->control_pressedF;
altF   = guiSP->alt_pressedF;

/* Select the requested fading shift: */
if (altF)
	{
	if (largeF && !smallF)
		{
		fading_shift = configSP->fading_stepA[4];
		}
	else if (smallF && !largeF)
		{
		fading_shift = configSP->fading_stepA[0];
		}
	else
		{
		fading_shift = configSP->fading_stepA[2];
		}
	}
else
	{
	if (largeF && !smallF)
		{
		fading_shift = configSP->fading_stepA[3];
		}
	else if (smallF && !largeF)
		{
		fading_shift = configSP->fading_stepA[1];
		}
	else if (smallF && largeF)
		{
		fading_shift = configSP->fading_stepA[0];
		}
	else
		{
		fading_shift = configSP->fading_stepA[2];
		}
	}

/* Take care for the sign: */
fading_shift *= sign;

/* Return the fading shift: */
return fading_shift;
}

/*===========================================================================*/


