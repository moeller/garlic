/* Copyright (C) 2006 Damir Zucic */

/*=============================================================================

			    hyphob_function7.c

Purpose:
	Draw the hydrophobicity function F7.  The sequence stored to the
	main sequence buffer  is used  to calculate  the function value.

Input:
	(1) Pointer to the storage where the minimal function value will
	    be stored.
	(2) Pointer to the storage where the maximal function value will
	    be stored.
	(3) Pointer to RuntimeS structure.

Output:
	(1) Function F7 calculated and stored.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) The function  F7 may be modified and used for many purposes.
	    Originally, it was introduced while searching for the method
	    which will be suitable for prediction of the porin secondary
	    structure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		HyphobFunction1_ (double *, double *, RuntimeS *);
int		HyphobFunction2_ (double *, double *, RuntimeS *);
int		HyphobFunction3_ (double *, double *, RuntimeS *);
int		HyphobFunction4_ (double *, double *, RuntimeS *);
int		HyphobFunction5_ (double *, double *, RuntimeS *);
int		HyphobFunction6_ (double *, double *, RuntimeS *);
int		HyphobFunction7_ (double *, double *, RuntimeS *);
void		InitHyphob_ (RuntimeS *);

/*======calculate the hydrophobicity function F7:============================*/

int HyphobFunction7_ (double *f7minP, double *f7maxP, RuntimeS *runtimeSP)
{
int		residuesN, residueI;   /* Do not use size_t instead of int ! */
double		f1, f1min, f1max;
double		f4, f4min, f4max;
double		f7;
double		average_value;
int		windowI, combinedI;
double		weightA[5] = {1.00, 1.00, 1.00, 1.00, 1.00};

/*------prepare some parameters:---------------------------------------------*/

/* The number of residues in the sequence buffer: */
residuesN = (int) runtimeSP->residuesN;
if (residuesN == 0) return -1;

/*------initialize (reset) F7:-----------------------------------------------*/

/* Initialize F7: */
for (residueI = 0; residueI < residuesN; residueI++)
        {
        /* Reset the function F7, it might be initialized before: */
        *(runtimeSP->function7P + residueI) = 0.0;
        }

/*------calculate the function F1:-------------------------------------------*/

/* Calculate the function F1: */
HyphobFunction5_ (&f1min, &f1max, runtimeSP);

/*------calculate the function F4:-------------------------------------------*/

/* Calculate the function F4: */
HyphobFunction6_ (&f4min, &f4max, runtimeSP);

/*------calculate the function F7:-------------------------------------------*/

/* Initialize the extreme values: */
*f7minP = +999999.0;
*f7maxP = -999999.0;

/* Scan the sequence and calculate F7: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Initialize the function value: */
	*(runtimeSP->function7P + residueI) = 0.0;

	/* 20060607.1258: */
	/* F7[i] = F1[i] - F4[i] */

	f1 = *(runtimeSP->function1P + residueI);
	f4 = *(runtimeSP->function4P + residueI);
	f7 = f1 - f4;

	/* Store the function value: */
	*(runtimeSP->function7P + residueI) = f7;
	}

/*------calculate the weighted average F7 value, over 5 residues:------------*/

#ifdef ALLDONE /*@@*/

for (residueI = 3; residueI < residuesN - 3; residueI++)
	{
	/* Reset the average value: */
	average_value = 0.0;

	/* Scan the sliding window: */
	for (windowI = 0; windowI < 5; windowI++)
		{
		/* Prepare and check the combined index: */
		combinedI = residueI + windowI - 2;
		if (combinedI < 0) continue;
		if (combinedI >= residuesN) continue;

		/* Copy the function value: */
		f7 = *(runtimeSP->function7P + combinedI);

		/* Add the weighted function value to the average value: */
		average_value += weightA[windowI] * f7;
		}

	/* Calculate and store the average value of F7: */
	average_value /= 5.0;		 /* Divide by 5 */
	*(runtimeSP->function7P + residueI) = average_value;
	}

#endif /*@@*/

/*------find the extreme values of F7:---------------------------------------*/

/* Find the extreme values of F7: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Find extreme values for F7: */
	f7 = *(runtimeSP->function7P + residueI);
	if (f7 < *f7minP) *f7minP = f7;
	if (f7 > *f7maxP) *f7maxP = f7;
	}

/*---------------------------------------------------------------------------*/

return 1;
}

/*===========================================================================*/


