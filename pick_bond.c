/* Copyright (C) 2001-2002 Damir Zucic */

/*=============================================================================

				pick_bond.c

Purpose:
	Pick bond for editing.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  NearestAtomS structure, which contains data about
	    the choosen pixel in the main window working area.

Output:
	(1) The edit mode index may be changed.  Some members of RuntimeS
	    structure may be set.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		IsPartOfRing_ (MolComplexS *, int, size_t, size_t);
int		InputRefresh_ (GUIS *, RuntimeS *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======handle ButtonPress events:===========================================*/

int PickBond_ (MolComplexS *mol_complexSP, int mol_complexesN,
	       RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	       NearestAtomS *nearest_atomSP, size_t pixelsN,
	       unsigned int *refreshIP, NearestAtomS *curr_pixelSP)
{
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN;
size_t			atom1_arrayI, atom2_arrayI;
int			bondI;
AtomS			*atom1SP, *atom2SP;
TrueBondS		*curr_bondSP;
int			atom1_serialI, atom2_serialI;
int			n;
char			*atom1_pure_nameP, *atom2_pure_nameP;

/* Check  the main window  drawing mode index (it */
/* was checked in the caller, but I am paranoic): */
if (guiSP->main_window_modeI != 0) return -1;

/* Copy and check the index of the macromolecular complex: */
mol_complexI = curr_pixelSP->mol_complexI;
if ((mol_complexI < 0) || (mol_complexI >= mol_complexesN)) return -2;

/* Prepare the pointer to the choosen macromolecular complex: */
curr_mol_complexSP = mol_complexSP + mol_complexI;

/* Copy and check the number of atoms in the choosen macromolecular complex: */
atomsN = curr_mol_complexSP->atomsN;
if (atomsN <= 0) return -3;

/* Copy and check the array index of the first atom: */
atom1_arrayI = curr_pixelSP->atomI;
if (atom1_arrayI >= atomsN) return -4;

/* Copy and check the bond index: */
bondI = curr_pixelSP->bondI;
if ((bondI < 0) || (bondI >= MAXBONDS)) return -5;

/* Prepare the pointer to the first atom: */
atom1SP = curr_mol_complexSP->atomSP + atom1_arrayI;

/* Prepare the pointer to the choosen bond: */
curr_bondSP = atom1SP->true_bondSA + bondI;

/* If this is not a covalent bond, ignore the click: */
if (curr_bondSP->bond_typeI != 1)
	{
	strcpy (runtimeSP->messageA, "This is not an ordinary covalent bond!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	InputRefresh_ (guiSP, runtimeSP);
	return -6;
	}

/* The second atom should belong to the same complex: */
if (curr_bondSP->neighbor_mol_complexI != mol_complexI) return -7;

/* Copy and check the array index of the second atom: */
atom2_arrayI = curr_bondSP->neighbor_arrayI;
if (atom2_arrayI >= atomsN) return -8;

/* Prepare the pointer to the second atom: */
atom2SP = curr_mol_complexSP->atomSP + atom2_arrayI;

/* Copy and compare the serial numbers of both atoms: */
atom1_serialI = atom1SP->raw_atomS.serialI;
atom2_serialI = atom2SP->raw_atomS.serialI;
if (atom1_serialI == atom2_serialI) return -9;

/* Check is it possible to rotate the specified bond. It */
/* is not possible to edit  the bond  which is part of a */
/* ring or some other closed structural element. If bond */
/* belongs to a ring  the return value will be positive. */
n = IsPartOfRing_ (curr_mol_complexSP, mol_complexI,
		   atom1_arrayI, atom2_arrayI);
if (n > 0)
	{
	strcpy (runtimeSP->messageA, "This bond belongs to a ring!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	InputRefresh_ (guiSP, runtimeSP);
	return -10;
	}

/* Store the serial numbers and the array indices: */
runtimeSP->atom1_serialI = atom1_serialI;
runtimeSP->atom2_serialI = atom2_serialI;
runtimeSP->atom1_arrayI  = atom1_arrayI;
runtimeSP->atom2_arrayI  = atom2_arrayI;

/* Prepare the purified names for two given atoms: */
atom1SP = curr_mol_complexSP->atomSP + atom1_arrayI;
atom2SP = curr_mol_complexSP->atomSP + atom2_arrayI;
atom1_pure_nameP = atom1SP->raw_atomS.pure_atom_nameA;
atom2_pure_nameP = atom2SP->raw_atomS.pure_atom_nameA;

/* If two atoms form the  N-CA bond,  the current command */
/* is equivalent to EDIT PHI. If they form the CA-C bond, */
/* the command  is equivalent to  EDIT PSI.  If they form */
/* the C-N bond, the command is equivalent to EDIT OMEGA. */

/* The first combination equivalent to EDIT PHI: */
if ((strcmp (atom1_pure_nameP, "N" ) == 0) &&
    (strcmp (atom2_pure_nameP, "CA") == 0))
	{
	runtimeSP->edit_modeI = 2;
	runtimeSP->edit_single_bondF = 1;
	}

/* The second combination equivalent to EDIT PHI: */
else if ((strcmp (atom1_pure_nameP, "CA") == 0) &&
	 (strcmp (atom2_pure_nameP, "N" ) == 0))
	{
	runtimeSP->edit_modeI = 2;
	runtimeSP->edit_single_bondF = 1;
	}

/* The first combination equivalent to EDIT PSI: */
else if ((strcmp (atom1_pure_nameP, "CA") == 0) &&
	 (strcmp (atom2_pure_nameP, "C" ) == 0))
	{
	runtimeSP->edit_modeI = 3;
	runtimeSP->edit_single_bondF = 1;
	}

/* The second combination equivalent to EDIT PSI: */
else if ((strcmp (atom1_pure_nameP, "C" ) == 0) &&
	 (strcmp (atom2_pure_nameP, "CA") == 0))
	{
	runtimeSP->edit_modeI = 3;
	runtimeSP->edit_single_bondF = 1;
	}

/* The first combination equivalent to EDIT OMEGA: */
else if ((strcmp (atom1_pure_nameP, "C") == 0) &&
	 (strcmp (atom2_pure_nameP, "N") == 0))
	{
	runtimeSP->edit_modeI = 4;
	runtimeSP->edit_single_bondF = 1;
	}

/* The second combination equivalent to EDIT OMEGA: */
else if ((strcmp (atom1_pure_nameP, "N") == 0) &&
	 (strcmp (atom2_pure_nameP, "C") == 0))
	{
	runtimeSP->edit_modeI = 4;
	runtimeSP->edit_single_bondF = 1;

	/* Swap the array and serial indices. The second array */
	/* index  will be used in  small_omega.c  to recognize */
	/* the residue  to which  the nitrogen  atom  belongs: */
	runtimeSP->atom2_arrayI = atom1_arrayI;
	runtimeSP->atom1_arrayI = atom2_arrayI;
	runtimeSP->atom1_serialI = atom2_serialI; 
	runtimeSP->atom2_serialI = atom1_serialI;
	}

/* All  other combinations  are  not  equivalent to */
/* special cases EDIT PHI, EDIT PSI and EDIT OMEGA: */
else
	{
	runtimeSP->edit_modeI = 6;
	runtimeSP->edit_single_bondF = 1;
	}

/* Refresh the input window: */
strcpy (runtimeSP->messageA, "");
runtimeSP->message_length = strlen (runtimeSP->messageA);
InputRefresh_ (guiSP, runtimeSP);

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


