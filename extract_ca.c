/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				extract_ca.c

Purpose:
	Extract coordinates of the CA atom for a given residue.

Input:
	(1) Pointer to VectorS structure, where CA coord. will be stored.
	(2) Pointer to AtomS structure,  pointing to the first element of
	    the atomic array.
	(3) Index of the first atom of a given residue.
	(4) Index of the last atom of a given residue.

Output:
	(1) VectorS structure filled with data.
	(2) Return value.

Return value:
	(1) The number of  successfully extracted vectors  (zero or one).

Notes:
	(1) Some files contain more than one entry for some atoms. Garlic
	    uses only the first entry and  discards other entries for the
	    same atom.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract CA coordinates:==============================================*/

int ExtractCA_ (VectorS *CA_vectorSP,
		AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
int		vectors_extractedN = 0;
size_t		atomI;
AtomS		*curr_atomSP;

/* Scan the given residue: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* CA: */
	if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, "CA") == 0)
		{
		CA_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		CA_vectorSP->y = curr_atomSP->raw_atomS.y;
		CA_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		break;
		}
	}

/* Return the number of extracted vectors: */
return vectors_extractedN;
}

/*===========================================================================*/


