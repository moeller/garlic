/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				refresh_pixmap.c

Purpose:
	Refresh pixmap (fill with given color).

Input:
	(1) Display structure pointer.
	(2) Pixmap ID.
	(3) Graphics context (GC).
	(4) Width of the area to be refreshed (left justified).
	(5) Height of the area to be refreshed (top justified).
	(6) Color ID.

Output:
	(1) Pixmap refreshed (filled with uniform color).
	(2) Return value.

Return value:
	(1) Always positive (trivial).

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======refresh pixmap (fill with given color):==============================*/

int RefreshPixmap_ (Display *displaySP, Pixmap pixmapID, GC theGC,
		    unsigned int width, unsigned int height,
		    unsigned long colorID)
{
XSetForeground (displaySP, theGC, colorID);

XFillRectangle (displaySP, pixmapID, theGC, 0, 0, width, height);

return 1;
}

/*===========================================================================*/


