/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				free_runtime.c

Purpose:
	Free memory used to store some runtime data.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) Memory used to store some runtime data freed.
	(2) Return value.

Return value:
	Positive always (trivial).

========includes:============================================================*/

#include <stdio.h>

#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======initialize runtime parameters:=======================================*/

int FreeRuntime_ (RuntimeS *runtimeSP)
{

/* Free memory used for command strings (the history buffer): */
if (runtimeSP->commandsP)		free (runtimeSP->commandsP);
if (runtimeSP->titlesP)			free (runtimeSP->titlesP);
if (runtimeSP->sequenceP)		free (runtimeSP->sequenceP);
if (runtimeSP->patternP)		free (runtimeSP->patternP);
if (runtimeSP->sec_structureP)		free (runtimeSP->sec_structureP);
if (runtimeSP->disulfideFP)		free (runtimeSP->disulfideFP);
if (runtimeSP->serialIP)		free (runtimeSP->serialIP);
if (runtimeSP->residue_codeIP)		free (runtimeSP->residue_codeIP);
if (runtimeSP->hydrophobicityP)		free (runtimeSP->hydrophobicityP);

if (runtimeSP->average_hydrophobicityP)
				free (runtimeSP->average_hydrophobicityP);
if (runtimeSP->weighted_hydrophobicityP)
				free (runtimeSP->weighted_hydrophobicityP);
if (runtimeSP->hydrophobic_momentP)
				free (runtimeSP->hydrophobic_momentP);
if (runtimeSP->larger_sided_hyphobP)
				free (runtimeSP->larger_sided_hyphobP);
if (runtimeSP->smaller_sided_hyphobP)
				free (runtimeSP->smaller_sided_hyphobP);

if (runtimeSP->function1P)		free (runtimeSP->function1P);
if (runtimeSP->function2P)		free (runtimeSP->function2P);
if (runtimeSP->function3P)		free (runtimeSP->function3P);
if (runtimeSP->function4P)		free (runtimeSP->function4P);
if (runtimeSP->function5P)		free (runtimeSP->function5P);
if (runtimeSP->function6P)		free (runtimeSP->function6P);
if (runtimeSP->function7P)		free (runtimeSP->function7P);

if (runtimeSP->auxiliaryIP)		free (runtimeSP->auxiliaryIP);
if (runtimeSP->aux_doubleP)		free (runtimeSP->aux_doubleP);

if (runtimeSP->reference_sequenceP)
				free (runtimeSP->reference_sequenceP);
if (runtimeSP->reference_sec_structureP)
				free (runtimeSP->reference_sec_structureP);
if (runtimeSP->reference_serialIP)
				free (runtimeSP->reference_serialIP);
if (runtimeSP->reference_residue_codeIP)
				free (runtimeSP->reference_residue_codeIP);
if (runtimeSP->reference_hydrophobicityP)
				free (runtimeSP->reference_hydrophobicityP);

if (runtimeSP->exposed_atom1IP)		free (runtimeSP->exposed_atom1IP);
if (runtimeSP->exposed_atom2IP)		free (runtimeSP->exposed_atom2IP);
if (runtimeSP->exposed_polar1SP)	free (runtimeSP->exposed_polar1SP);
if (runtimeSP->exposed_polar2SP)	free (runtimeSP->exposed_polar2SP);
if (runtimeSP->template_atomSP)         free (runtimeSP->template_atomSP);
if (runtimeSP->template_residueSP)      free (runtimeSP->template_residueSP);
if (runtimeSP->beta_cellSP)             free (runtimeSP->beta_cellSP);

/* Return positive value (trivial): */
return 1;
}

/*===========================================================================*/


