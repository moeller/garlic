/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				errmsg.c

Purpose:
	Print error message.

Input:
	(1) Program name.
	(2) Name of the function where error occured.
	(3) Name of the file which caused trouble, if file caused trouble.
	(4) \
	(5)  \   ... Error message, split into four strings.
	(6)  /       Don't forget to put newline somewhere!
	(7) /

Output:
	(1) Error message written to stderr.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>

/*======print error message:=================================================*/

/* Note: there is no newline in the last four fprintf! */

void ErrorMessage_ (char *module, char *function, char *filename,
		    char *s1, char *s2, char *s3, char *s4)
{
fprintf (stderr, "\nERROR>");
fprintf (stderr, " module: %s, function: %s", module, function);
if (strlen (filename) != 0) fprintf (stderr, ", file: %s", filename);
fprintf (stderr, "\n");
if (strlen (s1) != 0) fprintf (stderr, "%s", s1);
if (strlen (s2) != 0) fprintf (stderr, "%s", s2);
if (strlen (s3) != 0) fprintf (stderr, "%s", s3);
if (strlen (s4) != 0) fprintf (stderr, "%s", s4);
}

/*===========================================================================*/


