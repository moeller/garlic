
			Garlic Installation

Garlic is the most portable molecular viewer/editor in the unix world,
because the only libraries required by this program are the standard
C library (libc), the standard math library (libm) and the standard
X library (libX11). All other molecular visualization programs require
some additional libraries.

Before starting the installation, you might be interested to check the
technical requirements. Most contemporary unix and unix-like systems
(like linux) meet these requirements without need to change the
configuration, so you can skip this text if you are impatient.

If you have Apple McIntosh running Xtools from Tenon, please read the hints
contributed by Chris from Macinchem:

http://garlic.mefos.hr/garlic-1.6/install/macosx.html

To compile and install garlic, follow these instructions:

 (1) Put garlic package (garlic-1.6.tar.gz) into some empty directory.

 (2) Uncompress and unpack the package:
     gzip -d garlic-1.6.tar.gz
     tar xf garlic-1.6.tar

 (3) Change your working directory:
     cd garlic-1.6

 (4) Check which C compiler is available on your system.
     If you don't know how to find this information, try this:
     man -k compiler
     to obtain the list of available compilers.

 (5) If gcc (GNU C) compiler is not available on your system,
     use your favorite editor to edit Makefile. Replace gcc in:
     CC = gcc
     line with the name of your compiler (this may be cc, for example).
     If gcc is available on your system, do not change the CC line.

 (6) Find a directory which contains the Xlib library. Try this:
     cd /
     find . -name libX11\*
     Check where is the file libX11.so (sharable library - prefered)
     or libX11.a (static library - reserve option).
     If your directory is different from /usr/X11R6/lib, modify the line:
     LIBPATH = -L/usr/X11R6/lib
     in Makefile. For example, if libX11.so is stored in /usr/shlib
     directory on your system, the LIBPATH should be changed to:
     LIBPATH = -L/usr/shlib

 (7) Compile the package - just type:
     make
     and wait, it will take some time.

 (8) If everything worked fine, copy the executable to a choosen directory.
     /usr/local/bin may be a good choice. This job should be done by root
     (system administrator):
     cp garlic /usr/local/bin
     If you don't have root privileges, ask your local administrator to
     install garlic, or just leave it in one of your directories.

 (9) Now the .garlicrc configuration file and the file residues.pdb, which
     contains the template residues should be copied to a directory where
     these files will be accessible to everyone. If this is not done, garlic
     will work anyway, using hard-coded default configuration, but you will
     be unable to replace residues and to create peptides.
     The recommended directory is /usr/share/garlic but if /usr/share
     directory does not exist on your system, /usr/local/lib/garlic may be
     used. This should be done by root (system administrator):
     mkdir /usr/share/garlic
     cp .garlicrc /usr/share/garlic
     cp residues.pdb /usr/share/garlic

(10) It may be good idea to copy the configuration file to your home
     directory or to the subdirectory garlic of your home directory.
     The same may be done with the file residues.pdb. If you do this,
     you will be able to personalize your garlic sessions, because your
     private files will override the system-wide defaults. Of course,
     you don't need root privileges to do this:
     cp .garlicrc $HOME
     cp residues.pdb $HOME
     or:
     mkdir $HOME/garlic
     cp .garlicrc $HOME/garlic
     cp residues.pdb $HOME/garlic

(11) It is recommended to set the environment variable MOL_PATH, so garlic
     will search all directories listed in this variable to find the
     specified file. Detailed instructions may be found at:

     http://garlic.mefos.hr/garlic-1.6/install/mol_path.html

(12) Clean up the mess (sources and .o files). Be sure to leave all .pdb
     files for later practice. Leave also the files with the extension
     .script, these are some garlic tutorial scripts.

If garlic refuses to work because it fails to get the TrueColor visual,
try to execute xdpyinfo and xwininfo (click into the root window); check
the list of supported visuals and the current color depth. Choose the color
depth for which the TrueColor visual is available. If you have the SGI
Octane workstation, please read the text, contributed by Randal R. Ketchem:

http://garlic.mefos.hr/garlic-1.6/install/octane.html

If you have IBM RS6000 with AIX, read the hint, contributed by Nicolas Ferre:

http://garlic.mefos.hr/garlic-1.6/install/rs6000.html

The list of sites with proteins, nucleic acids and hetero compounds may be
found at:

http://garlic.mefos.hr/garlic-1.6/data/index.html

If you can't compile, install or run garlic, write to me (Damir Zucic),
my address is zucic@mefos.hr .


