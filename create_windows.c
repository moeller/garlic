/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				create_windows.c

Purpose:
	Prepare pixmaps,  hints,  cursors and some  other data.  After that,
	create windows, select events and map all windows.

Input:
	(1) Icon pixmap included as garlic.xpm file.
	(2) Control pixmap included as control.xpm file.
	(3) Pointer to GUIS structure. Some data will be used as input data,
	    some other will be added.
	(4) Pointer to ConfigS structure (input only).
	(5) Number of command line arguments.
	(6) Command line arguments.

Output:
	(1) Some data stored to GUIS structure.
	(2) Pixmaps created.
	(3) Main, icon and control window created.
	(4) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) The width and  height of  the main window free area  are used in
	    this function and in some other functions. To prepare the width,
	    it is necessary to know the control window width.  This width is
	    available  after  the control pixmap  is created. To prepare the
	    height, it is necessary to know the input window height.

	(2) There are two functions which are used to make pixmap. The first
	    function  (MakePixmap_, the older version)  may be to create and
	    draw pixmap which is  associated with some window and the second
	    function  (MakePixmap2_)  is used to create  hidden pixmap which
	    is not associated with any window.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

#include "garlic.xpm"
#include "control.xpm"
#include "small_arrows.xpm"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);
int		MakePixmap_ (WindowS *, GUIS *, char **);
int		MakePixmap2_ (Pixmap *, unsigned int *, unsigned int *,
			      GUIS *, char **);
void		SetSizeHints_ (GUIS *, ConfigS *);
void		SetWMHints_ (GUIS *);
void		SelectEvents_ (GUIS *);
void		HideControlWindow_ (GUIS *);

/*======create windows (main, icon, control, input and output):==============*/

int CreateWindows_ (GUIS *guiSP, ConfigS *configSP, int argc, char **argv)
{
int			n;
XSetWindowAttributes	main_attribS, icon_attribS, control_attribS,
			input_attribS, output_attribS;
unsigned long		main_value_mask, icon_value_mask, control_value_mask,
			input_value_mask, output_value_mask;
int			border_width = 1;               /* For all windows! */
static char		main_win_titleA[SHORTSTRINGSIZE];
static char		icon_win_titleA[SHORTSTRINGSIZE] = "garlic";
char			*main_win_titleP, *icon_win_titleP;
XTextProperty		main_win_titleS, icon_win_titleS;
static char		prog_nameA[SHORTSTRINGSIZE] = "garlic";
static char		class_nameA[SHORTSTRINGSIZE] = "Garlic";
static XClassHint	class_hintS;
Window			rootID;
int			geom_returnF;
int			x0, y0;
static unsigned int	main_wx, main_wy, main_bw, bpp;

/*------prepare the main window title:---------------------------------------*/

strcpy (main_win_titleA, "GARLIC 1.6 - Copyright (C) 2000-2006 Damir Zucic");

/*------main window:---------------------------------------------------------*/

/* Set size hints for main window: */
SetSizeHints_ (guiSP, configSP);

/* Set main window attributes: */
main_attribS.border_pixel = guiSP->main_winS.fg_colorID;
main_attribS.background_pixel = guiSP->main_winS.bg_colorID;
main_attribS.cursor = guiSP->main_winS.cursorID;
main_attribS.colormap = guiSP->colormapID;
main_value_mask = CWBorderPixel | CWBackPixel | CWCursor | CWColormap;

/* Copy the border width: */
guiSP->main_winS.border_width = border_width;

/* Create the main window (parent is root): */
guiSP->main_winS.ID = XCreateWindow (
		guiSP->displaySP, DefaultRootWindow (guiSP->displaySP),
		guiSP->main_winS.x0, guiSP->main_winS.y0,
		guiSP->main_winS.width, guiSP->main_winS.height,
		border_width, guiSP->depth,
		InputOutput, guiSP->visualSP,
		main_value_mask, &main_attribS);

/* Set the window colormap (is this really necessary?): */
XSetWindowColormap (guiSP->displaySP, guiSP->main_winS.ID, guiSP->colormapID);

/*------enable "delete window" command:--------------------------------------*/

/* Enable this program to trap  "delete window"  command; */
/* this has something to do with win. manager properties. */
/* Try:       strings window_manager_name | grep WM_      */
/* The window manager is  adding  decoration to  the main */
/* window;  the command "Delete" may be  available in the */
/* small popup-window  in the top left corner.  The lines */
/* below should take care that this message is processed. */
guiSP->delete_window_atom =  XInternAtom(guiSP->displaySP,
					 "WM_DELETE_WINDOW", False);
guiSP->protocols_atom = XInternAtom(guiSP->displaySP, "WM_PROTOCOLS", False);
XChangeProperty(guiSP->displaySP, guiSP->main_winS.ID, guiSP->protocols_atom,
		XA_ATOM, 32, PropModeReplace,
		(unsigned char *) &guiSP->delete_window_atom, 1);

/*------icon window:---------------------------------------------------------*/

/* Prepare icon pixmap: */
n = MakePixmap_ (&guiSP->icon_winS, guiSP, garlic_xpm);
if (n < 0) return -1;

/* Set icon window attributes: */
icon_attribS.border_pixel = BlackPixel (guiSP->displaySP, guiSP->screenID);
icon_attribS.background_pixmap = guiSP->icon_winS.pixmapID;
icon_attribS.colormap = guiSP->colormapID;
icon_value_mask = CWBorderPixel | CWBackPixmap | CWColormap;
guiSP->icon_winS.x0 = 500;
guiSP->icon_winS.y0 = 50;
guiSP->icon_winS.border_width = border_width;

/* Create the icon window (parent is root here also): */
guiSP->icon_winS.ID = XCreateWindow (
		guiSP->displaySP, DefaultRootWindow (guiSP->displaySP),
		guiSP->icon_winS.x0, guiSP->icon_winS.y0,
		guiSP->icon_winS.width, guiSP->icon_winS.height,
		border_width, guiSP->depth,
		InputOutput, guiSP->visualSP,
		icon_value_mask, &icon_attribS);

/* Set the window colormap (is this necessary?): */
XSetWindowColormap (guiSP->displaySP, guiSP->icon_winS.ID, guiSP->colormapID);

/*------control pixmap:------------------------------------------------------*/

/* Prepare control pixmap (from incl. data) and control window dimensions: */
n = MakePixmap_ (&guiSP->control_winS, guiSP, control_xpm);
if (n < 0) return -2;

/*------input window height:-------------------------------------------------*/

/* Set input window height: */
guiSP->input_winS.height = 2 * guiSP->input_winS.text_line_height +
			       guiSP->input_winS.quarter_font_height;

/*------main window free area width and height:------------------------------*/

/* Get the main window geometry: */
geom_returnF = XGetGeometry(guiSP->displaySP, guiSP->main_winS.ID, &rootID,
			    &x0, &y0, &main_wx, &main_wy, &main_bw, &bpp);

/* Prepare the main window free area width: */
if (geom_returnF)
	{
	n = (int) main_wx -
	    (int) guiSP->control_winS.width - 2 * (int) main_bw - 1;
	}
else
	{
	n = (int) guiSP->main_winS.width -
	    (int) guiSP->control_winS.width - 2 * (int) main_bw - 1;
	}
if (n > 0) guiSP->main_win_free_area_width = (unsigned int) n;
else guiSP->main_win_free_area_width = 0;

/* Prepare the main window free area height: */
if (geom_returnF)
	{
	n = (int) main_wy -
	    (int) guiSP->input_winS.height - 2 * (int) main_bw - 1;
	}
else
	{
	n = (int) guiSP->main_winS.height -
	    (int) guiSP->input_winS.height - 2 * (int) main_bw - 1;
	}
if (n > 0) guiSP->main_win_free_area_height = (unsigned int) n;
else guiSP->main_win_free_area_height = 0;

/*------control window:------------------------------------------------------*/

/* Set control window attributes: */
control_attribS.border_pixel = guiSP->control_winS.fg_colorID;
control_attribS.background_pixmap = guiSP->control_winS.pixmapID;
control_attribS.cursor = guiSP->control_winS.cursorID;
control_attribS.win_gravity = NorthEastGravity;
control_attribS.colormap = guiSP->colormapID;
control_value_mask = CWBorderPixel | CWBackPixmap | CWCursor |
		     CWWinGravity | CWColormap;

/* Prepare the control window position: */
guiSP->control_winS.x0 = guiSP->main_win_free_area_width;
guiSP->control_winS.y0 = 1;

/* Copy the border width: */
guiSP->control_winS.border_width = border_width;

/* Create control window: */
guiSP->control_winS.ID = XCreateWindow (
		guiSP->displaySP, guiSP->main_winS.ID,
		guiSP->control_winS.x0, guiSP->control_winS.y0,
		guiSP->control_winS.width, guiSP->control_winS.height,
		border_width, CopyFromParent,
		InputOutput, guiSP->visualSP,
		control_value_mask, &control_attribS);

/* Set the window colormap (is this necessary?): */
XSetWindowColormap (guiSP->displaySP, guiSP->control_winS.ID,
		    guiSP->colormapID);

/*------input window:--------------------------------------------------------*/

/* Set input window attributes: */
input_attribS.border_pixel = guiSP->input_winS.fg_colorID;
input_attribS.background_pixel = guiSP->input_winS.bg_colorID;
input_attribS.cursor = guiSP->input_winS.cursorID;
input_attribS.win_gravity = SouthWestGravity;
input_attribS.colormap = guiSP->colormapID;
input_value_mask = CWBorderPixel | CWBackPixel | CWCursor |
		   CWWinGravity | CWColormap;

/* Input window width and position (see above for height): */
guiSP->input_winS.x0 = 1;
n = (int) main_wx -
    (int) guiSP->control_winS.width - 4 * (int) main_bw - 2 * 1 - 2;
if (guiSP->main_win_free_area_width >= 10)
	{
	guiSP->input_winS.width = guiSP->main_win_free_area_width -
				  2 * main_bw - 2;
	}
else guiSP->input_winS.width = 10;
guiSP->input_winS.y0 = guiSP->main_win_free_area_height;

/* Copy the border width: */
guiSP->input_winS.border_width = border_width;

/* Create input window: */
guiSP->input_winS.ID = XCreateWindow (
		guiSP->displaySP, guiSP->main_winS.ID,
		guiSP->input_winS.x0, guiSP->input_winS.y0,
		guiSP->input_winS.width, guiSP->input_winS.height,
		border_width, CopyFromParent,
		InputOutput, guiSP->visualSP,
		input_value_mask, &input_attribS);

/* Set the window colormap (is this necessary?): */
XSetWindowColormap (guiSP->displaySP, guiSP->input_winS.ID, guiSP->colormapID);

/*------output window:-------------------------------------------------------*/

/* Set output window attributes: */
output_attribS.border_pixel = guiSP->output_winS.fg_colorID;
output_attribS.background_pixel = guiSP->input_winS.bg_colorID;
output_attribS.cursor = guiSP->output_winS.cursorID;
output_attribS.win_gravity = NorthEastGravity;
output_value_mask = CWBorderPixel | CWBackPixel | CWCursor | CWWinGravity;

/* Output window size and position: */
guiSP->output_winS.width = guiSP->control_winS.width;
guiSP->output_winS.x0 = guiSP->control_winS.x0;
guiSP->output_winS.y0 = guiSP->control_winS.height + 2 * main_bw + 1 + 2;
n = (int) main_wy - guiSP->control_winS.height - 4 * (int) main_bw - 4;
if ((n > 0) && (n >= 100)) guiSP->output_winS.height = (unsigned int) n;
else guiSP->output_winS.height = 100;

/* Copy the border width: */
guiSP->output_winS.border_width = border_width;

/* Create input window: */
guiSP->output_winS.ID = XCreateWindow (
		guiSP->displaySP, guiSP->main_winS.ID,
		guiSP->output_winS.x0, guiSP->output_winS.y0,
		guiSP->output_winS.width, guiSP->output_winS.height,
		border_width, CopyFromParent,
		InputOutput, guiSP->visualSP,
		output_value_mask, &output_attribS);

/* Set the window colormap (is this necessary?): */
XSetWindowColormap (guiSP->displaySP, guiSP->input_winS.ID, guiSP->colormapID);

/*------additional settings:-------------------------------------------------*/

/* Set window manager hints: */
SetWMHints_ (guiSP);

/* Set main window name: */
main_win_titleP = main_win_titleA;
if (!XStringListToTextProperty (&main_win_titleP, 1, &main_win_titleS))
	{
	ErrorMessage_ ("garlic", "CreateWindows_", "",
		"Failed to allocate storage for XTextProperty structure!\n",
		"", "", "");
	return -3;
	}

/* Set icon window name: */
icon_win_titleP = icon_win_titleA;
if (!XStringListToTextProperty (&icon_win_titleP, 1, &icon_win_titleS))
	{
	ErrorMessage_ ("garlic", "CreateWindows_", "",
		"Failed to allocate storage for XTextProperty structure!\n",
		"", "", "");
	return -4;
	}

/* Set class hints (just two char pointers and nothing else): */
class_hintS.res_name = prog_nameA;
class_hintS.res_class = class_nameA;

/* Inform window manager about application window properties: */
XSetWMProperties (guiSP->displaySP, guiSP->main_winS.ID,
		  &main_win_titleS, &icon_win_titleS,
		  argv, argc,
		  &guiSP->main_winS.size_hintsS,
		  &guiSP->wm_hintsS, &class_hintS);

/* Select events: */
SelectEvents_ (guiSP);

/* Map windows: */
/* The whole source code grew to about 59000 characters */
/* before I saw  the garlic window for  the first time! */
XMapSubwindows (guiSP->displaySP, guiSP->main_winS.ID);
XMapWindow (guiSP->displaySP, guiSP->main_winS.ID);

/* If control window should be hidden: */
if (!configSP->show_control_windowF) HideControlWindow_ (guiSP);

/*------main pixmap ("hidden frame"):----------------------------------------*/

/* The main hidden pixmap.  Drawing is done to a hidden pixmap */
/* which resides on the server side.  Another approach will be */
/* to use an XImage on the client side. An experiment was made */
/* to compare two approaches,  using XFree86 implementation of */
/* X11 window system. The XImage-based method was twice faster */
/* if both client and server ran on the same machine. However, */
/* the hidden pixmap method was chosen  because it offers much */
/* larger set of  drawing routines  (rectangles,  arcs  etc.). */
/* The main hidden pixmap is the original drawing destination, */
/* and the content is copied to the main window after drawing. */
guiSP->main_hidden_pixmapID = XCreatePixmap (
					guiSP->displaySP,
					DefaultRootWindow (guiSP->displaySP),
					guiSP->main_win_free_area_width,
					guiSP->main_win_free_area_height,
					guiSP->depth);
guiSP->main_hidden_pixmapF = 1;

/*------the hidden pixmap with small arrows:---------------------------------*/

/* The hidden pixmap with small arrows.  It is used to show quickly the */
/* approximate direction of the main chain,  projected to the XY plane. */
/* The appropriate arrow is drawn just under the sequence neighborhood. */
n = MakePixmap2_ (&guiSP->small_arrows_pixmapID,
		  &guiSP->small_arrows_width,
		  &guiSP->small_arrows_height,
		  guiSP, small_arrows_xpm);
if (n < 0) return -5;
else (guiSP->small_arrows_pixmapF = 1);

/*---------------------------------------------------------------------------*/

return 1;
}

/*===========================================================================*/


