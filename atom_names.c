/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

			    atom_names.c

Purpose:
	Extract atom names from the list.

Input:
	(1) Pointer to SelectS structure, where data will be stored.
	(2) Pointer to the string with list of atom names.

Output:
	(1) Some data added to SelectS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract atom names:==================================================*/

int ExtractAtomNames_ (SelectS *selectSP, char *stringP)
{
char		*P, *tokenP;
int		nameI;

/* Check the string length: */
if (strlen (stringP) == 0) return -1;

/* Initialize all_atom_namesF (1 = select all atom names): */
selectSP->all_atom_namesF = 0;

/* Initialize the number of atom names: */
selectSP->atom_namesN = 0;

/* If wildcard is present, set all_atom_namesF to one and return: */
if (strstr (stringP, "*"))
	{
	selectSP->all_atom_namesF = 1;
	return 1;
	}

/* Parse the list of atom names: */
P = stringP;
while ((tokenP = strtok (P, " \t,;")) != NULL)
	{
	/** Ensure the proper operation of strtok: **/
	P = NULL;

	/** Copy the token and update the count: **/
	nameI = selectSP->atom_namesN;
	strncpy (selectSP->atom_nameAA[nameI],
		 tokenP, ATOMNAMESIZE - 1);
	selectSP->atom_nameAA[nameI][ATOMNAMESIZE - 1] = '\0';

	/** Update and check the count: **/
	selectSP->atom_namesN++;
	if (selectSP->atom_namesN >= MAXFIELDS) break;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


