/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

			    translate_membrane.c

Purpose:
	Translate the membrane.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to VectorS structure, with shift vector.

Output:
	(1) Membrane translated.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======translate membrane:==================================================*/

void TranslateMembrane_ (MolComplexS *curr_mol_complexSP,
			 VectorS *shift_vectorSP)
{
MembraneS	*membraneSP;

/* Pointer to the current membrane: */
membraneSP = &curr_mol_complexSP->membraneS;

/* Translate the membrane center: */
membraneSP->center_x += shift_vectorSP->x;
membraneSP->center_y += shift_vectorSP->y;
membraneSP->center_z += shift_vectorSP->z;

/* Set the position_changedF: */
curr_mol_complexSP->position_changedF = 1;
}

/*===========================================================================*/


