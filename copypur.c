/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				copypur.c

Purpose:
	Copy string, omitting spaces.

Input:
	(1) Output string pointer.
	(2) Input string pointer.

Output:
	(1) Output string.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

/*======copy purified string:================================================*/

void CopyPurified_ (char *output_stringP, char *input_stringP)
{
int		n;

while ((n = *input_stringP++) != '\0') if (n != ' ') *output_stringP++ = n;
*output_stringP = '\0';
}

/*===========================================================================*/


