/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				read_file.c

Purpose:
	Try to recognize input  file format;  read file  if recognized.  In
	fact, this function does almost nothing:  file format is recognized
	by RecogFileFormat_ and ReadPDBFile_ reads PDB file.

Input:
	(1) Pointer to MolComplexS structure (macromolecular complexes). It
	    should point to the first free MolComplexS structure.
	(2) Input file name (pointer).
	(3) Pointer to ConfigS structure, with configuration data.

Output:
	(1) If PDB file is recognized, a function which read atomic data
	    is called.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Zero if there is nothing to read (strlen (file_nameP) is zero).
	(3) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		RecogFileFormat_ (char *);
int		ReadPDBFile_ (MolComplexS *, char *, ConfigS *);
void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======recognize file format, read file:====================================*/

int ReadFile_ (MolComplexS *mol_complexSP, char *file_nameP, ConfigS *configSP)
{
int		file_format;

/* Is file name specified at all? */
if (strlen (file_nameP) == 0) return 0;

/* Try to recognize file format: */
file_format = RecogFileFormat_ (file_nameP);
if (file_format < 0) return -1;

/* Read file (switch could be used here, but I don't like it): */
if (file_format == PDB_FORMAT)
	{
	if (ReadPDBFile_ (mol_complexSP, file_nameP, configSP) < 0) return -2;
	else return 1;
	}

/* If this point is reached, format is recognized, but file is not read: */
ErrorMessage_ ("garlic", "ReadFile_", file_nameP,
	"Unable to read file - file format not supported!\n", "", "", "");
return -3;
}

/*===========================================================================*/


