/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				position.c

Purpose:
	Execute position command:  move all caught macromol. complexes to
	the specified place (position). The geometric center  will be set
	to this position.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of the command string. The remainder
	    should contain three numeric values (x, y and z coordinate of
	    a new position).

Output:
	(1) All  caught macromolecular complexes  moved to  the specified
	    position.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		CopyDoubles_ (char *, char *, int);
void		TranslatePlane_ (MolComplexS *, VectorS *);
void		TranslateMembrane_ (MolComplexS *, VectorS *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute stereo command:==============================================*/

int CommandPosition_ (MolComplexS *mol_complexSP, int mol_complexesN,
		      RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		      NearestAtomS *nearest_atomSP, size_t pixelsN,
		      unsigned int *refreshIP, char *stringP)
{
char			copyA[STRINGSIZE];
double			x0, y0, z0, x1, y1, z1;
double			delta_x, delta_y, delta_z;
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
size_t			atomsN, atomI;
AtomS			*curr_atomSP;
VectorS			shift_vectorS;

/* Copy digits, signs and decimal points: */
CopyDoubles_ (copyA, stringP, STRINGSIZE - 1);
copyA[STRINGSIZE - 1] = '\0';

/* The input string should contain three numeric values: */
if (sscanf (copyA, "%lf %lf %lf\n", &x0, &y0, &z0) != 3)
	{
	strcpy (runtimeSP->messageA,
		"Three coordinates (x, y and z) expected!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_POSITION;
	}

/* Shift every caught macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Number of atoms in a macromolecular complex: **/
	atomsN = curr_mol_complexSP->atomsN;
	if (atomsN == 0) continue;

	/** Check is it necessary to translate the plane: */
	if (curr_mol_complexSP->move_bits & PLANE_MASK)
		{
		/*** If plane should be moved independently: ***/
		if (!(curr_mol_complexSP->move_bits & STRUCTURE_MASK))
			{
			x1 = curr_mol_complexSP->planeS.center_x[0];
			y1 = curr_mol_complexSP->planeS.center_y;
			z1 = curr_mol_complexSP->planeS.center_z[0];
			}

		/*** If plane is bound to the structure: ***/
		else
			{
			x1 = curr_mol_complexSP->geometric_center_vectorS.x;
			y1 = curr_mol_complexSP->geometric_center_vectorS.y;
			z1 = curr_mol_complexSP->geometric_center_vectorS.z;
			}

		/*** Prepare shift vector and translate the plane: ***/
		shift_vectorS.x = x0 - x1;
		shift_vectorS.y = y0 - y1;
		shift_vectorS.z = z0 - z1;
		TranslatePlane_ (curr_mol_complexSP, &shift_vectorS);
		}

	/** Check is it necessary to translate the membrane: **/
	if (curr_mol_complexSP->move_bits & MEMBRANE_MASK)
		{
		/*** If membrane should be moved independently: ***/
		if (!(curr_mol_complexSP->move_bits & STRUCTURE_MASK))
			{
			x1 = curr_mol_complexSP->membraneS.center_x;
			y1 = curr_mol_complexSP->membraneS.center_y;
			z1 = curr_mol_complexSP->membraneS.center_z;
			}

		/*** If membrane is bound to the structure: ***/
		else
			{
			x1 = curr_mol_complexSP->geometric_center_vectorS.x;
			y1 = curr_mol_complexSP->geometric_center_vectorS.y;
			z1 = curr_mol_complexSP->geometric_center_vectorS.z;
			}

		/*** Prepare shift vector and translate the membrane: ***/
		shift_vectorS.x = x0 - x1;
		shift_vectorS.y = y0 - y1;
		shift_vectorS.z = z0 - z1;
		TranslateMembrane_ (curr_mol_complexSP, &shift_vectorS);
		}

	/** Check is it necessary to translate the structure at all: **/
	if (!(curr_mol_complexSP->move_bits & STRUCTURE_MASK)) continue;

	/** Prepare shifts: **/
	delta_x = x0 - curr_mol_complexSP->geometric_center_vectorS.x;
	delta_y = y0 - curr_mol_complexSP->geometric_center_vectorS.y;
	delta_z = z0 - curr_mol_complexSP->geometric_center_vectorS.z;

	/** Scan all atoms in the current complex: **/
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/** Pointer to the current atom: **/
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/** Move atom to the new position: **/
		curr_atomSP->raw_atomS.x[0] += delta_x;
		curr_atomSP->raw_atomS.y    += delta_y;
		curr_atomSP->raw_atomS.z[0] += delta_z;
		if (configSP->stereoF)
			{
			curr_atomSP->raw_atomS.x[1] += delta_x;
			curr_atomSP->raw_atomS.z[1] += delta_z;
			}
		}

	/** Update the geometric center: **/
	curr_mol_complexSP->geometric_center_vectorS.x += delta_x;
	curr_mol_complexSP->geometric_center_vectorS.y += delta_y;
	curr_mol_complexSP->geometric_center_vectorS.z += delta_z;

	/** Update the extent vectors: **/
	curr_mol_complexSP->left_top_near_vectorS.x    += delta_x;
	curr_mol_complexSP->left_top_near_vectorS.y    += delta_y;
	curr_mol_complexSP->left_top_near_vectorS.z    += delta_z;
	curr_mol_complexSP->right_bottom_far_vectorS.x += delta_x;
	curr_mol_complexSP->right_bottom_far_vectorS.y += delta_y;
	curr_mol_complexSP->right_bottom_far_vectorS.z += delta_z;

	/** Update the rotation center: **/
	curr_mol_complexSP->rotation_center_vectorS.x  += delta_x;
	curr_mol_complexSP->rotation_center_vectorS.y  += delta_y;
	curr_mol_complexSP->rotation_center_vectorS.z  += delta_z;

	/** Update the slab center: **/
	curr_mol_complexSP->slab_center_vectorS.x      += delta_x;
	curr_mol_complexSP->slab_center_vectorS.y      += delta_y;
	curr_mol_complexSP->slab_center_vectorS.z      += delta_z;

	/** Update the color fading center: **/
	curr_mol_complexSP->fading_center_vectorS.x    += delta_x;
	curr_mol_complexSP->fading_center_vectorS.y    += delta_y;
	curr_mol_complexSP->fading_center_vectorS.z    += delta_z;

	/** Update the position_changedF flag: **/
	curr_mol_complexSP->position_changedF = 1;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_POSITION;
}

/*===========================================================================*/


