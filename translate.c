/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				translate.c

Purpose:
	In default editing mode (no editing), translate atoms in macromol.
	complexes which are caught.  TranslateComplex_ function takes care
	for stereo data and for position_changedF.  If editing atoms (mode
	index equal to 1), translate only the selected atoms. If edit mode
	index is equal to 101,  resize the selected portion of one or more
	objects.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Translation shift.
	(6) Translation direction (axis) identifier (1 = x, 2 = y, 3 = z).

Output:
	(1) Atoms translated in all caught macromolecular complexes.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		TranslatePlane_ (MolComplexS *, VectorS *);
void		TranslateMembrane_ (MolComplexS *, VectorS *);
void		ResizeComplex_ (MolComplexS *, double, int, ConfigS *);
void		TranslateComplex_ (MolComplexS *, VectorS *, ConfigS *, int);

/*======translate all caught macromol. complexes:============================*/

void Translate_ (MolComplexS *mol_complexSP, int mol_complexesN,
		 RuntimeS *runtimeSP, ConfigS *configSP,
		 double shift, int axisID)
{
int			edit_modeI;
VectorS			shift_vectorS;
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;

/* Copy the editing mode: */
edit_modeI = runtimeSP->edit_modeI;

/* Prepare the shift vector: */
switch (axisID)
	{
	case 1:
		shift_vectorS.x = shift;
		shift_vectorS.y = 0.0;
		shift_vectorS.z = 0.0;
		break;

	case 2:
		shift_vectorS.x = 0.0;
		shift_vectorS.y = shift;
		shift_vectorS.z = 0.0;
		break;

	case 3:
		shift_vectorS.x = 0.0;
		shift_vectorS.y = 0.0;
		shift_vectorS.z = shift;
		break;

	default:
		;
		break;
	}

for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Check is it necessary to translate the plane: */
	if ((curr_mol_complexSP->move_bits & PLANE_MASK) &&
	    (edit_modeI != 1) && (edit_modeI != 101))
		{
		TranslatePlane_ (curr_mol_complexSP, &shift_vectorS);
		}

	/* Check is it necessary to translate the membrane: */
	if ((curr_mol_complexSP->move_bits & MEMBRANE_MASK) &&
	    (edit_modeI != 1) && (edit_modeI != 101))
		{
		TranslateMembrane_ (curr_mol_complexSP, &shift_vectorS);
		}

	/* Check is it necessary to translate the structure at all: */
	if (!(curr_mol_complexSP->move_bits & STRUCTURE_MASK)) continue;

	/* If resizing is required: */
	if (edit_modeI == 101)
		{
		/* Resize the selected portion of the complex: */
		ResizeComplex_ (curr_mol_complexSP, shift, axisID, configSP);

		/* Do not translate this complex: */
		continue;
		}

	/* Translate or resize the complex (stereo data updated too): */
	TranslateComplex_ (curr_mol_complexSP,
			   &shift_vectorS, configSP, edit_modeI);
	}
}

/*===========================================================================*/


