/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				wheel.c

Purpose:
	Execute  wheel command:  draw helical wheel for the sequence which
	is kept in the sequence buffer.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of the command string.  This command
	    may be given without any keyword, with keyword OFF or with two
	    residue serial numbers.

Output:
	(1) The main window mode changed to 2 (default is zero),  i.e. the
	    main window will be switched to helical wheel plot.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) This command reinitializes the  NearestAtomS array,  except if
	    the additional keyword is not recognized.

	(2) Helical wheel is drawn for the sequence  which is currently in
	    the sequence buffer.  This sequence  will be  typically short.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractTwoIntegers_ (int *, int *, char *);
void		InitNearest_ (NearestAtomS *, size_t);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute wheel command:===============================================*/

int Wheel_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	    NearestAtomS *nearest_atomSP, size_t pixelsN,
	    unsigned int *refreshIP, char *stringP)
{
char		*P;
int		n;
int		residue1I, residue2I;
int		digit_foundF;

/* Reset the clockwise flag: */
runtimeSP->wheel_clockwiseF = 0;

/* If  the string  contans  the substring " OFF",  switch off */
/* the helical wheel plot and return to default drawing mode: */
if (strstr (stringP, " OFF"))
	{
	/* Reset drawing mode index: */
	guiSP->main_window_modeI = 0;

	/* Reinitialize the NearestAtomS array: */
	InitNearest_ (nearest_atomSP, pixelsN);
	*refreshIP = 1;

	/* Refresh the main window: */
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);

	/* Refresh the control window: */
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);

	/* Return the command code: */
	return COMMAND_WHEEL;
	}

/* If the string contains the substring " CLO", the helical wheel should be */
/* drawn clockwise (this gives the bottom view of the corresponding helix). */
if (strstr (stringP, " CLO")) runtimeSP->wheel_clockwiseF = 1;

/* Initialize the range of residue array indices: */
runtimeSP->range_startI = 0;
if (runtimeSP->residuesN >= 1)
	{
	runtimeSP->range_endI = (int) runtimeSP->residuesN - 1;
	}
else
	{
	runtimeSP->range_endI = 0;
	}

/* Scan the string for digits; if it contains at least one */
/* digit, try to extract two integers (the range indices): */
P = stringP;
digit_foundF = 0;
while ((n = *P++) != '\0')
	{
	if (isdigit (n))
		{
		digit_foundF = 1;
		break;
		}
	}

/* If at least one digit was found, try to extract two integers: */
if (digit_foundF)
	{
	/* Replace each minus in the input string with space: */
	P = stringP;
	while ((n = *P++) != '\0')
		{
		if (n == '-') *(P - 1) = ' ';
		}

	/* If reading fails: */
	if (ExtractTwoIntegers_ (&residue1I, &residue2I, stringP) < 0)
		{
		strcpy (runtimeSP->messageA,
			"Command interpretation failed!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_WHEEL;
		}

	/* If indices were successfully read, check them: */
	if ((residue1I < 1) || (residue2I < residue1I))
		{
		strcpy (runtimeSP->messageA, "Bad range (check indices)!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_WHEEL;
		}

	/* Store the extracted indices: */
	runtimeSP->range_startI = residue1I - 1;
	runtimeSP->range_endI = residue2I - 1;
	}

/* Set the main window drawing mode index: */
guiSP->main_window_modeI = 2;

/* Reinitialize the NearestAtomS array and refresh index: */
InitNearest_ (nearest_atomSP, pixelsN);
*refreshIP = 1;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_WHEEL;
}

/*===========================================================================*/


