/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				sp2_color.c

Purpose:
	Prepare spacefill color, version 2. In this function white, left,
	middle, right and black color are used.

Input:
	(1) Pointer to AtomS structure.
	(2) Pointer to GUIS structure.
	(3) Cosine of the angle between the vector from sphere center and
	    the current pixel and  the light source unit vector.  It will
	    be used to calculate the angle.

Output:
	(1) Return value.

Return value:
	The pixel (color) value.

Notes:
	(1) This function may be quite slow (a lot of math).

=============================================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======spacefill color:=====================================================*/

unsigned long Sp2Color_ (AtomS *curr_atomSP, GUIS *guiSP, double cos_angle)
{
static long double	angle;
static long double	angle1 = 10.0, angle2 = 40.0;
static long double	angle3 = 80.0, angle4 = 130.0;
static long double	delta_angle;
static long double	w0 = 0.100000, w1 = 0.033333;
static long double	w2 = 0.025000, w3 = 0.020000;
static unsigned long	colorID;
static unsigned long	red_mask, green_mask, blue_mask;
static unsigned long	red1, red2, red;
static unsigned long	green1, green2, green;
static unsigned long	blue1, blue2, blue;
static long double	r1, r2, g1, g2, b1, b2;
static long double	r, g, b;

/* Calculate the angle, in degrees: */
if (cos_angle <= -1.0) angle = 180.0;
else if (cos_angle >= 1.0) angle = 0.0;
else angle = RAD_TO_DEG * acos (cos_angle);

/* Copy masks: */
red_mask   = guiSP->visual_infoS.red_mask;
green_mask = guiSP->visual_infoS.green_mask;
blue_mask  = guiSP->visual_infoS.blue_mask;

/* Find sector and prepare color components: */
if (angle >= angle4)
	{
	return (guiSP->black_colorID);
	}

else if (angle >= angle3)
	{
	/* Extract input color components: */
	red1   = curr_atomSP->right_colorID & red_mask;
	red2   = guiSP->black_colorID       & red_mask;
	green1 = curr_atomSP->right_colorID & green_mask;
	green2 = guiSP->black_colorID       & green_mask;
	blue1  = curr_atomSP->right_colorID & blue_mask;
	blue2  = guiSP->black_colorID       & blue_mask;

	/* Convert to doubles: */
	r1 = (long double) red1;
	r2 = (long double) red2;
	g1 = (long double) green1;
	g2 = (long double) green2;
	b1 = (long double) blue1;
	b2 = (long double) blue2;

	/* Calculate new color components: */
	delta_angle = angle - angle3;
	r = r1 + w3 * (r2 - r1) * delta_angle;
	g = g1 + w3 * (g2 - g1) * delta_angle;
	b = b1 + w3 * (b2 - b1) * delta_angle;
	}

else if (angle >= angle2)
	{
	/* Extract input color components: */
	red1   = curr_atomSP->middle_colorID & red_mask;
	red2   = curr_atomSP->right_colorID  & red_mask;
	green1 = curr_atomSP->middle_colorID & green_mask;
	green2 = curr_atomSP->right_colorID  & green_mask;
	blue1  = curr_atomSP->middle_colorID & blue_mask;
	blue2  = curr_atomSP->right_colorID  & blue_mask;

	/* Convert to doubles: */
	r1 = (long double) red1;
	r2 = (long double) red2;
	g1 = (long double) green1;
	g2 = (long double) green2;
	b1 = (long double) blue1;
	b2 = (long double) blue2;

	/* Calculate new color components: */
	delta_angle = angle - angle2;
	r = r1 + w2 * (r2 - r1) * delta_angle;
	g = g1 + w2 * (g2 - g1) * delta_angle;
	b = b1 + w2 * (b2 - b1) * delta_angle;
	}

else if (angle >= angle1)
	{
	/* Extract input color components: */
	red1   = curr_atomSP->left_colorID   & red_mask;
	red2   = curr_atomSP->middle_colorID & red_mask;
	green1 = curr_atomSP->left_colorID   & green_mask;
	green2 = curr_atomSP->middle_colorID & green_mask;
	blue1  = curr_atomSP->left_colorID   & blue_mask;
	blue2  = curr_atomSP->middle_colorID & blue_mask;

	/* Convert to doubles: */
	r1 = (long double) red1;
	r2 = (long double) red2;
	g1 = (long double) green1;
	g2 = (long double) green2;
	b1 = (long double) blue1;
	b2 = (long double) blue2;

	/* Calculate new color components: */
	delta_angle = angle - angle1;
	r = r1 + w1 * (r2 - r1) * delta_angle;
	g = g1 + w1 * (g2 - g1) * delta_angle;
	b = b1 + w1 * (b2 - b1) * delta_angle;
	}

else
	{
	/* Extract input color components: */
	red1   = guiSP->white_colorID      & red_mask;
	red2   = curr_atomSP->left_colorID & red_mask;
	green1 = guiSP->white_colorID      & green_mask;
	green2 = curr_atomSP->left_colorID & green_mask;
	blue1  = guiSP->white_colorID      & blue_mask;
	blue2  = curr_atomSP->left_colorID & blue_mask;

	/* Convert to doubles: */
	r1 = (long double) red1;
	r2 = (long double) red2;
	g1 = (long double) green1;
	g2 = (long double) green2;
	b1 = (long double) blue1;
	b2 = (long double) blue2;

	/* Calculate new color components: */
	delta_angle = angle;
	r = r1 + w0 * (r2 - r1) * delta_angle;
	g = g1 + w0 * (g2 - g1) * delta_angle;
	b = b1 + w0 * (b2 - b1) * delta_angle;
	}

/* Check new color components: */
if (r < 0.0) r = 0.0;
if (g < 0.0) g = 0.0;
if (b < 0.0) b = 0.0;

/* Convert new color components: */
red   = ((unsigned long) r) & red_mask;
green = ((unsigned long) g) & green_mask;
blue  = ((unsigned long) b) & blue_mask;

/* Combine new color components: */
colorID = red | green | blue;

/* Return the pixel value: */
return colorID;
}

/*===========================================================================*/


