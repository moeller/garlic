/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				init_header.c

Purpose:
	Initialize offsets in HeaderS structure.

Input:
	(1) Pointer to HeaderS structure.

Output:
	(1) HeaderS structure initialized.

Return value:
	 No return value.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======initialize offsets in HeaderS structure:=============================*/

void InitializeHeader_ (HeaderS *hSP)
{
/* Set offsets: */
hSP->header_offset = 0;
hSP->title_offset  = hSP->header_offset + MAXHEADERLINES * HEADERLINESIZE;
hSP->compnd_offset = hSP->title_offset  + MAXTITLELINES  * HEADERLINESIZE;
hSP->source_offset = hSP->compnd_offset + MAXCOMPNDLINES * HEADERLINESIZE;
hSP->expdta_offset = hSP->source_offset + MAXSOURCELINES * HEADERLINESIZE;
hSP->author_offset = hSP->expdta_offset + MAXEXPDTALINES * HEADERLINESIZE;
}

/*===========================================================================*/


