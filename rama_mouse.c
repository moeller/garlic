/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				rama_mouse.c

Purpose:
	Handle MotionNotify events  if the main window contains Ramachandran
	plot.

Input:
	(1) Pointer to MolComplexS structure, with macromolecular complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure, with configuration data.
	(4) Pointer to GUIS structure, with GUI data.
	(5) Pointer to NearestAtomS structure.
	(6) The number of pixels in the main window free area.
	(7) Pointer to refreshI.
	(8) Pointer to XMotionEvent structure.

Output:
	(1) Some data  related to  the residue under  the pointer written to
	    output window.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Zero if event is ignored.
	(3) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======handle MotionNotify events for Ramachandran plot:====================*/

int RamaMouse_ (MolComplexS *mol_complexSP, int mol_complexesN,
		ConfigS *configSP, GUIS *guiSP,
		NearestAtomS *nearest_atomSP, size_t pixelsN,
		unsigned int refreshI,
		XMotionEvent *motion_eventSP)
{
size_t			pixelI;
NearestAtomS		*pixelSP;
MolComplexS		*curr_mol_complexSP;
AtomS			*curr_atomSP;
RawAtomS		*raw_atomSP;
static char		stringA[SHORTSTRINGSIZE];
int			line_height;
int			screen_x0, screen_y0;
ResidueS		*curr_residueSP;
double			phi, psi, omega;

/* If the event came from child window, return zero: */
if (motion_eventSP->subwindow != None) return 0;

/* Pixel index: */
pixelI = guiSP->main_win_free_area_width * motion_eventSP->y +
	 motion_eventSP->x;

/* Check the pixel index: */
if (pixelI >= pixelsN) return -1;

/* Pointer to NearestAtomS structure assigned with this pixel: */
pixelSP = nearest_atomSP + pixelI;

/* Check the refreshI associated with this pixel: */
if (pixelSP->last_refreshI != refreshI) return 0;

/* Refresh the output window: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->output_winS.bg_colorID);
XFillRectangle (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		0, 0, guiSP->output_winS.width, guiSP->output_winS.height);

/* Pointer to the current macromolecular complex: */
curr_mol_complexSP = mol_complexSP + pixelSP->mol_complexI;

/* Pointer to the current atom: */
curr_atomSP = curr_mol_complexSP->atomSP + pixelSP->atomI;

/* Pointer to raw atomic data: */
raw_atomSP = &curr_atomSP->raw_atomS;

/* Pointer to the current residue: */
curr_residueSP = curr_mol_complexSP->residueSP + curr_atomSP->residue_arrayI;

/* Prepare the text foreground color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->output_winS.fg_colorID);

/* Text line height: */
line_height = guiSP->output_winS.text_line_height;

/* The initial drawing position: */
screen_x0 = TEXTMARGIN;
screen_y0 = guiSP->output_winS.font_height + 5;

/* Draw the unique PDB identifier: */
sprintf (stringA, "(%d) %s",
	 curr_mol_complexSP->mol_complexID,
	 curr_mol_complexSP->unique_PDB_codeA);
stringA[SHORTSTRINGSIZE - 1] = '\0';
XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, strlen (stringA));

/* Draw residue data: */
screen_y0 += line_height;
sprintf (stringA, "%s %d %c",
	 raw_atomSP->residue_nameA,
	 raw_atomSP->residue_sequenceI,
	 raw_atomSP->chainID);
stringA[SHORTSTRINGSIZE - 1] = '\0';
XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
	     screen_x0, screen_y0, stringA, strlen (stringA));

/* The peptide group conformation: */
screen_y0 += line_height;
if (curr_residueSP->cis_transF == 1)
	{
	strcpy (stringA, "trans");
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}
else if (curr_residueSP->cis_transF == 2)
	{
	strcpy (stringA, "cis");
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}
else
	{
	strcpy (stringA, "bad/undef.");
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}

/* The angle phi (see dihedral_angles.c for definition): */
screen_y0 += line_height;
phi = curr_residueSP->phi;
if (phi == BADDIHEDANGLE)
	{
	strcpy (stringA, "phi: missing");
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}
else
	{
	phi *= RAD_TO_DEG;
	sprintf (stringA, "phi: %7.2f", phi);
	stringA[SHORTSTRINGSIZE - 1] = '\0';
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}

/* The angle psi (see dihedral_angles.c for definition): */
screen_y0 += line_height;
psi = curr_residueSP->psi;
if (psi == BADDIHEDANGLE)
	{
	strcpy (stringA, "psi: missing");
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}
else
	{
	psi *= RAD_TO_DEG;
	sprintf (stringA, "psi: %7.2f", psi);
	stringA[SHORTSTRINGSIZE - 1] = '\0';
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}

/* The angle omega psi (see dihedral_angles.c for definition): */
screen_y0 += line_height;
omega = curr_residueSP->omega;
if (omega == BADDIHEDANGLE)
	{
	strcpy (stringA, "omeg: missing");
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}
else
	{
	omega *= RAD_TO_DEG;
	sprintf (stringA, "omeg:%7.2f", omega);
	stringA[SHORTSTRINGSIZE - 1] = '\0';
	XDrawString (guiSP->displaySP, guiSP->output_winS.ID, guiSP->theGCA[0],
		     screen_x0, screen_y0, stringA, strlen (stringA));
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


