/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				add_two_colors.c

Purpose:
	Add (interpolate) two additional colors.

Input:
	(1) The first input color.
	(2) The second input color.
	(3) Pointer to GUIS structure.
	(4) Pointer to the first output color.
	(5) Pointer to the second output color.

Output:
	(1) The first output color prepared.
	(2) The second output color prepared.

Return value:
	No return value.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======add (interpolate) two colors:========================================*/

void AddTwoColors_ (unsigned long input_color1ID,
		    unsigned long input_color2ID,
		    GUIS *guiSP,
		    unsigned long *output_color1IDP,
		    unsigned long *output_color2IDP)
{
unsigned long		red_mask, green_mask, blue_mask;
unsigned long		red1, red2, red;
unsigned long		green1, green2, green;
unsigned long		blue1, blue2, blue;
double			r1, r2, g1, g2, b1, b2;
double			w1, w2, r, g, b;

/* Copy masks: */
red_mask   = guiSP->visual_infoS.red_mask;
green_mask = guiSP->visual_infoS.green_mask;
blue_mask  = guiSP->visual_infoS.blue_mask;

/* Extract input color components: */
red1   = input_color1ID & red_mask;
red2   = input_color2ID & red_mask;
green1 = input_color1ID & green_mask;
green2 = input_color2ID & green_mask;
blue1  = input_color1ID & blue_mask;
blue2  = input_color2ID & blue_mask;

/* Convert to doubles: */
r1 = (double) red1;
r2 = (double) red2;
g1 = (double) green1;
g2 = (double) green2;
b1 = (double) blue1;
b2 = (double) blue2;

/* Weighting factors: */
w1 = 2.0 / 3.0;
w2 = 1.0 / 3.0;

/* The first output color (components): */
r = w1 * r1 + w2 * r2;
g = w1 * g1 + w2 * g2;
b = w1 * b1 + w2 * b2;
red   = ((unsigned long) r) & red_mask;
green = ((unsigned long) g) & green_mask;
blue  = ((unsigned long) b) & blue_mask;

/* The first output color (value): */
*output_color1IDP = red | green | blue;

/* The second output color (components): */
r = w2 * r1 + w1 * r2;
g = w2 * g1 + w1 * g2;
b = w2 * b1 + w1 * b2;
red   = ((unsigned long) r) & red_mask;
green = ((unsigned long) g) & green_mask;
blue  = ((unsigned long) b) & blue_mask;

/* The second output color (value): */
*output_color2IDP = red | green | blue;

}

/*===========================================================================*/


