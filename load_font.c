/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				load_font.c

Purpose:
	Load font. If this is not posssible, try some replacements.

Input:
	(1) Pointer to WindowS structure, where font structure pointer will
	    be stored.
	(2) Pointer to display structure.
	(3) Font name (string).

Output:
	(1) Font structure pointer initialized.
	(2) Font flag set.
	(3) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======load font:===========================================================*/

int LoadFont_ (WindowS *winSP, Display *displaySP, char *font_nameP)
{
static XFontStruct	*fontSP;

/* The first attempt: */
fontSP = XLoadQueryFont (displaySP, font_nameP);
if (fontSP)
	{
	winSP->fontSP = fontSP;
	winSP->fontF = 1;
	return 1;
	}
else printf ("The font %s not available, trying 10x20 ...\n", font_nameP);

/* The second attempt - trying 10x20: */
fontSP = XLoadQueryFont (displaySP, "10x20");
if (fontSP) 
	{
	winSP->fontSP = fontSP;
	winSP->fontF = 1;
	return 2;
	}
else printf ("The font 10x20 not available, trying 9x15 ...\n");

/* The third attempt - 9x15: */
fontSP = XLoadQueryFont (displaySP, "9x15");
if (fontSP)
	{
	winSP->fontSP = fontSP;
	winSP->fontF = 1;
	return 3;
	}
else printf ("The font 9x15 not available, trying 7x14...\n");

/* The fourth attempt - 7x14: */
fontSP = XLoadQueryFont (displaySP, "7x14");
if (fontSP)
        {
        winSP->fontSP = fontSP;
	winSP->fontF = 1;
        return 4;
        }
else printf ("The font 7x14 not available, trying fixed ...\n");

/* The fifth (and last) attempt - fixed font: */
fontSP = XLoadQueryFont (displaySP, "fixed");
if (fontSP)
	{
	winSP->fontSP = fontSP;
	winSP->fontF = 1;
	return 5;
	}
else printf ("Fixed font not available, wouldn't try any more.\n");

return -1;
}

/*===========================================================================*/


