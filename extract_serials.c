/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				extract_serials.c

Purpose:
	Extract two residue serial numbers from a string. Minus [-],
	colon [:] and  tilde [~]  are accepted  as token separators.

Input:
	(1) Pointer to the first serial number.
	(2) Pointer to the second serial number.
	(3) Pointer to string.

Output:
	(1) The first serial number set.
	(2) The second serial number set.
	(3) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>

/*======extract residue ranges:==============================================*/

int ExtractSerials_ (int *serial1P, int *serial2P, char *stringP)
{
char		*tokenP;
int		n;

/* Take the first token: */
if ((tokenP = strtok (stringP, "-:~")) == NULL) return -1;

/* Read the first serial number: */
if (sscanf (tokenP, "%d", &n) != 1) return -2;
*serial1P = n;

/* Try to extract the second token: */
tokenP = strtok (NULL, "-:~");

/* If the second token is not present,  the second */
/* serial number should be equal to the first one: */
if (tokenP == NULL)
	{
	*serial2P = *serial1P;
	}

/* If the second token is present, read the */
/* second  serial number  from  this token: */
else
	{
	if (sscanf (tokenP, "%d", &n) != 1) return -3;
	*serial2P = n;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


