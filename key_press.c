/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				key_press.c

Purpose:
	Handle KeyPress events.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to the next macromolecular complex identifier.
	(4) Pointer to RuntimeS structure, with some runtime data.
	(5) Pointer to ConfigS structure, with configuration data.
	(6) Pointer to GUIS structure, with GUI data.
	(7) Pointer to pointer to NearestAtomS structure.
	(8) Pointer to the number of pixels in the main window free area.
	(9) Pointer to refreshI.
       (10) Pointer to XKeyEvent structure.

Output:
	(1) Some action performed.
	(2) Return value.

Return value:
	(1) Positive or zero on success. If positive value is returned,
	    it should be  interpreted  as command code  returned by the
	    function ExecuteCommand1_ ().
	(2) Negative, if ExecuteCommand1_ () function failes. The value
	    should be interpreted as (internal) error code.

Notes:
	(1) The value of refreshI  (*refreshIP)  may be changed in this
	    function.  It is checked in  EventLoop_ (see event_loop.c).
	    It is assumed that refreshI is updated no more than once in
	    each event handling function.  If this is not the case, the
	    care should be taken to avoid refreshI overflow.

========includes:============================================================*/

#include <stdio.h>

#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <X11/keysym.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

double		RotationAngle_ (ConfigS *, GUIS *, double);
int		Rotate_ (MolComplexS *, int,
			 RuntimeS *, ConfigS *, double, int);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);
int		DockingRefresh_ (RuntimeS *, GUIS *);
double		TranslationShift_ (ConfigS *, GUIS *, double);
void		Translate_ (MolComplexS *, int,
			    RuntimeS *, ConfigS *, double, int);
double		SlabShift_ (ConfigS *, GUIS *, double);
void		MoveBackSlab_ (MolComplexS *, int, ConfigS *, double);
void		MoveFrontSlab_ (MolComplexS *, int, ConfigS *, double);
double		FadingShift_ (ConfigS *, GUIS *, double);
void		MoveBackFading_ (MolComplexS *, int, ConfigS *, double);
void		MoveFrontFading_ (MolComplexS *, int, ConfigS *, double);
int		AddCharToCommand_ (RuntimeS *, GUIS *, int);
int		InputRefresh_ (GUIS *, RuntimeS *);
int		EatLeftChar_ (RuntimeS *);
int		EatRightChar_ (RuntimeS *);
int		ExecuteCommand1_ (MolComplexS *, int *, int *,
				  RuntimeS *, ConfigS *, GUIS *,
				  NearestAtomS **, size_t *, unsigned int *);
int		TruncateCommand_ (RuntimeS *);
int		ReplaceCommand_ (RuntimeS *, int, GUIS *);
void		InitNearest_ (NearestAtomS *, size_t);

/*======handle KeyPress events:==============================================*/

int KeyPress_ (MolComplexS *mol_complexSP, int *mol_complexesNP,
	       int *next_mol_complexIDP,
	       RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	       NearestAtomS **nearest_atomSPP, size_t *pixelsNP,
	       unsigned int *refreshIP,
	       XKeyEvent *key_eventSP)
{
static char		stringA[STRINGSIZE];
static KeySym		key_symID;
static XComposeStatus	compose_statusS;
static double		rotation_angle;
static double		shift;
static int		carriage_pos, comm_length;
static int		new_char;
static int		command_code;
static int		n;

/* Get the KeySym: */
XLookupString (key_eventSP, stringA, STRINGSIZE, &key_symID, &compose_statusS);

/* Select the proper action for a given KeySym: */
switch (key_symID)
	{

/*------modifiers:-----------------------------------------------------------*/

	/* Shift key pressed: */
	case XK_Shift_L:
	case XK_Shift_R:
		guiSP->shift_pressedF = 1;
		break;

	/* Control key pressed: */
	case XK_Control_L:
	case XK_Control_R:
		guiSP->control_pressedF = 1;
		break;

	/* Alt or meta key pressed: */
	case XK_Alt_L:
	case XK_Alt_R:
	case XK_Meta_L:
	case XK_Meta_R:
		guiSP->alt_pressedF = 1;
		break;

/*------rotations:-----------------------------------------------------------*/

	/* Right-handed (positive) rotation about x: */
	case XK_KP_2:
	case XK_KP_Down:
		(*refreshIP)++;
		rotation_angle = RotationAngle_ (configSP, guiSP, 1.0);
		Rotate_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, configSP, rotation_angle, 1);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;


	/* Left-handed (negative) rotation about x: */
	case XK_KP_8:
	case XK_KP_Up:
		(*refreshIP)++;
		rotation_angle = RotationAngle_ (configSP, guiSP, -1.0);
		Rotate_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, configSP, rotation_angle, 1);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Right-handed (positive) rotation about y: */
	case XK_KP_4:
	case XK_KP_Left:
		(*refreshIP)++;
		rotation_angle = RotationAngle_ (configSP, guiSP, +1.0);
		Rotate_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, configSP, rotation_angle, 2);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Left-handed (negative) rotation about y: */
	case XK_KP_6:
	case XK_KP_Right:
		(*refreshIP)++;
		rotation_angle = RotationAngle_ (configSP, guiSP, -1.0);
		Rotate_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, configSP, rotation_angle, 2);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Right-handed (positive) rotation about z: */
	case XK_KP_9:
	case XK_KP_Prior:
		(*refreshIP)++;
		rotation_angle = RotationAngle_ (configSP, guiSP, +1.0);
		Rotate_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, configSP, rotation_angle, 3);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Left-handed (negative) rotation about z: */
	case XK_KP_7:
	case XK_KP_Home:
		(*refreshIP)++;
		rotation_angle = RotationAngle_ (configSP, guiSP, -1.0);
		Rotate_ (mol_complexSP, *mol_complexesNP,
			 runtimeSP, configSP, rotation_angle, 3);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

/*------translations:--------------------------------------------------------*/

	/* Positive translation along x: */
	case XK_KP_Multiply:
	case XK_KP_F4:
		(*refreshIP)++;
		shift = TranslationShift_ (configSP, guiSP, +1.0);
		Translate_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, shift, 1);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Negative translation along x: */
	case XK_KP_Divide:
	case XK_KP_F3:
		(*refreshIP)++;
		shift = TranslationShift_ (configSP, guiSP, -1.0);
		Translate_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, shift, 1);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Positive translation along y: */
	case XK_KP_Add:
	case XK_KP_Separator:
		(*refreshIP)++;
		shift = TranslationShift_ (configSP, guiSP, +1.0);
		Translate_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, shift, 2);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Negative translation along y: */
	case XK_KP_Subtract:
		(*refreshIP)++;
		shift = TranslationShift_ (configSP, guiSP, -1.0);
		Translate_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, shift, 2);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Positive translation along z: */
	case XK_KP_5:
	case XK_KP_Begin:
		(*refreshIP)++;
		shift = TranslationShift_ (configSP, guiSP, +1.0);
		Translate_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, shift, 3);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

	/* Negative translation along z: */
	case XK_KP_0:
	case XK_KP_Insert:
		(*refreshIP)++;
		shift = TranslationShift_ (configSP, guiSP, -1.0);
		Translate_ (mol_complexSP, *mol_complexesNP,
			    runtimeSP, configSP, shift, 3);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		if (guiSP->dockingF) DockingRefresh_ (runtimeSP, guiSP);
		break;

/*------slab:----------------------------------------------------------------*/

	/* Push the back slab surface farther: */
	case XK_KP_1:
	case XK_KP_End:
		(*refreshIP)++;
		shift = SlabShift_ (configSP, guiSP, 1.0);
		MoveBackSlab_ (mol_complexSP, *mol_complexesNP,
			       configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

	/* Pull the back slab surface closer: */
	case XK_KP_3:
	case XK_KP_Next:
		(*refreshIP)++;
		shift = SlabShift_ (configSP, guiSP, -1.0);
		MoveBackSlab_ (mol_complexSP, *mol_complexesNP,
			       configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

	/* Push the front slab surface farther: */
	case XK_KP_Delete:
	case XK_KP_Decimal:
		(*refreshIP)++;
		shift = SlabShift_ (configSP, guiSP, 1.0);
		MoveFrontSlab_ (mol_complexSP, *mol_complexesNP,
				configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

	/* Pull the front slab surface closer: */
	case XK_KP_Enter:
		(*refreshIP)++;
		shift = SlabShift_ (configSP, guiSP, -1.0);
		MoveFrontSlab_ (mol_complexSP, *mol_complexesNP,
				configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

/*------fading:--------------------------------------------------------------*/

	/* Push the back fading surface farther: */
	case XK_F1:
		(*refreshIP)++;
		shift = FadingShift_ (configSP, guiSP, 1.0);
		MoveBackFading_ (mol_complexSP, *mol_complexesNP,
				 configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

	/* Pull the back fading surface closer: */
	case XK_F2:
		(*refreshIP)++;
		shift = FadingShift_ (configSP, guiSP, -1.0);
		MoveBackFading_ (mol_complexSP, *mol_complexesNP,
				 configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

	/* Push the front fading surface farther: */
	case XK_F3:
		(*refreshIP)++;
		shift = FadingShift_ (configSP, guiSP, 1.0);
		MoveFrontFading_ (mol_complexSP, *mol_complexesNP,
				  configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

	/* Pull the front fading surface closer: */
	case XK_F4:
		(*refreshIP)++;
		shift = FadingShift_ (configSP, guiSP, -1.0);
		MoveFrontFading_ (mol_complexSP, *mol_complexesNP,
				  configSP, shift);
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

/*------line editing:--------------------------------------------------------*/

	/* Move the input window carriage (keyboard cursor) to the left: */
	case XK_Left:
		carriage_pos = runtimeSP->carriage_position;
		carriage_pos--;
		if (carriage_pos >= 0)
			{
			runtimeSP->carriage_position = carriage_pos;
			InputRefresh_ (guiSP, runtimeSP);
			}
		break;

	/* Move the input window carriage (keyboard cursor) to the right: */
	case XK_Right:
		carriage_pos = runtimeSP->carriage_position;
		comm_length = runtimeSP->command_length;
		carriage_pos++;
		if (carriage_pos <= comm_length)
			{
			runtimeSP->carriage_position = carriage_pos;
			InputRefresh_ (guiSP, runtimeSP);
			}
		break;

	/* Replace the current command with the previous one, if available: */
	case XK_Up:
		n = ReplaceCommand_ (runtimeSP, -1, guiSP);
		if (n < 0) break;
		InputRefresh_ (guiSP, runtimeSP);
		break;

	/* Replace the current command with the next one (if available): */
	case XK_Down:
		n = ReplaceCommand_ (runtimeSP, +1, guiSP);
		if (n < 0) break;
		InputRefresh_ (guiSP, runtimeSP);
		break;

	/* Move the input window carriage to the beginning of command line: */
	case XK_Home:
		runtimeSP->carriage_position = 0;
		InputRefresh_ (guiSP, runtimeSP);
		break;

	/* Move the input window carrriage to the end of command line: */
	case XK_End:
		runtimeSP->carriage_position = runtimeSP->command_length;
		InputRefresh_ (guiSP, runtimeSP);
		break;

	/* Remove one character on the left side: */
	case XK_BackSpace:
		EatLeftChar_ (runtimeSP);
		InputRefresh_ (guiSP, runtimeSP);
		break;

	/* Remove one character on the right side: */
	case XK_Delete:
		EatRightChar_ (runtimeSP);
		InputRefresh_ (guiSP, runtimeSP);
		break;

/*------command execution:---------------------------------------------------*/

	/* Print command string to log file (if requested), */
	/* execute command,  truncate command string, reset */
	/* the string length and  refresh the input window. */
	/* If some kind  of error occurs,  the return  code */
	/* should  be  negative.  In  that case,  print the */
	/* error message to  log file (if it is requested). */
	case XK_Return:
		/** Print command to log file, if requested: **/
		if (configSP->log_fileF)
			{
			fprintf (configSP->log_fileP, "%s\n",
				 runtimeSP->curr_commandA);
			fflush (configSP->log_fileP);
			}

		/** Parse command string and execute command: **/
		command_code = ExecuteCommand1_ (
					mol_complexSP, mol_complexesNP,
					next_mol_complexIDP,
					runtimeSP, configSP, guiSP,
					nearest_atomSPP, pixelsNP, refreshIP);

		/** Print the latest message to log file, if requested: **/
		if ((command_code < 0) && (configSP->log_fileF))
			{
			fprintf (configSP->log_fileP, "#%s\n",
				 runtimeSP->messageA);
			fflush (configSP->log_fileP);
			}

		/** Truncate the command string: **/
		TruncateCommand_ (runtimeSP);

		/** Refresh (redraw) the input window: **/
		InputRefresh_ (guiSP, runtimeSP);

		/** Update and check the command index **/
		/** and the  total number of commands: **/
		runtimeSP->next_commandI++;
		if (runtimeSP->next_commandI >= MAXCOMMSTRINGS - 1)
			{
			runtimeSP->next_commandI = 0;
			}
		if (runtimeSP->highest_commandI < MAXCOMMSTRINGS - 1)
			{
			runtimeSP->highest_commandI++;
			}

		/** Update the old_commandI: **/
		runtimeSP->old_commandI = runtimeSP->next_commandI;

		/** Return command code to the caller: **/
		return command_code;
		break;

/*------escape key causes return to the main drawing mode:-------------------*/

	/* Reset main window drawing mode and refresh the main window: */
	case XK_Escape:
		/* Reset drawing mode index: */
		guiSP->main_window_modeI = 0;

		/* Reinitialize the NearestAtomS array: */
		InitNearest_ (*nearest_atomSPP, *pixelsNP);
		*refreshIP = 1;

		/* Refresh the main window: */
		(*refreshIP)++;
		MainRefresh_ (mol_complexSP, *mol_complexesNP,
			      runtimeSP, configSP, guiSP,
			      *nearest_atomSPP, *pixelsNP, *refreshIP);

		/* Refresh the control window: */
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);
		break;

/*------character input:-----------------------------------------------------*/

	/* All other keys should belong to the current command string: */
	default:
		/** Check the character: **/
		new_char = stringA[0];
		if (!isprint (new_char)) break;

		/** Append the character to the command string: **/
		if (AddCharToCommand_ (runtimeSP, guiSP, new_char) < 0) break;

		/** Refresh the input window: **/
		InputRefresh_ (guiSP, runtimeSP);
		;

/*---------------------------------------------------------------------------*/

	}

/* Return zero if this point is reached: */
return 0;
}

/*===========================================================================*/


