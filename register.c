/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				register.c

Purpose:
	Register the user - try to send short e-mail message to the author.

Input:
	No input.

Output:
	(1) E-mail send to the author.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) This function tryes to send mail, but it does not try too hard.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "defines.h"

/*======register:============================================================*/

int Register_ (void)
{
char		outfile_nameA[SHORTSTRINGSIZE];
char		command_stringA[STRINGSIZE];
FILE		*outfileP;
int		n;

/* Open the output (message) file: */
do
	{
	/** The first attempt: **/
	strcpy (outfile_nameA, "garlic_reg.txt");
	if ((outfileP = fopen (outfile_nameA, "w")) != NULL) break;

	/** The second attempt: **/
	strcpy (outfile_nameA, "/tmp/garlic_reg.txt");
	if ((outfileP = fopen (outfile_nameA, "w")) != NULL) break;

	/** If this point is reached, both attempts failed: **/
	printf ("Unable to prepare the message file - registration failed!\n");
	return -1;
	} while (0);

/* Write the message: */
fprintf (outfileP, "Hi!\n");

/* Close the file: */
fclose (outfileP);

/* Send the message: */
do
	{
	/** The first attempt (no path): **/
	strcpy (command_stringA, "mailx zucic@garlic.mefos.hr < ");
	strcat (command_stringA, outfile_nameA);
	n = system (command_stringA);
	if (n >= 0) break;

	/** The second attempt (using Digital Unix mailx pathname): **/
	strcpy (command_stringA, "/bin/mailx zucic@garlic.mefos.hr < ");
	strcat (command_stringA, outfile_nameA);
	n = system (command_stringA);
	if (n >= 0) break;

	/** The third attempt (using SunOS mailx pathname): **/
	strcpy (command_stringA, "/usr/bin/mailx zucic@garlic.mefos.hr < ");
	strcat (command_stringA, outfile_nameA);
	n = system (command_stringA);
	if (n >= 0) break;

	/** If this point is reached, all attempts to send mail failed: **/
	remove (outfile_nameA);
	return -2;
	} while (0);

/* Remove (delete) the message file: */
remove (outfile_nameA);

/* If this point is reached, registration might be successful: */
return 1;
}

/*===========================================================================*/


