/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				chi3_from_cbcgsdce.c

Purpose:
	Calculate dihedral angle chi3, using CB, CG, SD and CE coordinates.

Input:
	(1) Pointer to AtomS structure, pointing to the first atom of the
	    current macromolecular complex.
	(2) Index of the first atom of the current residue.
        (3) Index of the last atom of the currrent residue.

Output:
	Return value.

Return value:
	(1) Dihedral angle chi3, on success.
	(2) BADDIHEDANGLE on failure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractFourAtoms_ (VectorS *, VectorS *, VectorS *, VectorS *,
				   char *, char *, char *, char *,
				   AtomS *, size_t, size_t);
void		VectorProduct_ (VectorS *, VectorS *, VectorS *);
double		AbsoluteValue_ (VectorS *);
double		ScalarProduct_ (VectorS *, VectorS *);

/*======calculate chi3 from CB, CG, SD and CE:===============================*/

double Chi3FromCBCGSDCE_ (AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
static VectorS		CB_vectorS, CG_vectorS, SD_vectorS, CE_vectorS;
int			n;
VectorS			CG_CB_vectorS, CG_SD_vectorS;
VectorS			SD_CG_vectorS, SD_CE_vectorS;
VectorS			u1S, u2S;
VectorS			v1S, v2S;
double			denom, ratio, alpha;
double			chi3;

/* Extract CB, CG, SD and CE coordinates: */
n = ExtractFourAtoms_ (&CB_vectorS, &CG_vectorS, &SD_vectorS, &CE_vectorS,
		       "CB", "CG", "SD", "CE",
		       atomSP, atom_startI, atom_endI);

/* All four atoms are required to calculate the angle chi3: */
if (n < 4) return BADDIHEDANGLE;

/* The first pair of auxiliary vectors: */
CG_CB_vectorS.x = CB_vectorS.x - CG_vectorS.x;
CG_CB_vectorS.y = CB_vectorS.y - CG_vectorS.y;
CG_CB_vectorS.z = CB_vectorS.z - CG_vectorS.z;
CG_SD_vectorS.x = SD_vectorS.x - CG_vectorS.x;
CG_SD_vectorS.y = SD_vectorS.y - CG_vectorS.y;
CG_SD_vectorS.z = SD_vectorS.z - CG_vectorS.z;

/* The second pair of auxiliary vectors: */
SD_CG_vectorS.x = CG_vectorS.x - SD_vectorS.x;
SD_CG_vectorS.y = CG_vectorS.y - SD_vectorS.y;
SD_CG_vectorS.z = CG_vectorS.z - SD_vectorS.z;
SD_CE_vectorS.x = CE_vectorS.x - SD_vectorS.x;
SD_CE_vectorS.y = CE_vectorS.y - SD_vectorS.y;
SD_CE_vectorS.z = CE_vectorS.z - SD_vectorS.z;

/* Two vectors  perpendicular to  CG_SD_vectorS,  mutually orthogonal, */
/* the second in the plane defined by CG_CB_vectorS and CG_SD_vectorS: */
VectorProduct_ (&u1S, &CG_CB_vectorS, &CG_SD_vectorS);
VectorProduct_ (&u2S, &u1S, &CG_SD_vectorS);

/* Two vectors  perpendicular to  SD_CG_vectorS,  mutually orthogonal, */
/* the second in the plane defined by SD_CG_vectorS and SD_CE_vectorS: */
VectorProduct_ (&v1S, &SD_CG_vectorS, &SD_CE_vectorS);
VectorProduct_ (&v2S, &SD_CG_vectorS, &v1S);

/* Calculate the angle alpha, which will be used to calculate chi3: */

/* Avoid division by zero: */
denom = AbsoluteValue_ (&u1S) * AbsoluteValue_ (&v1S);
if (denom == 0.0) return BADDIHEDANGLE;

/* Use the scalar product to calculate the cosine of the angle: */
ratio = ScalarProduct_ (&u1S, &v1S) / denom;

/* Arc cosine is very sensitive to floating point errors: */
if (ratio <= -1.0) alpha = 3.1415927;
else if (ratio >= 1.0) alpha = 0.0;
else alpha = acos (ratio);

/* There are two possible solutions; the right one is resolved here: */
if (ScalarProduct_ (&v2S, &u1S) >= 0) chi3 = alpha;
else chi3 = -alpha;

/* Return the angle (in radians): */
return chi3;
}

/*===========================================================================*/


