/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				alloc_nearest.c

Purpose:
	Allocate memory for  NearestAtomS  array. The role of this array is
	described in typedefs.h file. This function is called only once. It
	allocates many bytes,  so when garlic crashes, really big core file
	should be expected. If the main window is enlarged the NearestAtomS
	array should be reallocated.

Input:
	(1) Pointer to  the number of pixels in  the free area of  the main
	    window.  It is initialized  with the number of  allocated array
	    elements.
	(2) Pointer to GUIS structure.

Output:
	(1) Memory allocated.
	(2) The number of pixels updated.
	(3) Return value.

Return value:
	(1) Pointer to allocated memory, on success.
	(2) NULL on failure.

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======allocate memory for NearestAtomS array:==============================*/

NearestAtomS *AllocateNearest_ (size_t *pixelsNP, GUIS *guiSP)
{
static size_t			elementsN;
static size_t			struct_size;
static size_t			mem_size;
static NearestAtomS		*nearest_atomSP;

/* Prepare the parameters which define the size of memory chunk: */
elementsN = (size_t) guiSP->main_win_free_area_width *
	    (size_t) guiSP->main_win_free_area_height;
struct_size = sizeof (NearestAtomS);

/* Allocate memory: */
nearest_atomSP = (NearestAtomS *) calloc (elementsN, struct_size);

/* On failure: */
if (nearest_atomSP == NULL)
	{
	ErrorMessage_ ("garlic", "AllocateNearest_", "",
		"Failed to allocate memory for NearestAtomS array!\n",
		"", "", "");
	mem_size = elementsN * struct_size;
	fprintf (stderr, "A total of %d bytes required.\n", mem_size);
	*pixelsNP = 0;
	return NULL;
	}

/* On success, update the number of pixels: */
*pixelsNP = elementsN;

return nearest_atomSP;
}

/*===========================================================================*/


