/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				print_usage.c

Purpose:
	Print usage info. This function will be called if someone tryes to
	run garlic without X server.

Input:
	No input.

Output:
	Usage info written to stdout.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

/*======print usage info:====================================================*/

void PrintUsage_ (void)
{
fprintf (stdout, "\n=============================================\n");
fprintf (stdout, "Garlic, free molecular visualisation program.\n");
fprintf (stdout, "=============================================\n\n");
fprintf (stdout, "Usage:\n\n");
fprintf (stdout, "      garlic [options] [infile]              \n");
fprintf (stdout, "      garlic -h                ... print help\n");
}

/*===========================================================================*/


