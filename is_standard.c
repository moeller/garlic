/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

			    is_standard.c

Purpose:
	If the specified residue belongs to the set of 20 standard residues,
	return  positive value  or zero.  Otherwise,  return negative value.
	This function  contains the list of  standard residue names.  If the
	specified residue name matches one of the names from the list,  this
	function will return the array index.

Input:
	(1) Residue name.

Output:
	(1) Return value.

Return value:
	(1) Positive or zero, if residue name was found in the list.
	(2) Negative otherwise.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>

/*======check residue name:==================================================*/

int IsStandard_ (char *residue_nameP)
{
int		nameI;
static char	residue_nameAA[20][4] = {"ALA", "ARG", "ASN", "ASP", "CYS",
				     /*    0      1      2      3      4    */

					 "GLN", "GLU", "GLY", "HIS", "ILE",
				     /*    5      6      7      8      9    */

					 "LEU", "LYS", "MET", "PHE", "PRO",
				     /*   10     11     12     13     14    */

					 "SER", "THR", "TRP", "TYR", "VAL"};
				     /*   15     16     17     18     19    */

/* Scan the list of standard residue names: */
for (nameI = 0; nameI < 20; nameI++)
	{
	if (strcmp (residue_nameP, residue_nameAA[nameI]) == 0) return nameI;
	}

/* If this point is reached,  residue name was */
/* not found in the list of 20 standard names: */
return -1;
}

/*===========================================================================*/


