/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				init_nearest.c

Purpose:
	Initialize last_refreshI and styleI for each NearestAtomS structure.
	For a given pixel,  the  NearestAtomS  structure  stores information
	about the last atom drawn in that pixel.  If the current refreshI in
	the event loop  is larger than  last_refreshI for a given pixel, the
	pixel is  free for drawing.  If these  two values  are equal,  the z
	coordinate associated with a given pixel should be compared with the
	z coordinate of  the current atom.  This function must be called if:
	(1) The main window is resized.
	(2) A new macromolecular complex is loaded.
	(3) A previously loaded complex is discarded.
	(4) Atoms are deleted or inserted.
	(5) Visibility status of at least one atom is changed.
	(6) The refreshI in EventLoop_ function is reset.

Input:
	(1) Pointer to an array of NearestAtomS structures.
	(2) Number of array elements (the maximal number of pixels).

Output:
	(1) The last_refreshI in each NearestAtomS structure initialized.
	(2) The styleI in each NearestAtomS structure initialized.

Return value:
	No return value.

Notes:
	(1) The initial last_refreshI index value to zero.  The lowest value
	    of the refreshI in event_loop.c is one. All pixel which have the
	    last_refreshI equal to zero are thus free for drawing - there is
	    no need to compare the z coord. of  the current atom (bond) with
	    the z value associated with a given pixel.

	(2) The initial styleI index value is zero.

	(3) By the way,  as calloc is used  to reallocate  the memory,  this
	    function is unnecessary.

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======initialize the array of NearestAtomS structures:=====================*/

void InitNearest_ (NearestAtomS *nearest_atomSP, size_t max_pixelsN)
{
size_t			i = 0;
NearestAtomS		*curr_pixelSP;

for (i = 0; i < max_pixelsN; i++)
	{
	curr_pixelSP = nearest_atomSP + i;
	curr_pixelSP->last_refreshI = 0;
	curr_pixelSP->styleI = 0;
	}
}

/*===========================================================================*/


