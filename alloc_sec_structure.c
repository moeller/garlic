/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				alloc_sec_structure.c

Purpose:
	Allocate and initialize the storage for the secondary structure
	associated with a macromolecular complex.

Input:
	(1) Pointer to MolComplexS structure.

Output:
	(1) Storage  for the secondary structure information  allocated
	    and initialized, on success.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) One letter codes are used for the secondary structure.

	(2) Default conformation is 'E' (extended).

========includes:============================================================*/

#include <stdio.h>

#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======allocate storage for secondary structure:============================*/

int AllocSecondaryStructure_ (MolComplexS *mol_complexSP)
{
static size_t		char_size;
static size_t		elementsN;
static int		i;

/* Size of a single character: */
char_size = sizeof (char);

/* Allocate storage for the secondary structure */
/* associated with  the macromolecular complex: */
elementsN = MAXRESIDUES;
mol_complexSP->secondary_structureP = (char *) malloc (elementsN);
if (mol_complexSP->secondary_structureP == NULL)
        {
        ErrorMessage_ ("garlic", "AllocSecondaryStructure_", "",
			"Failed to allocate the secondary structure buffer!\n",
			"", "", "");
        return -1;
        }

/* Initialize the secondary structure buffer; */
/* use  the code for  extended  conformation: */
for (i = 0; i < MAXRESIDUES; i++)
	{
	*(mol_complexSP->secondary_structureP + i) = 'E';
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


