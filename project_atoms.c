/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				project_atoms.c

Purpose:
	For each macromolecular complex,  project atoms to  the screen.
	Do not project atoms which fall outside  the visible area.  The
	maximal bond length (see .garlicrc file) is used as the maximal
	atomic diameter. This parameter is used to prepare rationalized
	parameters which are used here (see calc_params.c).

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure, with configuration data.
	    
Output:
	(1) Data added to each array element in a given MolComplexS
	    structure.
	(2) Return value.

Return value:
	The number of projected atoms.

Notes:
	(1) Project all atoms,  even if some  of them  are out of slab.
	    There may be some bonds connecting atoms inside and outside
	    the slab.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======project atoms:=======================================================*/

size_t ProjectAtoms_ (MolComplexS *mol_complexSP, int mol_complexesN,
		      ConfigS *configSP)
{
static size_t		projected_atomsN = 0;
static int		mol_complexI;
static int		imagesN, imageI;
static MolComplexS	*curr_mol_complexSP;
static double		nominator_x, nominator_y, user_atomic_position;
static double		x0, y0, z0, x1, y1;
static int		center_screen_x[2], center_screen_y;
static size_t		atomsN, atomI;
static AtomS		*curr_atomSP;
static double		denominator, reciprocal_denominator;
static double		rationalized_x, rationalized_y = 0;

/* The number of images (1 for mono, 2 for stereo): */
if (configSP->stereoF == 1) imagesN = 2;
else imagesN = 1;

/* Prepare the factors which are used to */
/* reduce the number of multiplications: */
nominator_x = configSP->user_screen_atomic_distance *
	      configSP->atomic_to_screen_scale_x;
nominator_y = configSP->user_screen_atomic_distance *
	      configSP->atomic_to_screen_scale_y;

/* Prepare the user position in atomic coordinates: */
user_atomic_position = configSP->user_atomic_position;

/* Copy range parameters: */
x0 = configSP->rationalized_x0;
y0 = configSP->rationalized_y0;
x1 = configSP->rationalized_x1;
y1 = configSP->rationalized_y1;
z0 = configSP->win_atomic_z0;

/* Position of the window free area center: */
center_screen_x[0] = configSP->center_screen_x[0];
center_screen_x[1] = configSP->center_screen_x[1];
center_screen_y = configSP->center_screen_y;

/* Project every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Do not project coordinates if position has not changed: */
	if (curr_mol_complexSP->position_changedF == 0) continue;

	/* Scan the entire macromolecular complex: */
	atomsN = curr_mol_complexSP->atomsN;
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/** Prepare the current atom pointer: **/
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/** Prepare one image (mono) or two images (stereo): **/
		for (imageI = 0; imageI < imagesN; imageI++)
			{
			/*** Prepare the factor required for projection: ***/
			denominator = curr_atomSP->raw_atomS.z[imageI] -
				      user_atomic_position;
			if (denominator == 0.0) continue;
			reciprocal_denominator = 1.0 / denominator;

			/*** If atom is on the wrong side of the screen, ***/
			/*** set the flag and  do not project this atom: ***/
			if (curr_atomSP->raw_atomS.z[imageI] < z0)
				{
				curr_atomSP->inside_windowF = 0;
				break;
				}

			/*** Calculate the projection to the ***/
			/*** screen,  in rationalized units: ***/
			rationalized_x = curr_atomSP->raw_atomS.x[imageI] *
					 nominator_x * reciprocal_denominator;
			rationalized_y = curr_atomSP->raw_atomS.y *
					 nominator_y * reciprocal_denominator;

			/*** Check is the atom at least partly visible: ***/
			if (rationalized_x < x0)
				{
				curr_atomSP->inside_windowF = 0;
				break;
				}
			else if (rationalized_x > x1)
				{
				curr_atomSP->inside_windowF = 0;
				break;
				}
			else if (rationalized_y < y0)
				{
				curr_atomSP->inside_windowF = 0;
				break;
				}
			else if (rationalized_y > y1)
				{
				curr_atomSP->inside_windowF = 0;
				break;
				}
			else curr_atomSP->inside_windowF = 1;

			/*** If this point is reached, atom is at least ***/
			/*** partly visible; calculate and store horiz. ***/
			/*** component of the projection to the screen: ***/
			curr_atomSP->raw_atomS.screen_x[imageI] =
				(int) rationalized_x + center_screen_x[imageI];
			}

		/** Check is anything visible; if not, take the next atom: **/
		if (curr_atomSP->inside_windowF == 0) continue;

		/** Vertical component of screen projection: **/
		curr_atomSP->raw_atomS.screen_y =
				(int) rationalized_y + center_screen_y;

		/** Increase the number of visible atoms: **/
		projected_atomsN++;
		}
	}

return projected_atomsN;
}

/*===========================================================================*/


