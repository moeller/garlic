/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				open_cfg_file.c

Purpose:
	Open configuration file. Up to seven directories are searched. If all
	attempts fail, return NULL.

Input:
	(1) Environment variable HOME used.

Output:
	(1) Return value. 

Return value:
	(1) File pointer, if file found.
	(2) NULL, if all attempts to find file failed.

Notes:
	(1) The following seven pathnames will be checked, if necessary:

	    (1) ./.garlicrc		    (current working directory)
	    (2) $HOME/.garlicrc		    (home directory)
	    (3) $HOME/garlic/.garlicrc
	    (4) /usr/share/garlic/.garlicrc (recomm.  system-wide default!)
	    (5) /etc/garlicrc               (note: file name is garlicrc !)
	    (6) /usr/local/lib/garlic/.garlicrc
	    (7) /usr/lib/garlic/.garlicrc

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"

/*======try to find and open .garlicrc file:=================================*/

FILE *OpenConfigFile_ (void)
{
static FILE	*fileP;
static char	tildaA[10] = "~";
char		*home_dirP;
int		n;
char		config_file_nameA[STRINGSIZE];

/* The first attempt - try to find .garlicrc file in current directory: */
if ((fileP = fopen (".garlicrc", "r")) != NULL)
	{
	return fileP;
	}

/* The second attempt - try to find .garlicrc file in users home directory: */
do
	{
	if ((home_dirP = getenv ("HOME")) != NULL) break;
	if ((home_dirP = getenv ("home")) != NULL) break;
	home_dirP = tildaA;
	} while (0);

n = STRINGSIZE - 100;
strncpy (config_file_nameA, home_dirP, n);
config_file_nameA[n] = '\0';
strncat (config_file_nameA, "/.garlicrc", 11);
config_file_nameA[STRINGSIZE - 1] = '\0';
if ((fileP = fopen (config_file_nameA, "r")) != NULL)
	{
	return fileP;
	}

/* The third attempt - try to find personal file in $HOME/garlic directory: */
n = STRINGSIZE - 100;
strncpy (config_file_nameA, home_dirP, n);
config_file_nameA[n] = '\0';
strncat (config_file_nameA, "/garlic/.garlicrc", 18);
config_file_nameA[STRINGSIZE - 1] = '\0';
if ((fileP = fopen (config_file_nameA, "r")) != NULL)
	{
	return fileP;
	}

/* The fourth attempt  -  /usr/share/garlic/.garlicrc . */
/* This pathname is recommended as system-wide default! */
if ((fileP = fopen ("/usr/share/garlic/.garlicrc", "r")) != NULL)
        {
        return fileP;
        }

/* The fifth attempt - /etc/garlicrc, default for */
/* Debian Linux  (the file name  is  garlicrc !): */
if ((fileP = fopen ("/etc/garlicrc", "r")) != NULL)
	{
	return fileP;
	}

/* The sixth attempt - /usr/local/lib/garlic directory: */
if ((fileP = fopen ("/usr/local/lib/garlic/.garlicrc", "r")) != NULL)
	{
	return fileP;
	}

/* The seventh attempt - /usr/lib/garlic directory: */
if ((fileP = fopen ("/usr/lib/garlic/.garlicrc", "r")) != NULL)
	{
	return fileP;
	}

/* If this point is reached, all attempts to */
/* open the configuration file  have failed: */
return NULL;
}

/*===========================================================================*/


