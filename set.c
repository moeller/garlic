/* Copyright (C) 2001, 2002 Damir Zucic */

/*=============================================================================

				    set.c

Purpose:
	Execute the command set: set the value of phi, psi, omega, chi1, chi2
	or  chi3  angle  for all  selected residues.  A residue is treated as
	selected if the first atom of this residue is selected.  For standard
	protein residues, this is typically N atom (nitrogen).

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of  the command string.  The command set
	    must be accompanied by one of the following keywords:  PHI,  PSI,
	    OMEGA, CHI1, CHI2, CHI3, CHI4 and CHI5. The keyword  OMEGA may be
	    abbreviated to  three characters.  Do not abbreviate the keywords
	    CHI1, CHI2, CHI3, CHI4 and CHI5.  In addition to one keyword, the
	    remainder of  the string must contain  the numeric value (double)
	    of the specified angle in degrees.

Output:
	(1) The specified dihedral angle will be changed.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The side chain dihedral angles will not be set for proline.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		SetPhi_ (MolComplexS *, ConfigS *, double);
int		SetPsi_ (MolComplexS *, ConfigS *, double);
int		SetOmega_ (MolComplexS *, ConfigS *, double);
int		SetChi1_ (MolComplexS *, ConfigS *, double);
int		SetChi2_ (MolComplexS *, ConfigS *, double);
int		SetChi3_ (MolComplexS *, ConfigS *, double);
int		SetChi4_ (MolComplexS *, ConfigS *, double);
int		SetChi5_ (MolComplexS *, ConfigS *, double);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute the command set:=============================================*/

int Set_ (MolComplexS *mol_complexSP, int mol_complexesN,
	  RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	  NearestAtomS *nearest_atomSP, size_t pixelsN,
	  unsigned int *refreshIP, char *stringP)
{
char			*remainderP;
char			first_tokenA[SHORTSTRINGSIZE];
char			second_tokenA[SHORTSTRINGSIZE];
char			*P;
int			n;
double			angle;
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;

/* Extract the first token: */
remainderP = ExtractToken_ (first_tokenA, STRINGSIZE, stringP, " \t\n");

/* If the first token is not available, inform */
/* the user about  the syntax of this command: */
if (!remainderP)
	{
	strcpy (runtimeSP->messageA,
		"Use the following syntax: set angle_name angle_in_degrees");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SET;
	}

/* If this point is reached, the first token is present. */

/* Extract the second token (the angle in degrees): */
remainderP = ExtractToken_ (second_tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "The angle is missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SET;
	}

/* Try to extract the numeric value of the angle. */

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P = second_tokenA;
while ((n = *P++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
	}

/* Try to extract the numeric value: */
if (sscanf (second_tokenA, "%lf", &angle) != 1)
	{
	strcpy (runtimeSP->messageA, "Unable to extract the angle!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SET;
	}

/* Convert degrees to radians: */
angle *= DEG_TO_RAD;

/* The specified angle should be set for each */
/* selected residue  in each  caught complex. */

/* Scan the array of macromolecular complexes: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is this complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Now try to recognize the keyword which specifies the angle. */

	/* Keyword PHI: */
	if (strstr (first_tokenA, "PHI") == first_tokenA)
		{
		SetPhi_ (curr_mol_complexSP, configSP, angle);
		}

	/* Keyword PSI: */
	else if(strstr (first_tokenA, "PSI") == first_tokenA)
		{
		SetPsi_ (curr_mol_complexSP, configSP, angle);
		}

	/* Keyword OMEGA: */
	else if (strstr (first_tokenA, "OME") == first_tokenA)
		{
		SetOmega_ (curr_mol_complexSP, configSP, angle);
		}

	/* Keyword CHI1: */
	else if (strstr (first_tokenA, "CHI1") == first_tokenA)
		{
		SetChi1_ (curr_mol_complexSP, configSP, angle);
		}

	/* Keyword CHI2: */
	else if (strstr (first_tokenA, "CHI2") == first_tokenA)
		{
		SetChi2_ (curr_mol_complexSP, configSP, angle);
		}

	/* Keyword CHI3: */
	else if (strstr (first_tokenA, "CHI3") == first_tokenA)
		{
		SetChi3_ (curr_mol_complexSP, configSP, angle);
		}

	/* Keyword CHI4: */
	else if (strstr (first_tokenA, "CHI4") == first_tokenA)
		{
		SetChi4_ (curr_mol_complexSP, configSP, angle);
		}

	/* Keyword CHI5: */
	else if (strstr (first_tokenA, "CHI5") == first_tokenA)
		{
		SetChi5_ (curr_mol_complexSP, configSP, angle);
		}
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_SET;
}

/*===========================================================================*/


