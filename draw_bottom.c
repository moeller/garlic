/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				draw_bottom.c

Purpose:
	Draw the exposed polar residues  at the surface of  the bottom
	complex. This complex is colored red. Hydrogen bond donors are
	shown as crosses,  acceptors as circles and residues which may
	be both donors and acceptors are shown as combined crosses and
	circles.

Input:
	(1) Pointer to GUIS structure.
	(2) Pointer to RuntimeS structure.

Output:
	(1) Symbols (crosses and circles) drawn to docking window.
	(2) Return value.

Return value:
	The number of symbols.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======draw exposed polar residues of the bottom complex:===================*/

int DrawBottom_ (GUIS *guiSP, RuntimeS *runtimeSP)
{
int			symbolsN = 0;
int			exposed_polarN, exposed_polarI;
MolComplexS		*curr_mol_complexSP;
int			*exposed_atomIP;
ExposedResidueS		*exposed_polarSP;
int			geom_returnF;
Window			root_windowID;
int			x0, y0;
unsigned int		width, height, border_width, bpp;
int			square_width, half_square_width;
int			screen_center_x0, screen_center_y0;
int			screen_x0, screen_y0, screen_x1, screen_y1;
int			screen_delta_x, screen_delta_y;
double			docking_area_width;
double			scale_factor;
int			atom_screen_radius;
double			plane_center_x, plane_center_z;
ExposedResidueS		*curr_exposedSP;
AtomS			*curr_atomSP;
double			x, z;
double			relative_x, relative_z;
double			d;
int			screen_x, screen_y;
double			donorI;

/* Copy pointers: */
exposed_polarN = runtimeSP->exposed_polar1N;
curr_mol_complexSP = runtimeSP->mol_complex1SP;
exposed_atomIP = runtimeSP->exposed_atom1IP;
exposed_polarSP = runtimeSP->exposed_polar1SP;

/* Docking window geometry: */
geom_returnF = XGetGeometry(guiSP->displaySP,
			    guiSP->docking_winS.ID,
			    &root_windowID,
                            &x0, &y0, &width, &height,
			    &border_width, &bpp);

/* Refresh docking window: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->black_colorID);
XFillRectangle (guiSP->displaySP, guiSP->docking_winS.ID, guiSP->theGCA[0],
		0, 0, width, height);

/* Prepare some geometric parameters: */
square_width = (int) width - 8 * (int) guiSP->docking_winS.border_width;
if (height < width) square_width = (int) height -
				   8 * (int) guiSP->docking_winS.border_width;
half_square_width = square_width / 2;
screen_center_x0 = width  / 2;
screen_center_y0 = height / 2;
screen_x0 = screen_center_x0 - half_square_width;
screen_y0 = screen_center_y0 - half_square_width;
screen_x1 = screen_center_x0 + half_square_width;
screen_y1 = screen_center_y0 + half_square_width;
screen_delta_x = screen_x1 - screen_x0;
screen_delta_y = screen_y1 - screen_y0;
docking_area_width = runtimeSP->docking_area_width;
if (docking_area_width != 0.0)
	{
	scale_factor = square_width / docking_area_width;
	}
else scale_factor = 0.0;
atom_screen_radius = (int) (scale_factor * 2.0);

/* Draw bounding rectangle (white): */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->white_colorID);
XDrawRectangle (guiSP->displaySP, guiSP->docking_winS.ID, guiSP->theGCA[0],
		screen_x0, screen_y0,
		(unsigned int) screen_delta_x, (unsigned int) screen_delta_y);

/* Copy the plane center position (plane belongs to bottom complex): */
plane_center_x = runtimeSP->mol_complex1SP->planeS.center_x[0];
plane_center_z = runtimeSP->mol_complex1SP->planeS.center_z[0];

/* Prepare cyan color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->cyan_colorID);

/* Scan the list of exposed polar residues: */
for (exposed_polarI = 0; exposed_polarI < exposed_polarN; exposed_polarI++)
	{
	/* Pointer to the current exposed polar residue: */
	curr_exposedSP = exposed_polarSP + exposed_polarI;

	/* If this residue is excluded check the next one: */
	if (curr_exposedSP->excludedF) continue;

	/* Pointer to the representative atom: */
	curr_atomSP = curr_mol_complexSP->atomSP +
	curr_exposedSP->representative_atomI;

	/* Copy the atomic coordinates: */
	x = curr_atomSP->raw_atomS.x[0];
	z = curr_atomSP->raw_atomS.z[0];

	/* Position of the atom relative to the plane center: */
	relative_x = x - plane_center_x;
	relative_z = z - plane_center_z;

	/* Atomic coordinates in screen units: */
	d = scale_factor * relative_z + screen_center_x0;
	screen_x = (int) d;
	d = scale_factor * relative_x + screen_center_y0;
	screen_y = (int) d;

	/* Check the screen coordinates: */
	if ((screen_x < screen_x0) || (screen_x > screen_x1)) continue;
	if ((screen_y < screen_y0) || (screen_y > screen_y1)) continue;

	/* Prepare the color index: */
	donorI = (int) curr_exposedSP->donorI;

	/* If the current residue is hydrogen bond donor, draw cross: */
	if ((donorI == 1) || (donorI == 2))
		{
		XDrawLine (guiSP->displaySP,
			   guiSP->docking_winS.ID,
			   guiSP->theGCA[0],
			   screen_x, screen_y + atom_screen_radius,
			   screen_x, screen_y - atom_screen_radius);
		XDrawLine (guiSP->displaySP,
			   guiSP->docking_winS.ID,
			   guiSP->theGCA[0],
			   screen_x - 1, screen_y + atom_screen_radius,
			   screen_x - 1, screen_y - atom_screen_radius);
		XDrawLine (guiSP->displaySP,
			   guiSP->docking_winS.ID,
			   guiSP->theGCA[0],
			   screen_x + 1, screen_y + atom_screen_radius,
			   screen_x + 1, screen_y - atom_screen_radius);
		XDrawLine (guiSP->displaySP,
			   guiSP->docking_winS.ID,
			   guiSP->theGCA[0],
			   screen_x - atom_screen_radius, screen_y,
			   screen_x + atom_screen_radius, screen_y);
		XDrawLine (guiSP->displaySP,
			   guiSP->docking_winS.ID,
			   guiSP->theGCA[0],
			   screen_x - atom_screen_radius, screen_y - 1,
			   screen_x + atom_screen_radius, screen_y - 1);
		XDrawLine (guiSP->displaySP,
			   guiSP->docking_winS.ID,
			   guiSP->theGCA[0],
			   screen_x - atom_screen_radius, screen_y + 1,
			   screen_x + atom_screen_radius, screen_y + 1);
		}
	
	/* If the current residue is hydrogen bond acceptor, draw circle: */
	if ((donorI == 0) || (donorI == 2))
		{
		XDrawArc (guiSP->displaySP,
			  guiSP->docking_winS.ID,
			  guiSP->theGCA[0],
			  screen_x - atom_screen_radius,
			  screen_y - atom_screen_radius,
			  (unsigned int) (2 * atom_screen_radius),
			  (unsigned int) (2 * atom_screen_radius),
			  0, 23040);
		XDrawArc (guiSP->displaySP,
			  guiSP->docking_winS.ID,
			  guiSP->theGCA[0],
			  screen_x - atom_screen_radius - 1,
			  screen_y - atom_screen_radius - 1,
			  (unsigned int) (2 * atom_screen_radius + 2),
			  (unsigned int) (2 * atom_screen_radius + 2),
			  0, 23040);
		}

	/* Increment the counter: */
	symbolsN++;
	}

/* Return the number of symbols drawn: */
return symbolsN;
}

/*===========================================================================*/


