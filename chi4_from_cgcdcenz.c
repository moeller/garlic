/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				chi4_from_cgcdcenz.c

Purpose:
	Calculate dihedral angle chi4, using CG, CD, CE and NZ coordinates.

Input:
	(1) Pointer to AtomS structure, pointing to the first atom of the
	    current macromolecular complex.
	(2) Index of the first atom of the current residue.
        (3) Index of the last atom of the currrent residue.

Output:
	Return value.

Return value:
	(1) Dihedral angle chi4, on success.
	(2) BADDIHEDANGLE on failure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractFourAtoms_ (VectorS *, VectorS *, VectorS *, VectorS *,
				   char *, char *, char *, char *,
				   AtomS *, size_t, size_t);
void		VectorProduct_ (VectorS *, VectorS *, VectorS *);
double		AbsoluteValue_ (VectorS *);
double		ScalarProduct_ (VectorS *, VectorS *);

/*======calculate chi4 from CG, CD, CE and NZ:===============================*/

double Chi4FromCGCDCENZ_ (AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
static VectorS		CG_vectorS, CD_vectorS, CE_vectorS, NZ_vectorS;
int			n;
VectorS			CD_CG_vectorS, CD_CE_vectorS;
VectorS			CE_CD_vectorS, CE_NZ_vectorS;
VectorS			u1S, u2S;
VectorS			v1S, v2S;
double			denom, ratio, alpha;
double			chi4;

/* Extract CG, CD, CE and NZ coordinates: */
n = ExtractFourAtoms_ (&CG_vectorS, &CD_vectorS, &CE_vectorS, &NZ_vectorS,
		       "CG", "CD", "CE", "NZ",
		       atomSP, atom_startI, atom_endI);

/* All four atoms are required to calculate the angle chi4: */
if (n < 4) return BADDIHEDANGLE;

/* The first pair of auxiliary vectors: */
CD_CG_vectorS.x = CG_vectorS.x - CD_vectorS.x;
CD_CG_vectorS.y = CG_vectorS.y - CD_vectorS.y;
CD_CG_vectorS.z = CG_vectorS.z - CD_vectorS.z;
CD_CE_vectorS.x = CE_vectorS.x - CD_vectorS.x;
CD_CE_vectorS.y = CE_vectorS.y - CD_vectorS.y;
CD_CE_vectorS.z = CE_vectorS.z - CD_vectorS.z;

/* The second pair of auxiliary vectors: */
CE_CD_vectorS.x = CD_vectorS.x - CE_vectorS.x;
CE_CD_vectorS.y = CD_vectorS.y - CE_vectorS.y;
CE_CD_vectorS.z = CD_vectorS.z - CE_vectorS.z;
CE_NZ_vectorS.x = NZ_vectorS.x - CE_vectorS.x;
CE_NZ_vectorS.y = NZ_vectorS.y - CE_vectorS.y;
CE_NZ_vectorS.z = NZ_vectorS.z - CE_vectorS.z;

/* Two vectors  perpendicular to  CD_CE_vectorS,  mutually orthogonal, */
/* the second in the plane defined by CD_CG_vectorS and CD_CE_vectorS: */
VectorProduct_ (&u1S, &CD_CG_vectorS, &CD_CE_vectorS);
VectorProduct_ (&u2S, &u1S, &CD_CE_vectorS);

/* Two vectors  perpendicular to  CE_CD_vectorS,  mutually orthogonal, */
/* the second in the plane defined by CE_CD_vectorS and CE_NZ_vectorS: */
VectorProduct_ (&v1S, &CE_CD_vectorS, &CE_NZ_vectorS);
VectorProduct_ (&v2S, &CE_CD_vectorS, &v1S);

/* Calculate the angle alpha, which will be used to calculate chi4: */

/* Avoid division by zero: */
denom = AbsoluteValue_ (&u1S) * AbsoluteValue_ (&v1S);
if (denom == 0.0) return BADDIHEDANGLE;

/* Use the scalar product to calculate the cosine of the angle: */
ratio = ScalarProduct_ (&u1S, &v1S) / denom;

/* Arc cosine is very sensitive to floating point errors: */
if (ratio <= -1.0) alpha = 3.1415927;
else if (ratio >= 1.0) alpha = 0.0;
else alpha = acos (ratio);

/* There are two possible solutions; the right one is resolved here: */
if (ScalarProduct_ (&v2S, &u1S) >= 0) chi4 = alpha;
else chi4 = -alpha;

/* Return the angle (in radians): */
return chi4;
}

/*===========================================================================*/


