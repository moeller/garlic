/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				generate_plane.c

Purpose:
	Generate the plane for a given macromolecular complex. The plane
	is defined by the plane center,  normal vector and plane radius.
	It may be  partially transparent or opaque.  Stereo data  should
	be prepared.  Each plane is initially tilted toward observer, so
	that the top surface is visible.

Input:
	(1) Pointer to MolComplexS structure,  with macromolecular data.
	(2) Pointer to ConfigS structure, with configuration data.
	(3) Pointer to GUIS structure, with GUI data.

Output:
	(1) Plane generated for a given macromolecular complex.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Zero, if complex contains no atoms (no geometric center).

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);

/*======generate plane:======================================================*/

int GeneratePlane_ (MolComplexS *mol_complexSP, ConfigS *configSP, GUIS *guiSP)
{
size_t			atomsN;
double			tilt_angle;
double			sin_angle, cos_angle;

/* Return zero if there are no atoms: */
atomsN = mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* The plane is initially hidden: */
mol_complexSP->planeS.hiddenF = 1;

/* The plane center: */
mol_complexSP->planeS.center_x[0] = mol_complexSP->geometric_center_vectorS.x;
mol_complexSP->planeS.center_x[1] = mol_complexSP->planeS.center_x[0];
mol_complexSP->planeS.center_y    = mol_complexSP->geometric_center_vectorS.y;
mol_complexSP->planeS.center_z[0] = mol_complexSP->geometric_center_vectorS.z;
mol_complexSP->planeS.center_z[1] = mol_complexSP->planeS.center_z[0];

/* Circle radius: */
mol_complexSP->planeS.circle_radius = (0.5 + CIRCLEMARGIN) *
				      mol_complexSP->max_extent;

/* The plane tilt angle, in radians: */
tilt_angle = CIRCLETILT * DEG_TO_RAD;

/* The normal vector (perpendicular to x axis): */
mol_complexSP->planeS.normal_x[0] = 0;
mol_complexSP->planeS.normal_y = -cos (tilt_angle);
mol_complexSP->planeS.normal_z[0] = -sin (tilt_angle);

/* Prepare the sine and cosine of the stereo angle: */
sin_angle = sin (configSP->stereo_angle);
cos_angle = cos (configSP->stereo_angle);

/* Prepare stereo data for the normal vector: */
mol_complexSP->planeS.normal_x[1] = 
			 mol_complexSP->planeS.normal_x[0] * cos_angle +
			 mol_complexSP->planeS.normal_z[0] * sin_angle;
mol_complexSP->planeS.normal_z[1] =
			-mol_complexSP->planeS.normal_x[0] * sin_angle +
			 mol_complexSP->planeS.normal_z[0] * cos_angle;

/* Define colors. */

/* Top side near color: */
ParseColor_ (&mol_complexSP->planeS.top_near_rgbS,
	     &mol_complexSP->planeS.top_near_colorID,
	     guiSP, "RGB:0000/8888/FFFF", "white");

/* Top side far color: */
ParseColor_ (&mol_complexSP->planeS.top_far_rgbS,
	     &mol_complexSP->planeS.top_far_colorID,
	     guiSP, "RGB:0000/2222/4444", "black");

/* Bottom side near color: */
ParseColor_ (&mol_complexSP->planeS.bottom_near_rgbS,
	     &mol_complexSP->planeS.bottom_near_colorID,
	     guiSP, "RGB:DDDD/8888/8888", "white");

/* Bottom side far color: */
ParseColor_ (&mol_complexSP->planeS.bottom_far_rgbS,
	     &mol_complexSP->planeS.bottom_far_colorID,
	     guiSP, "RGB:3333/2222/2222", "black");

/* Initialize transparency: */
mol_complexSP->planeS.transparency = DEFAULT_TRANSP;

/* If this point is reached, return positive value (success indicator): */
return 1;
}

/*===========================================================================*/


