/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				prepare_backbone.c

Purpose:
	Prepare the array of BackboneS structures:  extract data about CA
	atoms.  Check distances between neighbouring CA atoms. Set flags.

Input:
	(1) Pointer to  MolComplexS structure.
	(2) Pointer to ConfigS structure.

Output:
	(1) The array of  BackboneS  structures allocated and filled with
	    data about C-alpha atoms.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Zero if there are no atoms.
	(3) Negative on failure (memory allocation may fail).

========includes:============================================================*/

#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======prepare backbone data:===============================================*/

int PrepareBackbone_ (MolComplexS *mol_complexSP, ConfigS *configSP)
{
size_t		atomI, atomsN;
AtomS		*curr_atomSP;
int		c_alphaN = 0;
size_t		elementsN;
size_t		struct_size;
size_t		mem_size;
BackboneS	*curr_backboneSP;
int		c_alphaI;
size_t		atom1I, atom2I;
AtomS		*atom0SP, *atom1SP, *atom2SP;
double		x0, y0, z0, x1, y1, z1, x2, y2, z2, delta_x, delta_y, delta_z;
double		d1_squared, d2_squared;
double		max_dist_squared;

/* Initialize the number of CA atoms: */
mol_complexSP->c_alphaN = 0;

/* Return zero if there are no atoms in this macromolecular complex: */
atomsN = mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* The maximal distance squared: */
max_dist_squared = configSP->CA_CA_dist_max_squared;

/* Count CA atoms: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Current AtomS pointer: **/
	curr_atomSP = mol_complexSP->atomSP + atomI;

	/** Check the purified atom name: **/
	if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, "CA") == 0)
		{
		c_alphaN++;
		}
	}

/* Allocate memory for the array of BackboneS structures: */

/** Add some extra space: **/
elementsN = c_alphaN + 100;
struct_size = sizeof (BackboneS);
mem_size = elementsN * struct_size;

/** Allocate memory: **/
mol_complexSP->backboneSP = (BackboneS *) calloc (elementsN, struct_size);

/** If something goes wrong, inform user and return negative value: **/
if (mol_complexSP->backboneSP == NULL)
	{
	ErrorMessage_ ("garlic", "PrepareBackbone_", "",
		       "Failed to allocate memory for BackboneS array!\n",
		       "", "", "");
	mem_size = elementsN * struct_size;
	fprintf (stderr, "A total of %d bytes required.\n", mem_size);
	return -1;
	}

/* Initialize the BackboneS pointer: */
curr_backboneSP = mol_complexSP->backboneSP;

/* Scan the macromolecular complex again and extract required data: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to the current atom: **/
	curr_atomSP = mol_complexSP->atomSP + atomI;

	/** Check the purified atom name: **/
	if (strcmp (curr_atomSP->raw_atomS.pure_atom_nameA, "CA") != 0)
			continue;

	/** Copy the index of the current atom: **/
	curr_backboneSP->c_alphaI = atomI;

	/** Update the BackboneS pointer: */
	curr_backboneSP++;
	}

/* Copy the number of CA atoms: */
mol_complexSP->c_alphaN  = c_alphaN;

/* Check distances between neighbouring CA atoms: */
for (c_alphaI = 0; c_alphaI < c_alphaN; c_alphaI++)
	{
	/** Pointer to the current BackboneS: **/
	curr_backboneSP = mol_complexSP->backboneSP + c_alphaI;

	/** Pointer to the current CA atom: **/
	atom0SP = mol_complexSP->atomSP + curr_backboneSP->c_alphaI;

	/** Position of the current CA atom: **/
	x0 = atom0SP->raw_atomS.x[0];
	y0 = atom0SP->raw_atomS.y;
	z0 = atom0SP->raw_atomS.z[0];

	/** Check distances between current CA, previous CA and next CA: **/

	/*** For the first CA, the previous CA does not exist: ***/
	if (c_alphaI > 0)
		{
		atom1I = (curr_backboneSP - 1)->c_alphaI;
		atom1SP = mol_complexSP->atomSP + atom1I;
		x1 = atom1SP->raw_atomS.x[0];
		y1 = atom1SP->raw_atomS.y;
		z1 = atom1SP->raw_atomS.z[0];
		delta_x = x1 - x0;
		delta_y = y1 - y0;
		delta_z = z1 - z0;
		d1_squared = delta_x * delta_x +
			     delta_y * delta_y +
			     delta_z * delta_z;
		if (d1_squared <= max_dist_squared)
			{
			curr_backboneSP->previous_c_alphaF = 1;
			curr_backboneSP->previous_c_alphaI = atom1I;
			}
		else curr_backboneSP->previous_c_alphaF = 0;
		}

	/*** For the last CA, the next CA does not exist: ***/
	if (c_alphaI < c_alphaN - 1)
		{
		atom2I = (curr_backboneSP + 1)->c_alphaI;
		atom2SP = mol_complexSP->atomSP + atom2I;
		x2 = atom2SP->raw_atomS.x[0];
		y2 = atom2SP->raw_atomS.y;
		z2 = atom2SP->raw_atomS.z[0];
		delta_x = x2 - x0;
		delta_y = y2 - y0;
		delta_z = z2 - z0;
		d2_squared = delta_x * delta_x +
			     delta_y * delta_y +
			     delta_z * delta_z;
		if (d2_squared <= max_dist_squared)
			{
			curr_backboneSP->next_c_alphaF = 1;
			curr_backboneSP->next_c_alphaI = atom2I;
			}
		else curr_backboneSP->next_c_alphaF = 0;
		}
	}

/* Initialize backbone drawing style and hide all backbone elements: */
for (c_alphaI = 0; c_alphaI < c_alphaN; c_alphaI++)
	{
	curr_backboneSP = mol_complexSP->backboneSP + c_alphaI;
	curr_backboneSP->backbone_styleI = -1;
	curr_backboneSP->hiddenF = 1;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


