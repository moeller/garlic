/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				parse_color.c

Purpose:
	Parse color string and prepare color structure and color ID.

Input:
	(1) Pointer to RGBS structure, where color components will be stored.
	(2) Pointer to color ID (unsigned long).
	(3) Pointer to GUIS structure, with input data.
	(4) Color string pointer.

Output:
	(1) XColor structure initialized.
	(2) Color ID initialized.
	(3) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/cursorfont.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		RGBSFromXColor_ (RGBS *, XColor *);
unsigned long	PixelFromRGBS_ (RGBS *, GUIS *);

/*======parse color string:==================================================*/

int ParseColor_ (RGBS *rgbSP, unsigned long *colorIDP,
		 GUIS *guiSP, char *color_stringP, char *default_color_stringP)
{
Status		status;
XColor		colorS;

/* The first attempt: */
status = XParseColor (guiSP->displaySP, guiSP->colormapID,
		      color_stringP, &colorS);
if (status)
	{
	RGBSFromXColor_ (rgbSP, &colorS);
	*colorIDP = PixelFromRGBS_ (rgbSP, guiSP);
	return 1;
	}

/* The second attempt (if the first one failed): */
status = XParseColor (guiSP->displaySP, guiSP->colormapID,
		      default_color_stringP, &colorS);
if (status)
	{
	RGBSFromXColor_ (rgbSP, &colorS);
	*colorIDP = PixelFromRGBS_ (rgbSP, guiSP);
	return 2;
	}

/* If this point is reached, both attempts to allocate color failed: */
return -1;
}

/*===========================================================================*/


