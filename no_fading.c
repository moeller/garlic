/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				no_fading.c

Purpose:
	Prepare the left, middle and right color for each atom in a complex.
	No fading in  this function - near atom  colors  are used.  White is
	used as a replacement color if color allocation fails.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Pointer to GUIS structure.

Output:
	(1) left_colorID, middle_colorID and right_colorID members of  AtomS
	    structure initialized for each atom in macromolecular complex.
	(2) Return value.

Return value:
	Zero always (trivial).

Notes:
	(1) Do not skip hidden atoms,  the color of these atoms  may be used
	    for backbone drawing!

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

unsigned long	PixelFromRGBS_ (RGBS *, GUIS *);

/*======fading not used:=====================================================*/

size_t NoFading_ (MolComplexS *curr_mol_complexSP, GUIS *guiSP)
{
size_t			atoms_between_surfacesN = 0;
size_t			atomsN, atomI;
XColor			colorS;
AtomS			*curr_atomSP;

/* Prepare and check the number of atoms in a complex: */
atomsN = curr_mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* Set DoRed, DoGreen and DoBlue flags in colorS: */
colorS.flags = DoRed | DoGreen | DoBlue;

/* Assign the same set of three color ID's to each atom: */
for (atomI = 0; atomI < atomsN; atomI++)
        {
	/** Prepare the current atom pointer: **/
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/** Is atom out of slab? **/
	if (!curr_atomSP->inside_slabF) continue;

	/** Prepare color ID's (use near atom colors): **/
	curr_atomSP->left_colorID   =
		PixelFromRGBS_ (curr_atomSP->left_rgbSA, guiSP);
	curr_atomSP->middle_colorID =
		PixelFromRGBS_ (curr_atomSP->middle_rgbSA, guiSP);
	curr_atomSP->right_colorID  =
		PixelFromRGBS_ (curr_atomSP->right_rgbSA, guiSP);
	}

/* Return the total number of allocated colors: */
return atoms_between_surfacesN;
}

/*===========================================================================*/


