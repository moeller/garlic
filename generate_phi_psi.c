/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

			    generate_phi_psi.c

Purpose:
	Generate the angles phi and psi. Use the specified secondary
	structure code. The pseudo-random seed should be initialized
	somewhere else (check the function ApplyStructure_ ()). Note
	that small deviations from ideal values are introduced. This
	is good if you want to prepare the Ramachandran plot for the
	new structure.

Input:
	(1) Pointer to double, where phi angle will be stored.
	(2) Pointer to double, where psi angle will be stored.
	(3) Secondary structure one letter code.

Output:
	(1) Angles psi and psi generated.
	(2) Return value.

Return value:
	(1) Positive if code is recognized.
	(2) Negative if code is not recognized.

========includes:============================================================*/

#include <stdio.h>

#include <stdlib.h>
#include <math.h>

#include "defines.h"

/*======generate angles phi and psi:=========================================*/

int GeneratePhiAndPsi_ (double *phiP, double *psiP, int sec_structure_code)
{
double		phi0, psi0;
double		max_value, reciprocal_denominator;
double		radius, angle;
double		delta_phi, delta_psi;

/* Prepare the mean values: */
switch (sec_structure_code)
	{
	/* Beta sheet, coil, turn: */
	case 'E':
	case 'C':
	case 'T':
		phi0 = -2.426008;	/* -139.0 degrees */
		psi0 =  2.356195;	/* +135.0 degrees */
		break;

	/* Alpha helix: */
	case 'H':
		phi0 = -0.994838;	/* -57.0 degrees */
		psi0 = -0.820305;	/* -47.0 degrees */
		break;

	/* 3-10 helix: */
	case 'G':
		phi0 = -1.239184;	/* -71.0 degrees */
		psi0 = -0.314159;	/* -18.0 degrees */
		break;

	/* Do not change phi and psi if code is not recognized: */
	default:
		return -1;
	}

/* Prepare denominator: */
max_value = (double) RAND_MAX;
if (max_value != 0.0) reciprocal_denominator = 1.0 / max_value;
else reciprocal_denominator = 1.0;

/* Generate small deviations: */
radius = (double) rand () * reciprocal_denominator;
angle = 6.2831853 * ((double) rand () * reciprocal_denominator);
delta_phi = 0.087266463 * radius * cos (angle);
delta_psi = 0.087266463 * radius * sin (angle);

/* Combine mean values and deviations: */
*phiP = phi0 + delta_phi;
*psiP = psi0 + delta_psi;

/* Return positive value: */
return 1;
}

/*===========================================================================*/


