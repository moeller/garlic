/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				geomcenter.c

Purpose:
	Find geometric center position. Add it to MolComplexS structure.

Input:
	(1) Pointer to MolComplexS structure (macromolecular complex).

Output:
	(1) Geometric center vector stored to MolComplexS structure.
	(2) Return value.

Return value:
	(1) Positive if there are some atoms.
	(2) Zero if there are no atoms at all.

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======find geometric center position:======================================*/

int GeometricCenter_ (MolComplexS *mol_complexSP)
{
size_t		atomsN, i;
AtomS		*curr_atomSP;
double		x = 0.0, y = 0.0, z = 0.0, denom;

atomsN = mol_complexSP->atomsN;
if (atomsN <= 0) return 0;

curr_atomSP = mol_complexSP->atomSP;
for (i = 0; i < atomsN; i++)
	{
	x += curr_atomSP->raw_atomS.x[0];
	y += curr_atomSP->raw_atomS.y;
	z += curr_atomSP->raw_atomS.z[0];
	curr_atomSP++;
	}
denom = 1.0 / (double) atomsN;	/* This is safe: atomsN is checked above! */
mol_complexSP->geometric_center_vectorS.x = x * denom;
mol_complexSP->geometric_center_vectorS.y = y * denom;
mol_complexSP->geometric_center_vectorS.z = z * denom;

return 1;
}

/*===========================================================================*/


