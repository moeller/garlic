/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				required_atoms.c

Purpose:
	Count the number of atoms  required for the new structure.  This
	is done before creating the structure, to calculate the required
	storage.

Input:
	(1) Pointer to RuntimeS structure, with template atoms.
	(2) The number of residues in the new structure.

Output:
	(1) Return value.

Return value:
	(1) The number of atoms required for the new structure.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======number of atoms required for the  new structure:=====================*/

int CountRequiredAtoms_ (RuntimeS *runtimeSP, int residuesN)
{
int		total_atomsN = 0;
int		max_length;
int		residueI;
char		*curr_residue_nameP;
int		template_residuesN, template_residueI;
ResidueS	*template_residueSP;
AtomS		*first_atomSP;
char		*curr_template_nameP;
int		atomsN;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* The number of template residues: */
template_residuesN = runtimeSP->template_residuesN;

/* Scan the sequence: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Prepare the pointer to the current residue name: */
	curr_residue_nameP = runtimeSP->sequenceP + residueI * max_length;

	/* Scan the list of template residues: */
	for (template_residueI = 0;
	     template_residueI < template_residuesN;
	     template_residueI++)
		{
		/* Pointer to the current template residue: */
		template_residueSP = runtimeSP->template_residueSP +
				     template_residueI;

		/* Pointer to  the first atom of */
		/* the current template residue: */
		first_atomSP = runtimeSP->template_atomSP +
			       template_residueSP->residue_startI;

		/* Pointer to the current template residue name: */
		curr_template_nameP =
				first_atomSP->raw_atomS.pure_residue_nameA;

		/* Compare  the current residue name */
		/* with  the template  residue name; */
		/* if they match, prepare the number */
		/* of atoms and add it to the total. */
		if (strncmp (curr_residue_nameP,
			     curr_template_nameP, max_length) == 0)
			{
			atomsN = (int) template_residueSP->residue_endI -
				 (int) template_residueSP->residue_startI + 1;
			total_atomsN += atomsN;
			break;
			}
		}
	}

/* Return the total number of atoms: */
return total_atomsN;
}

/*===========================================================================*/


