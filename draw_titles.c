/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				draw titles.c

Purpose:
	Draw titles.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to GUIS structure.

Output:
	(1) Titles drawn to the hidden pixmap.
	(2) Return value.

Return value:
	The number of titles drawn.

=============================================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======draw titles:=========================================================*/

int DrawTitles_ (RuntimeS *runtimeSP, GUIS *guiSP)
{
int		titles_drawnN = 0;
int		titleI;
char		*curr_titleP;
int		curr_title_length;
int		screen_x, screen_y;

/* Prepare the text color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->main_winS.fg_colorID);

/* Scan the set of titles: */
for (titleI = 0; titleI < MAXTITLES; titleI++)
	{
	/** Do not draw hidden titles: **/
	if (runtimeSP->title_hiddenF[titleI] != 0) continue;

	/** Pointer to the current title: **/
	curr_titleP = runtimeSP->titlesP + titleI * TITLESTRINGSIZE;

	/** Do not draw empty titles: **/
	curr_title_length = strlen (curr_titleP);
	if (curr_title_length == 0) continue;

	/** String position: **/
	screen_x = runtimeSP->title_screen_x[titleI];
	screen_y = runtimeSP->title_screen_y[titleI];

	/** Draw the string: **/
	XDrawString (guiSP->displaySP,
		     guiSP->main_winS.ID,
		     guiSP->theGCA[0],
		     screen_x, screen_y,
		     curr_titleP, curr_title_length);
	}

/* Return the number of titles that were drawn: */
return titles_drawnN;
}

/*===========================================================================*/


