/* Copyright (C) 2003 Damir Zucic */

/*=============================================================================

			average_hydrophobicity.c

Purpose:
	Calculate the average hydrophobicity.

Input:
	(1) Pointer to the storage where the minimal average hydrophobicity
	    will be stored.
	(2) Pointer to the storage where the maximal average hydrophobicity
	    will be stored.
	(3) Pointer to RuntimeS structure.

Output:
	(1) Average hydrophobicity calculated and stored.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======calculate the average hydrophobicity:================================*/

int AverageHydrophobicity_ (double *min_valueP, double *max_valueP,
			    RuntimeS *runtimeSP)
{
int             residuesN, residueI;   /* Do not use size_t instead of int ! */
int		used_residuesN;
int		window_width, half_window_width, windowI, combinedI;
double		hydrophobicity, average_hydrophobicity = 0;

/*------prepare some parameters:---------------------------------------------*/

/* The number of residues in sequence buffer: */
residuesN = (int) runtimeSP->residuesN;
if (residuesN == 0) return -1;

/* The sliding window width: */
window_width = runtimeSP->sliding_window_width;
half_window_width = window_width / 2;

/*------calculate the average hydrophobicity:--------------------------------*/

/* Initialize the extreme values: */
*min_valueP = +999999.0;
*max_valueP = -999999.0;

/* Scan the sequence: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Reset the number of residues used for averaging: */
	used_residuesN = 0;

	/* Reset the average hydrophobicity: */
	average_hydrophobicity = 0.0;

	/* Scan the sliding window: */
	for (windowI = 0; windowI < window_width; windowI++)
		{
		/* Prepare and check the combined index: */
		combinedI = residueI - half_window_width + windowI;
		if (combinedI < 0) continue;
		if (combinedI >= residuesN) continue;

		/* The hydrophobicity of the residue defined by combinedI: */
		hydrophobicity =
			(double) *(runtimeSP->hydrophobicityP + combinedI);

		/* Add hydrophobicity to the total: */
		average_hydrophobicity += hydrophobicity;

		/* Increase the number of residues used for averaging: */
		used_residuesN++;
		}

	/* Check how many residues were used: */
	if (used_residuesN == window_width)
		{
		average_hydrophobicity /= (double) window_width;
		}
	else average_hydrophobicity = 0.0;

	/* Store the average hydrophobicity: */
	*(runtimeSP->average_hydrophobicityP + residueI) =
						average_hydrophobicity;

	/* Dummy values should not be used to determine extremes: */
	if (used_residuesN != window_width) continue;

	/* Find the extreme values: */
	if (average_hydrophobicity < *min_valueP)
		{
		*min_valueP = average_hydrophobicity;
		}
	if (average_hydrophobicity > *max_valueP)
		{
		*max_valueP = average_hydrophobicity;
		}

	/* End of residueI loop: */
	}

/*---------------------------------------------------------------------------*/

return 1;
}

/*===========================================================================*/


