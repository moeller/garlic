/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				venn.c

Purpose:
	Execute venn command:  draw Venn diagram for the sequence which is
	kept in the sequence buffer.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of the command string.  This command
	    may be given without any keyword, with the keyword OFF or with
	    a range of residue serial indices.

Output:
	(1) The main window mode changed to 3 (default is zero),  i.e. the
	    main window will be switched to Venn diagram.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) This command reinitializes the  NearestAtomS array,  except if
	    the additional keyword is not recognized.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractTwoIntegers_ (int *, int *, char *);
void		InitNearest_ (NearestAtomS *, size_t);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute venn command:================================================*/

int Venn_ (MolComplexS *mol_complexSP, int mol_complexesN,
	   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	   NearestAtomS *nearest_atomSP, size_t pixelsN,
	   unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
char		*P;
int		n;
int		residue1I, residue2I;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain keyword OFF: */
if (remainderP)
	{
	/* If keyword OFF is present, switch to default drawing mode: */
	if (strstr (tokenA, "OFF") == tokenA)
		{
		/* Reset drawing mode index: */
		guiSP->main_window_modeI = 0;

		/* Reinitialize the NearestAtomS array: */
		InitNearest_ (nearest_atomSP, pixelsN);
		*refreshIP = 1;

		/* Refresh the main window: */
		(*refreshIP)++;
		MainRefresh_ (mol_complexSP, mol_complexesN,
			      runtimeSP, configSP, guiSP,
			      nearest_atomSP, pixelsN, *refreshIP);

		/* Refresh the control window: */
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);

		/* Return the command code: */
		return COMMAND_VENN;
		}

	/* If keyword OFF is not present, try to read two array indices: */
	else
		{
		/* Replace each minus in the input string with space: */
		P = stringP;
		while ((n = *P++) != '\0')
			{
			if (n == '-') *(P - 1) = ' ';
			}

		/* If reading fails: */
		if (ExtractTwoIntegers_ (&residue1I, &residue2I, stringP) < 0)
			{
			strcpy (runtimeSP->messageA,
				"Command interpretation failed!");
			runtimeSP->message_length =
						strlen (runtimeSP->messageA);
			return ERROR_VENN;
			}

		/* If array indices were successfully read, check them: */
		if ((residue1I < 1) || (residue2I < residue1I))
			{
			strcpy (runtimeSP->messageA,
				"Bad range (check indices)!");
			runtimeSP->message_length =
						strlen (runtimeSP->messageA);
			return ERROR_VENN;
			}

		/* Store the extracted indices: */
		runtimeSP->range_startI = residue1I - 1;
		runtimeSP->range_endI   = residue2I - 1;
		}
	}

/* If the first token is not available, initialize range indices: */
else
	{
	runtimeSP->range_startI = 0;
	if (runtimeSP->residuesN >= 1)
		{
		runtimeSP->range_endI = (int) runtimeSP->residuesN - 1;
		}
	else
		{
		runtimeSP->range_endI = 0;
		}
	}

/* Set the main window drawing mode index: */
guiSP->main_window_modeI = 3;

/* Reinitialize the NearestAtomS array and refresh index: */
InitNearest_ (nearest_atomSP, pixelsN);
*refreshIP = 1;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_VENN;
}

/*===========================================================================*/


