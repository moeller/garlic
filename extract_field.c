/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				extract_field.c

Purpose:
	Extract field (substring) from ATOM or HETATM line (string).
	Arguments are not checked: it should be done in caller!

Input:
	(1) Output string pointer.
	(2) Position of the first character.
	(3) Position of the last character.
	(4) Input string pointer.

Output:
	(1) Substring stored at location pointed at by output string pointer.
	(2) Return value.

Return value:
	(1) Output substring length.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>

/*======extract field (substring):===========================================*/

int ExtractField_ (char *substringP, int start, int end, char *stringP)
{
int		i;

/* Copy (end - start + 1) characters: */
for (i = start; i <= end; i++) *(substringP + i - start) = *(stringP + i);

/* Terminate the substring: */
*(substringP + i - start) = '\0';

return strlen (substringP);
}

/*===========================================================================*/


