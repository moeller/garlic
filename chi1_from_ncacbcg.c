/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

				chi1_from_ncacbcg.c

Purpose:
	Calculate dihedral angle chi1, using N, CA, CB and CG coordinates.

Input:
	(1) Pointer to AtomS structure, pointing to the first atom of the
	    current macromolecular complex.
	(2) Index of the first atom of the current residue.
        (3) Index of the last atom of the currrent residue.

Output:
	Return value.

Return value:
	(1) Dihedral angle chi1, on success.
	(2) BADDIHEDANGLE on failure.

========includes:============================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		ExtractFourAtoms_ (VectorS *, VectorS *, VectorS *, VectorS *,
				   char *, char *, char *, char *,
				   AtomS *, size_t, size_t);
void		VectorProduct_ (VectorS *, VectorS *, VectorS *);
double		AbsoluteValue_ (VectorS *);
double		ScalarProduct_ (VectorS *, VectorS *);

/*======calculate chi1 from N, CA, CB and CG:================================*/

double Chi1FromNCACBCG_ (AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
static VectorS		N_vectorS, CA_vectorS, CB_vectorS, CG_vectorS;
int			n;
VectorS			CA_N_vectorS, CA_CB_vectorS;
VectorS			CB_CA_vectorS, CB_CG_vectorS;
VectorS			u1S, u2S;
VectorS			v1S, v2S;
double			denom, ratio, alpha;
double			chi1;

/* Extract N, CA, CB and CG coordinates: */
n = ExtractFourAtoms_ (&N_vectorS, &CA_vectorS, &CB_vectorS, &CG_vectorS,
		       "N", "CA", "CB", "CG",
		       atomSP, atom_startI, atom_endI);

/* All four atoms are required to calculate the angle chi1: */
if (n < 4) return BADDIHEDANGLE;

/* The first pair of auxiliary vectors: */
CA_N_vectorS.x  =  N_vectorS.x - CA_vectorS.x;
CA_N_vectorS.y  =  N_vectorS.y - CA_vectorS.y;
CA_N_vectorS.z  =  N_vectorS.z - CA_vectorS.z;
CA_CB_vectorS.x = CB_vectorS.x - CA_vectorS.x;
CA_CB_vectorS.y = CB_vectorS.y - CA_vectorS.y;
CA_CB_vectorS.z = CB_vectorS.z - CA_vectorS.z;

/* The second pair of auxiliary vectors: */
CB_CA_vectorS.x = CA_vectorS.x - CB_vectorS.x;
CB_CA_vectorS.y = CA_vectorS.y - CB_vectorS.y;
CB_CA_vectorS.z = CA_vectorS.z - CB_vectorS.z;
CB_CG_vectorS.x = CG_vectorS.x - CB_vectorS.x;
CB_CG_vectorS.y = CG_vectorS.y - CB_vectorS.y;
CB_CG_vectorS.z = CG_vectorS.z - CB_vectorS.z;

/* Two vectors perpendicular to  CA_CB_vectorS,  mutually orthogonal, */
/* the second in the plane defined by CA_N_vectorS and CA_CB_vectorS: */
VectorProduct_ (&u1S, &CA_N_vectorS, &CA_CB_vectorS);
VectorProduct_ (&u2S, &u1S, &CA_CB_vectorS);

/* Two vectors  perpendicular to  CB_CA_vectorS,  mutually orthogonal, */
/* the second in the plane defined by CB_CA_vectorS and CB_CG_vectorS: */
VectorProduct_ (&v1S, &CB_CA_vectorS, &CB_CG_vectorS);
VectorProduct_ (&v2S, &CB_CA_vectorS, &v1S);

/* Calculate the angle alpha, which will be used to calculate chi1: */

/* Avoid division by zero: */
denom = AbsoluteValue_ (&u1S) * AbsoluteValue_ (&v1S);
if (denom == 0.0) return BADDIHEDANGLE;

/* Use the scalar product to calculate the cosine of the angle: */
ratio = ScalarProduct_ (&u1S, &v1S) / denom;

/* Arc cosine is very sensitive to floating point errors: */
if (ratio <= -1.0) alpha = 3.1415927;
else if (ratio >= 1.0) alpha = 0.0;
else alpha = acos (ratio);

/* There are two possible solutions; the right one is resolved here: */
if (ScalarProduct_ (&v2S, &u1S) >= 0) chi1 = alpha;
else chi1 = -alpha;

/* Return the angle (in radians): */
return chi1;
}

/*===========================================================================*/


