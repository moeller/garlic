/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				comm_slab.c

Purpose:
	Execute slab command. Change slab mode or change the position of
	front or back slab surface.

Input:
	(1) Pointer to MolComplexS structure,  with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) String  which contains  slab surface specification  and slab
	    shift.

Output:
	(1) Slab surfaces moved.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ChangeSlab_ (MolComplexS *, int, int);
void		MoveBackSlab_ (MolComplexS *, int, ConfigS *, double);
void		MoveFrontSlab_ (MolComplexS *, int, ConfigS *, double);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute slab command:================================================*/

int CommandSlab_ (MolComplexS *mol_complexSP, int mol_complexesN,
		  RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		  NearestAtomS *nearest_atomSP, size_t pixelsN,
		  unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		surfaceID;
double		shift;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Slab specification incomplete!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SLAB;
	}

/* Switch slab off: */
if (strstr (tokenA, "OFF") == tokenA)
	{
	ChangeSlab_ (mol_complexSP, mol_complexesN, 0);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_SLAB;
	}

/* Change slab to planar: */
else if (strstr (tokenA, "PLA") == tokenA)
	{
	ChangeSlab_ (mol_complexSP, mol_complexesN, 1);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_SLAB;
	}

/* Change slab to spherical: */
else if (strstr (tokenA, "SPH") == tokenA)
	{
	ChangeSlab_ (mol_complexSP, mol_complexesN, 2);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_SLAB;
	}

/* Change slab to semi-spherical: */
else if ((strstr (tokenA, "HALF-SPH") == tokenA) ||
	 (strstr (tokenA, "HALF_SPH") == tokenA))
	{
	ChangeSlab_ (mol_complexSP, mol_complexesN, 3);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_SLAB;
	}

/* Change slab to cylindrical: */
else if (strstr (tokenA, "CYL") == tokenA)
	{
	ChangeSlab_ (mol_complexSP, mol_complexesN, 4);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_SLAB;
	}

/* Change slab to semi-cylindrical: */
else if ((strstr (tokenA, "HALF-CYL") == tokenA) ||
	 (strstr (tokenA, "HALF_CYL") == tokenA))
	{
	ChangeSlab_ (mol_complexSP, mol_complexesN, 5);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_SLAB;
	}

/* Identify the surface: */
else if (strstr (tokenA, "FRO") == tokenA) surfaceID = 1;
else if (strstr (tokenA, "BAC") == tokenA) surfaceID = 2;
else
	{
	strcpy (runtimeSP->messageA,
		"Bad surface (valid keywords are front and back)!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BAD_SURFACE;
	}

/* Extract the token which contains the shift: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Slab shift missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_SHIFT;
	}

/* Extract the shift: */
if (sscanf (tokenA, "%lf", &shift) != 1)
	{
	strcpy (runtimeSP->messageA,
		"Slab specification is bad (unknown keyword used)!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_SHIFT;
	}

/* Move the specified slab surface: */
if (surfaceID == 1)
	{
	MoveFrontSlab_ (mol_complexSP, mol_complexesN, configSP, shift);
	}
else
	{
	MoveBackSlab_ (mol_complexSP, mol_complexesN, configSP, shift);
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_SLAB;
}

/*===========================================================================*/


