/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				align_complexes.c

Purpose:
	Align two macromolecular complexes vertically. The first complex
	will be placed at the bottom, while the second will be placed at
	the top. A small separation will be left between them.

Input:
	(1) Pointer to MolComplexS structure, with the first complex.
	(2) Pointer to MolComplexS structure, with the second complex.
	(3) Pointer to ConfigS structure.

Output:
	(1) Two macromolecular complexes will be translated.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		TranslateComplex_ (MolComplexS *, VectorS *, ConfigS *, int);
void		TranslatePlane_ (MolComplexS *, VectorS *);
void		TranslateMembrane_ (MolComplexS *, VectorS *);

/*======align two complexes vertically:======================================*/

int AlignComplexes_ (MolComplexS *mol_complex1SP, MolComplexS *mol_complex2SP,
		     ConfigS *configSP)
{
size_t		atomI;
AtomS		*curr_atomSP;
double		y, y1_min, y2_max;
VectorS		shift_vectorS;

/* Check pointers: */
if (!mol_complex1SP) return -1;
if (!mol_complex2SP) return -2;

/* Both complexes should contain at least one atom: */
if (mol_complex1SP->atomsN == 0) return -3;
if (mol_complex2SP->atomsN == 0) return -4;

/* For the first complex, find the minimal y: */
y1_min = mol_complex1SP->atomSP->raw_atomS.y;
for (atomI = 0; atomI < mol_complex1SP->atomsN; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = mol_complex1SP->atomSP + atomI;

	/* The y coordinate of the current atom: */
	y = curr_atomSP->raw_atomS.y;

	/* Compare y with y1_min: */
	if (y < y1_min) y1_min = y;
	}

/* For the second complex, find the maximal y: */
y2_max = mol_complex2SP->atomSP->raw_atomS.y;
for (atomI = 0; atomI < mol_complex2SP->atomsN; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = mol_complex2SP->atomSP + atomI;

	/* The y coordinate of the current atom: */
	y = curr_atomSP->raw_atomS.y;

	/* Compare y with y2_max: */
	if (y > y2_max) y2_max = y;
	}

/* Prepare the translation vector for the first complex. The geometric */
/* center should be moved to y axis, slightly below the system origin: */
shift_vectorS.x = -1.0 * mol_complex1SP->geometric_center_vectorS.x;
shift_vectorS.y = -1.0 *(y1_min - 0.5 * DOCKING_GAP);
shift_vectorS.z = -1.0 * mol_complex1SP->geometric_center_vectorS.z;

/* Translate the first complex and associated objects: */
TranslateComplex_ (mol_complex1SP, &shift_vectorS, configSP, 0);
TranslatePlane_ (mol_complex1SP, &shift_vectorS);
TranslateMembrane_ (mol_complex1SP, &shift_vectorS);

/* Prepare the translation vector for the second complex. The geometric */
/* center should be moved to y axis,  slightly above the system origin: */
shift_vectorS.x = -1.0 * mol_complex2SP->geometric_center_vectorS.x;
shift_vectorS.y = -1.0 *(y2_max + 0.5 * DOCKING_GAP);
shift_vectorS.z = -1.0 * mol_complex2SP->geometric_center_vectorS.z;

/* Translate the second complex and associated objects: */
TranslateComplex_ (mol_complex2SP, &shift_vectorS, configSP, 0);
TranslatePlane_ (mol_complex2SP, &shift_vectorS);
TranslateMembrane_ (mol_complex2SP, &shift_vectorS);

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


