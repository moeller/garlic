/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				alloc_mol_complex.c

Purpose:
	Allocate memory for an array of MolComplexS structures. The calloc
	function is used to zero-initialize all bytes.

Input:
	No arguments.

Output:
	(1) Memory for an array of MolComplexS structures allocated.
	(2) Return value.

Return value:
	(1) Pointer to allocated memory, on success.
	(2) NULL on failure.

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======allocate memory for array of MolComplexS structures:=================*/

MolComplexS *AllocateMolComplexSA_ (size_t max_complexes)
{
static MolComplexS		*mol_complexSP;

/* Allocate zero-initialized memory: */
mol_complexSP = (MolComplexS *) calloc (max_complexes, sizeof (MolComplexS));

if (mol_complexSP == NULL)
	{
	ErrorMessage_ ("garlic", "AllocateMolComplexSA_", "",
		"Failed to allocate memory for an array",
		" of MolComplexS structures!\n", "", "");
	}

/* Return pointer to allocated memory (equal to NULL on failure): */
return mol_complexSP;
}

/*===========================================================================*/


