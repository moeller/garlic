/* Copyright (C) 2000-2004 Damir Zucic */

/*=============================================================================

				scale.c

Purpose:
	Execute scale command: change the hydrophobicity scale. This scale
	will be assigned to all caught macromolecular complexes and to the
	sequence stored in the sequence buffer.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to the remainder of the command string.  The remainder
	    should contain the hydrophobicity scale name. The keywords are
	    EIS (EISENBERG), INT (INTERFACE), IN2, KD (KYTE-DOOLITLE), OCT  
	    (OCTANOL) and DIF (DIFFERENTIAL, octanol minus interface).

Output:
	(1) Hydrophobicity scale changed  for each caught complex  and for
	    the sequence stored in the sequence buffer.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The main window content will not be refreshed.

	(2) Check init_runtime.c for default hydrophobicity scale!

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		AssignHydrophobicity_ (MolComplexS *, int);
void		InitHyphob_ (RuntimeS *);

/*======execute scale command:===============================================*/

int Scale_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    RuntimeS *runtimeSP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		scaleI = 2;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;

/* Extract the first token, if present: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If there are no tokens in the remainder of the */
/* command  string,  the  scale name  is missing: */
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Scale name missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SCALE;
	}

/* OCT (OCTANOL); before it was WHI (WHITE): */
else if (strstr (tokenA, "OCT") == tokenA) scaleI = 0;

/* For compatibility reasons, check for the old */
/* (obsolete) keyword for  White octanol scale: */
else if (strstr (tokenA, "WHI") == tokenA)
	{
	strcpy (runtimeSP->messageA, 
		"Keyword WHITE (WHI) is obsolete, please use OCTANOL (OCT)!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	scaleI = 0;
	}

/* KD (KYTE-DOOLITLE): */
else if (strstr (tokenA, "KD")  == tokenA) scaleI = 1;

/* EIS (EISENBERG): */
else if (strstr (tokenA, "EIS") == tokenA) scaleI = 2;

/* INT (INTERFACE): */
else if (strstr (tokenA, "INT") == tokenA) scaleI = 3;

/* IN2 (modified interface scale): */
else if (strstr (tokenA, "IN2") == tokenA) scaleI = 4;

/* DIF (differential, octanol-interface): */
else if (strstr (tokenA, "DIF") == tokenA) scaleI = 5;

/* Keyword not recognized: */
else
	{
	strcpy (runtimeSP->messageA, "Keyword not recognized!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_SCALE;
	}

/* For each caught macromolecular complex, change the scale: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* Check is the current macromolecular complex caught: */
	if (curr_mol_complexSP->catchF == 0) continue;

	/* Change hydrophobicity scale: */
	AssignHydrophobicity_ (curr_mol_complexSP, scaleI);
	}

/* Update the hydrophobicity scale index in RuntimeS structure: */
runtimeSP->hydrophobicity_scaleI = scaleI;

/* Update hydrophobicity values assigned to residues in the sequence buffer: */
InitHyphob_ (runtimeSP);

/* Return the command code: */
return COMMAND_SCALE;
}

/*===========================================================================*/


