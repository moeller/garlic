/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				hybond_style.c

Purpose:
	Change drawing style for hydrogen bonds.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Bond drawing style index.

Output:
	(1) Bond drawing style changed for every hydrogen bond in a given
	    complex.
	(2) Return value.

Return value:
	(1) Positive if the given complex contains at least one atom.
	(2) Zero otherwise.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======change hydrogen bond drawing style:==================================*/

int ChangeHybondStyle_ (MolComplexS *curr_mol_complexSP, int styleI)
{
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
int		bondsN, bondI;
TrueBondS	*curr_bondSP;

/* Prepare and check the number of atoms in this complex: */
atomsN = curr_mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* Check every atom: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/* Pointer to this atom: */
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/* The number of bonds: */
	bondsN = curr_atomSP->bondsN;

	/* Check every bond: */
	for (bondI = 0; bondI < bondsN; bondI++)
		{
		/* Pointer to the structure with data about current bond: */
		curr_bondSP = curr_atomSP->true_bondSA + bondI;

		/* If the current bond is not hydrogen bond, skip it: */
		if (curr_bondSP->bond_typeI != 0) continue;

		/* Change the bond drawing style: */
		curr_bondSP->bond_styleI = styleI;
		}
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


